<?php
/**
 * Created by PhpStorm.
 * User: HanhDo
 * Date: 4/11/2019
 * Time: 2:14 PM
 */
//$seat_data = $_SESSION['sttp_seat'];

$segment = $seat_data['segment'];
$traveler = $seat_data['traveler'];
$traveler_info = $passenger;
$seat = $seat_data['seat'];
$optional = $seat_data['optional'];
$traveler_key = array();

$arr_icon = array(
    'Available' => '<img src="' . ST_TravelPort()->get_url('assets/images/seat/seat_available_free.svg') . '" width="45"/>',
    'AvailablePaid' => '<img src="' . ST_TravelPort()->get_url('assets/images/seat/seat_available_paid.svg') . '" width="45"/>',
    'Occupied' => '<img src="' . ST_TravelPort()->get_url('assets/images/seat/seat_occupied.svg') . '" width="45"/>',
    'Selected' => '<img src="' . ST_TravelPort()->get_url('assets/images/seat/seat_selected.svg') . '" width="45"/>',
    'SelectedActive' => '<img src="' . ST_TravelPort()->get_url('assets/images/seat/seat_selected_active.svg') . '" width="45"/>',
    'Blocked' => '<img src="' . ST_TravelPort()->get_url('assets/images/seat/seat_blocked.svg') . '" width="45"/>',
    'Reserved' => '<img src="' . ST_TravelPort()->get_url('assets/images/seat/seat_blocked.svg') . '" width="45"/>',
    'NoSeat' => '',
);
if(!empty($segment)){
    ?>

    <div class="sttp-seat-popup">

        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <?php foreach ($segment as $k => $v){ ?>
                <li role="<?php echo $v['Attributes']['Key']; ?>" class="<?php echo $k == 0 ? 'active' : ''; ?>"><a href="#<?php echo 'segment' . $k; ?>" aria-controls="<?php echo 'segment' . $k; ?>" role="tab" data-toggle="tab">
                        <?php
                        echo $v['Attributes']['Origin'] . ' > ' . $v['Attributes']['Destination'];
                        ?>
                    </a></li>
            <?php } ?>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <?php foreach ($segment as $k => $v){?>
                <div role="tabpanel" class="tab-pane <?php echo $k == 0 ? 'active' : ''; ?>" id="<?php echo 'segment' . $k; ?>">
                    <?php
                    $info = $v['Attributes'];
                    $startDate = STTP_Search::inst()->getDateTime($info['DepartureTime'], 'd/m/Y');
                    $endDate = STTP_Search::inst()->getDateTime($info['ArrivalTime'], 'd/m/Y');
                    ?>
                    <div class="carrier-info">
                        <span><b>Carrier:</b> <?php echo $info['Carrier'] ?></span> &nbsp;
                        <span><b>Class:</b> <?php echo $info['ClassOfService'] ?></span> &nbsp;
                        <span><b>Journey Time:</b> <?php echo STTP_Search::inst()->minuteToHourMinute($info['TravelTime']); ?></span> &nbsp;
                        <span><b>Flight Type:</b> <?php echo $info['Equipment'] ?></span> &nbsp;
                        <span><b>Flight Number:</b> <?php echo $info['FlightNumber'] ?></span> &nbsp;
                        <span><b><?php echo $startDate['date'] . ' ' . $startDate['time'] . ' ' . $startDate['time_zone']; ?></b></span> &nbsp;
                        <span><b><?php echo $endDate['date'] . ' ' . $endDate['time'] . ' ' . $endDate['time_zone']; ?></b></span>
                    </div>

                    <div class="row">
                        <div class="col-lg-2">
                            <div class="sttp-seat-traveler">
                                <?php

                                if(!empty($traveler_info)){
                                    $it = 0;
                                    foreach ($traveler_info as $kk => $vv){
                                        ?>
                                        <div class="item <?php echo $it == 0 ? 'active' : ''; ?>">
                                            <span class="seat-value" data-key="<?php echo $kk; ?>" data-name="<?php echo $vv['first_name'] . ' ' . $vv['last_name'];  ?>" data-coderef="<?php echo $v['Attributes']['Origin'] . '|' . $v['Attributes']['Destination']; ?>"></span>
                                            <span class="traveler-name">
                                                <?php echo $vv['first_name'] . ' ' . $vv['last_name'];  ?>
                                            </span>
                                        </div>
                                        <?php
                                        $it++;
                                    }
                                }
                                ?>
                            </div>
                            <div class="sttp-seat-note">
                                <div class="item">
                                        <img src="<?php echo ST_TravelPort()->get_url('assets/images/seat/seat_selected_active.svg') ?>" /> Selected Seat active
                                </div><br />
                                <div class="item">
                                        <img src="<?php echo ST_TravelPort()->get_url('assets/images/seat/seat_selected.svg') ?>" /> Selected Seat
                                </div><br />
                                <div class="item">
                                        <img src="<?php echo ST_TravelPort()->get_url('assets/images/seat/seat_available_free.svg') ?>" /> Available Free Seat
                                </div><br />
                                <div class="item">
                                        <img src="<?php echo ST_TravelPort()->get_url('assets/images/seat/seat_available_paid.svg') ?>" /> Available Paid Seat
                                </div><br />
                                <div class="item">
                                        <img src="<?php echo ST_TravelPort()->get_url('assets/images/seat/seat_available_after.svg') ?>" /> Available after reservation
                                </div><br />
                                <div class="item">
                                        <img src="<?php echo ST_TravelPort()->get_url('assets/images/seat/seat_blocked.svg') ?>" /> Blocked
                                </div><br />
                                <div class="item">
                                    <img src="<?php echo ST_TravelPort()->get_url('assets/images/seat/seat_occupied.svg') ?>" /> Occupied
                                </div><br />
                               <!-- <div class="item">
                                    <a data-toggle="tooltip" data-html="true" title="Exit Row">
                                        <img src="<?php /*echo ST_TravelPort()->get_url('assets/images/seat/seat_exit.svg') */?>" /> </a>
                                </div>-->
                            </div>
                            <button class="sttp-apply-seat"><?php echo __('APPLY', 'st_travelport') ?> <i class="fa fa-spin fa-spinner"></i></button>
                        </div>
                        <div class="col-lg-10">
                            <div class="sttp-seat-table">
                                <?php
                                if(isset($seat[$v['Attributes']['Key']])){
                                    $seat_temp = $seat[$v['Attributes']['Key']];
                                    $stemp = array();
                                    foreach ($seat_temp as $kk => $vv){
                                        $stemp[$vv['SearchTravelerRef']][] = $vv;
                                    }

                                    $vitemI = 0;
                                    foreach ($stemp as $kitem => $vitem) {
                                        if($vitemI == 0) {
                                            $column_count = count($vitem);
                                            $row_count = count($vitem[0]['Facility']);

                                            $class = '';
                                            if ($vitemI == 0)
                                                $class = 'active';

                                            echo '<table class="seat-table seat-table-' . $vitemI . ' ' . $class . '">';
                                            echo '<tr>';
                                            foreach ($vitem as $kk => $vv) {
                                                echo '<th>' . $vv['Number'] . '</th>';
                                            }
                                            echo '</tr>';
                                            for ($i = $row_count - 1; $i >= 0; $i--) {
                                                echo '<tr>';
                                                for ($j = 0; $j < $column_count; $j++) {
                                                    $type = $vitem[$j]['Facility'][$i]['Attributes']['Type'];
                                                    if ($type != 'Aisle') {
                                                        $seat_available = $vitem[$j]['Facility'][$i]['Attributes']['Availability'];
                                                        $price_optional = 'Free';
                                                        $seat_code = $vitem[$j]['Facility'][$i]['Attributes']['SeatCode'];
                                                        $seat_code_char = explode('-', $seat_code)[1];
                                                        $seat_code_str = '<span class="seat-code-label">'. $seat_code_char .'</span>';

                                                        echo '<td>';
                                                        if ($seat_available == 'NoSeat') {
                                                            $seat_code_temp = explode('-', $seat_code);
                                                            echo $seat_code_temp[1];
                                                        } elseif($seat_available == 'Available') {

                                                            if(isset($vitem[$j]['Facility'][$i]['Attributes']['OptionalServiceRef'])){
                                                                if(isset($optional[$vitem[$j]['Facility'][$i]['Attributes']['OptionalServiceRef']])){
                                                                    $price_optional = $optional[$vitem[$j]['Facility'][$i]['Attributes']['OptionalServiceRef']]['TotalPrice'];
                                                                }
                                                            }
                                                            ?>
                                                            <?php

                                                            echo '<span title="Seat: '. $seat_code . ' &nbsp;&nbsp; Price: ' . $price_optional .'" class="ct-tooltip item-available normal" data-seat="'. $vitem[$j]['Facility'][$i]['Attributes']['SeatCode'] .'" data-price="'. $price_optional .'" data-index="0">';
                                                            echo '<span class="selected">'. $seat_code_str .  $arr_icon['Selected'] .'</span>';
                                                            echo '<span class="selected-active">'. $seat_code_str . $arr_icon['SelectedActive'] .'</span>';
                                                            echo '<span class="normal">';
                                                            if(isset($vitem[$j]['Facility'][$i]['Attributes']['Paid'])) {
                                                                $seat_available_paid = $vitem[$j]['Facility'][$i]['Attributes']['Paid'];
                                                            }else{
                                                                $seat_available_paid = 'false';
                                                            }
                                                            if($seat_available_paid == 'true'){
                                                                echo $seat_code_str . $arr_icon['AvailablePaid'];
                                                            }else{
                                                                echo $seat_code_str . $arr_icon['Available'];
                                                            }
                                                            echo '</span>';
                                                            echo '</span>';
                                                        }else{
                                                            if (isset($arr_icon[$seat_available])) {
                                                                echo $arr_icon[$seat_available];
                                                            } else {
                                                                echo $seat_available;
                                                            }
                                                        }
                                                        ?>
                                                        <div class="sttp-seat-tooltip">
                                                            <div class="foot">
	                                                            <?php echo $price_optional; ?>
                                                            </div>
                                                        </div>
                                                        <?php
                                                        echo '</td>';
                                                    } else {
                                                        echo '<td class="aisle"></td>';
                                                    }

                                                }

                                                echo '</tr>';
                                            }
                                            echo '</table>';
                                        }
                                        $vitemI++;
                                    }

                                }else{
                                    echo __('No available SeatMap for this segment.', 'st_travelport');
                                }
                                ?>
                            </div>
                        </div>
                    </div>

                </div>
            <?php } ?>
        </div>

    </div>
<?php } ?>
<h3><?php echo __('Passenger Information', 'st_travelport') ?></h3>
<?php
$traveler = STTP_Search::inst()->getBookingTravelerData($order_code);
echo '<table class="sttp-history-passenger payment-success">';
echo '<tr>';
echo '<th>'. __('Type', 'st_travelport') .'</th>';
echo '<th>'. __('Full Name', 'st_travelport') .'</th>';
echo '<th>'. __('Email', 'st_travelport') .'</th>';
echo '<th>'. __('Phone', 'st_travelport') .'</th>';
echo '<th>'. __('Age', 'st_travelport') .'</th>';
echo '</tr>';
foreach ($traveler as $k => $v){
	echo '<tr>';
	echo '<td>'. $v['type'] .'</td>';
	echo '<td>'. $v['first_name'][0] . ' ' . $v['last_name'][0] .'</td>';
	echo '<td>'. $v['email'][0] .'</td>';
	echo '<td>'. $v['phone'][0] .'</td>';
	echo '<td>';
	if(isset($v['age'])){
		echo $v['age'];
	}
	echo '</td>';
	echo '</tr>';
}
echo '</table>';
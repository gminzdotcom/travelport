<?php
/**
 * Created by PhpStorm.
 * User: HanhDo
 * Date: 4/3/2019
 * Time: 8:42 AM
 */
$fly_out = STInput::get('fly_out');
$fly_back = STInput::get('fly_back');
$flight_way = STInput::get('flight_way');
$from_code = STInput::get('from_code');
$to_code = STInput::get('to_code');
$adult_number = STInput::get('adult_number');
$child_number = STInput::get('child_number');
$infant_number = STInput::get('infant_number');
$sttp_child_age = STInput::get('sttp_child_age', array());
$sttp_infant_age = STInput::get('sttp_infant_age', array());
$cabin_class = STInput::get('preferred_cabin', '');


$roundtrip = true;
if(empty($fly_back) || $flight_way == 'oneway')
    $roundtrip = false;
$pointrs = STTP_Search::inst()->getSearchResults($from_code, $to_code, $fly_out, $fly_back, $flight_way, $adult_number, $child_number, $infant_number, $sttp_child_age, $sttp_infant_age, $cabin_class, $roundtrip);

if(isset($pointrs['faultcode'])){
    echo '<div class="alert alert-warning">';
    echo $pointrs['faultcode'] . '<br />';
    echo $pointrs['faultstring'];
    echo '</div>';
    return;
}
$posts = $pointrs['posts'];

global $segments;
$segments = $pointrs['segments'];
$flights = $pointrs['flights'];
$fares = $pointrs['fare'];

$filter_data = STTP_Search::inst()->getFilterData($posts);

$per_page = get_option('st_travelport_search_item', '10');
$total_rows = count($posts);
$pages = ceil($total_rows / $per_page);
$current_page = STInput::get('sttp_page', 1);
$current_page = ($total_rows > 0) ? min($pages, $current_page) : 1;
$start = $current_page * $per_page - $per_page;

$arr_carries_total = array();

$slice = array_slice($posts, $start, $per_page);
//dd($slice);die;

//dd($slice);die;
/* Check sort data */
$price_from = '';
$price_to = '';
if(isset($_GET['sttp_price_range'])) {
	$price = $_GET['sttp_price_range'];
	if(!empty($price)){
	    $price_arr = explode(';', $price);
	    $price_from = $price_arr[0];
	    $price_to = $price_arr[1];
		$posts = array_filter( $posts, function ( $item ) use ( $price_arr ) {
			$price_with_text = $item['Attributes']['TotalPrice'];

            $main_currency =  get_option('st_travelport_currency');
            if(empty($main_currency))
                $main_currency = 'GBP';

			$price_with_text = str_replace( $main_currency, '', $price_with_text );
			if ( $price_with_text >= $price_arr[0] && $price_with_text <= $price_arr[1] ) {
				return true;
			} else {
				return false;
			}
		} );
    }
}

if(isset($_GET['sttp_stops'])){
    $sttp_stops = $_GET['sttp_stops'];
    if(!in_array('all', $sttp_stops)){
	    $posts = array_filter( $posts, function ( $item ) use ( $sttp_stops ) {
		    $countBookingInfo = count($item['FlightList'][0]['Options'][0]['BookingInfo']) - 1;
		    if ( in_array($countBookingInfo, $sttp_stops) ) {
			    return true;
		    } else {
			    return false;
		    }
	    } );
    }
}

if(isset($_GET['sttp_preferred_cabin'])){
    $sttp_cabin = $_GET['sttp_preferred_cabin'];
    if(!in_array('', $sttp_cabin)){
        $posts = array_filter( $posts, function ( $item ) use ( $sttp_cabin ) {
            $bookingInfoCabin = $item['FlightList'][0]['Options'][0]['BookingInfo'];
            $cjeck = false;
            foreach ($bookingInfoCabin as $k => $v){
                if ( in_array($v['CabinClass'], $sttp_cabin) ) {
                    $cjeck = true;
                }
            }
            if($cjeck){
                return true;
            }else{
                return false;
            }
        } );
    }
}

if(isset($_GET['sttp_airline'])){
	$sttp_airline = $_GET['sttp_airline'];
	if(!in_array('all', $sttp_airline)){
		$posts = array_filter( $posts, function ( $item ) use ( $sttp_airline ) {
			global $segments;
			$first_item = $item['FlightList'][0]['Options'][0];
			$itnObj = STTP_Search::inst()->getItnObj($first_item['BookingInfo'], $segments);

			$check = false;
			if(!empty($sttp_airline)){
				foreach ($sttp_airline as $kk => $vv){
					if(in_array($vv, $itnObj['carrier'])){
						$check = true;
						break;
					}
				}
			}
			if ( $check ) {
				return true;
			} else {
				return false;
			}
		} );
	}
}

if(isset($_GET['sttp_airport'])){
	$sttp_airport = $_GET['sttp_airport'];
	if(!in_array('all', $sttp_airport)){
		$posts = array_filter( $posts, function ( $item ) use ( $sttp_airport ) {
			global $segments;
			$first_item = $item['FlightList'][0]['Options'][0];
			$itnObj = STTP_Search::inst()->getItnObj($first_item['BookingInfo'], $segments);

			$check = false;
			if(!empty($sttp_airport)){
				foreach ($sttp_airport as $kk => $vv){
					if(in_array($vv, $itnObj['proccess'])){
						$check = true;
						break;
					}
				}
			}
			if ( $check ) {
				return true;
			} else {
				return false;
			}
		} );
	}
}

if(isset($_GET['sttp_departure'])){
	$sttp_departure = $_GET['sttp_departure'];
    $posts = array_filter( $posts, function ( $item ) use ( $sttp_departure ) {
        global $segments;
        $arr_time = explode(';', $sttp_departure);
        $first_item = $item['FlightList'][0]['Options'][0];
        $itnObj = STTP_Search::inst()->getItnObj($first_item['BookingInfo'], $segments);
        $dateTime = STTP_Search::inst()->getDateTime($itnObj['time'][0]);
        $time_search = $dateTime['time'];
        if($time_search >= $arr_time[0] && $time_search <= $arr_time[1]){
            return true;
        }else{
            return false;
        }
    } );
}

if(isset($_GET['sttp_duration'])){
	$sttp_duration = $_GET['sttp_duration'];
	$posts = array_filter( $posts, function ( $item ) use ( $sttp_duration ) {
		global $segments;
		$arr_time = explode(';', $sttp_duration);
		$first_item = $item['FlightList'][0]['Options'][0];
		$duration_item = STTP_Search::inst()->formatTravelTime($first_item['Attributes']['TravelTime']);
		if($duration_item >= $arr_time[0] && $duration_item <= $arr_time[1]){
			return true;
		}else{
			return false;
		}
	} );
}


$sort_by = STInput::get('sort_by', '');
if(!empty($sort_by)){
    switch ($sort_by){
        case 'departure_early':
            array_multisort(array_map(function($element) {
                global $segments;
                $first_item = $element['FlightList'][0]['Options'][0];
                return strtotime($segments[$first_item['BookingInfo'][0]['SegmentRef']]['DepartureTime']);
            }, $posts), SORT_ASC, $posts);
        break;
        case 'departure_late':
	        array_multisort(array_map(function($element) {
		        global $segments;
		        $first_item = $element['FlightList'][0]['Options'][0];
		        return strtotime($segments[$first_item['BookingInfo'][0]['SegmentRef']]['DepartureTime']);
	        }, $posts), SORT_DESC, $posts);
            break;
        case 'arrival_early':
	        array_multisort(array_map(function($element) {
		        global $segments;
		        $first_item = $element['FlightList'][0]['Options'][0];
		        return strtotime($segments[$first_item['BookingInfo'][count($first_item['BookingInfo']) - 1]['SegmentRef']]['ArrivalTime']);
	        }, $posts), SORT_ASC, $posts);
            break;
        case 'arrival_late':
	        array_multisort(array_map(function($element) {
		        global $segments;
		        $first_item = $element['FlightList'][0]['Options'][0];
		        return strtotime($segments[$first_item['BookingInfo'][count($first_item['BookingInfo']) - 1]['SegmentRef']]['ArrivalTime']);
	        }, $posts), SORT_DESC, $posts);
            break;
        case 'duration_lth':
            array_multisort(array_map(function($element) {
                return STTP_Search::inst()->formatTravelTimeInt($element['FlightList'][0]['Options'][0]['Attributes']['TravelTime']);
            }, $posts), SORT_ASC, $posts);
            break;
        case 'duration_htl':
            array_multisort(array_map(function($element) {
                return STTP_Search::inst()->formatTravelTimeInt($element['FlightList'][0]['Options'][0]['Attributes']['TravelTime']);
            }, $posts), SORT_DESC, $posts);
            break;
        case 'price_lth':
            array_multisort(array_map(function($element) {
                return $element['Attributes']['TotalPrice'];
            }, $posts), SORT_ASC, $posts);
            break;
        case 'price_htl':
            array_multisort(array_map(function($element) {
                return $element['Attributes']['TotalPrice'];
            }, $posts), SORT_DESC, $posts);
            break;
    }
}


$this_link = get_the_permalink();
$this_link = add_query_arg($_GET, $this_link);
?>
<div class="sttp-search-result">
    <div id="sttp-form-get">
        <form method="get">
            <?php
            if(!empty($_GET)) {
                foreach ($_GET as $k => $v) {
                    if(is_array($v)){
                        $v = implode(',', $v);
                    }
                    echo '<input type="hidden" name="'. $k .'" value="'. $v .'"/>';
                }
            }
            ?>
        </form>
    </div>

    <div class="data-carries"></div>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-3 sttp-sidebar">
            <form method="get">
                <?php
                $getData = $_GET;
                if(!empty($getData)){
	                foreach ($getData as $k => $v){
		                if($k != 'sttp_stops' && $k != 'sttp_price_range' && $k != 'sttp_airline' && $k != 'sttp_airport' && $k != 'sttp_preferred_cabin') {
			                if ( is_array( $v ) ) {
				                foreach ( $v as $vv ) {
					                echo '<input type="hidden" name="' . $k . '[]" value="' . $vv . '" />';
				                }
			                } else {
				                echo '<input type="hidden" name="' . $k . '" value="' . $v . '" />';
			                }
		                }
	                }
                }


                ?>
            <div class="sidebar-item range-slider">
                <div class="item-title">
                    <h5><?php echo __('Price', 'st_travelport'); ?></h5>
                </div>
                <div class="item-content">
                    <?php
                    $main_currency =  get_option('st_travelport_currency');
                    if(empty($main_currency))
                        $main_currency = 'GBP';
                    ?>
                    <input type="text" class="sttp_price_range" name="sttp_price_range" value="" data-symbol="<?php echo $main_currency; ?>" data-min="<?php echo $filter_data['price']['min']; ?>" data-max="<?php echo $filter_data['price']['max']; ?>" data-step="1" data-sttpfrom="<?php echo $price_from; ?>" data-sttpto="<?php echo $price_to; ?>"/>
                    <button type="submit" class="btn btn-primary btn-xs"><?php echo __('APPLY', 'st_travelport'); ?></button>
                </div>
            </div>
                <br />
                <!--Cabin class-->
                <?php if($cabin_class == ''){ ?>
                <div class="sidebar-item stops">
                    <div class="item-title">
                        <h5><?php echo __('Cabin Class', 'st_travelport'); ?></h5>
                    </div>
                    <div class="item-content">
                        <div class="st-icheck-item">
                            <label><?php echo __('All', 'st_travelport') ?> <input type="checkbox" onchange="this.form.submit();" <?php if(isset($_GET['sttp_preferred_cabin'])) echo in_array('', $_GET['sttp_preferred_cabin']) ? 'checked' : ''; ?> name="sttp_preferred_cabin[]" value="" data-way=""><span class="checkmark fcheckbox"></span></label>
                        </div>
                        <?php
                        $arr_class = array(
                            'Premium First',
                            'First',
                            'Business',
                            'Premium Economy',
                            'Economy'
                        );
                        foreach ($arr_class as $ck => $cv){
                            ?>
                            <div class="st-icheck-item">
                                <label><?php echo $cv; ?> <input type="checkbox" onchange="this.form.submit();" <?php if(isset($_GET['sttp_preferred_cabin'])) echo in_array($cv, $_GET['sttp_preferred_cabin']) ? 'checked' : ''; ?> name="sttp_preferred_cabin[]" value="<?php echo $cv; ?>" data-way=""><span class="checkmark fcheckbox"></span></label>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div><br />
                <?php } ?>
                <!--End Cabin Class-->

                <div class="sidebar-item stops">
                    <div class="item-title">
                        <h5><?php echo __('Stops', 'st_travelport'); ?></h5>
                    </div>
                    <div class="item-content">
                        <?php
                        if(!empty($filter_data['stops'])){
                            ?>
                            <div class="st-icheck-item">
                                <label><?php echo __('All', 'st_travelport') ?> <input type="checkbox" onchange="this.form.submit();" <?php if(isset($_GET['sttp_stops'])) echo in_array('all', $_GET['sttp_stops']) ? 'checked' : ''; ?> name="sttp_stops[]" value="all" data-way=""><span class="checkmark fcheckbox"></span></label>
                            </div>
                            <?php
                            foreach ($filter_data['stops'] as $sk => $sv){
                                $name_stop = '';
                                if($sv == 0){
                                    $name_stop = 'Non-Stop';
                                }elseif($sv == 1){
	                                $name_stop = '1 stop';
                                }else{
	                                $name_stop = $sv . ' stops';
                                }
                                ?>
                                <div class="st-icheck-item">
                                    <label><?php echo $name_stop; ?> <input type="checkbox" onchange="this.form.submit();" <?php if(isset($_GET['sttp_stops'])) echo in_array($sv, $_GET['sttp_stops']) ? 'checked' : ''; ?> name="sttp_stops[]" value="<?php echo $sv; ?>" data-way=""><span class="checkmark fcheckbox"></span></label>
                                </div>
                                <?php
                            }
                        }
                        ?>
                    </div>
                </div>
                <br />
                <div class="sidebar-item airline">
                    <div class="item-title">
                        <h5><?php echo __('Airline', 'st_travelport'); ?></h5>
                    </div>
                    <div class="item-content">
			            <?php
			            if(!empty($filter_data['airline'])){
			                ?>
                        <div class="st-icheck-item">
                            <label><?php echo __('All', 'st_travelport') ?> <input type="checkbox" onchange="this.form.submit();" <?php if(isset($_GET['sttp_airline'])) echo in_array('all', $_GET['sttp_airline']) ? 'checked' : ''; ?> name="sttp_airline[]" value="all" data-way=""><span class="checkmark fcheckbox"></span></label>
                        </div>
                            <?php
				            foreach ($filter_data['airline'] as $sk => $sv){
                                ?>
                                <div class="st-icheck-item">
                                    <label><span class="sttp-carrier"><?php echo $sv; ?></span> <input type="checkbox" onchange="this.form.submit();" <?php if(isset($_GET['sttp_airline'])) echo in_array($sv, $_GET['sttp_airline']) ? 'checked' : ''; ?> name="sttp_airline[]" value="<?php echo $sv; ?>" data-way=""><span class="checkmark fcheckbox"></span></label>
                                </div>
					            <?php
				            }
			            }
			            ?>
                    </div>
                </div>
                <br />
                <div class="sidebar-item airport">
                    <div class="item-title">
                        <h5><?php echo __('Airport', 'st_travelport'); ?></h5>
                    </div>
                    <div class="item-content">
			            <?php
			            if(!empty($filter_data['airport'])){
				            ?>
                            <div class="st-icheck-item">
                                <label><?php echo __('All', 'st_travelport') ?> <input type="checkbox" onchange="this.form.submit();" <?php if(isset($_GET['sttp_airport'])) echo in_array('all', $_GET['sttp_airport']) ? 'checked' : ''; ?> name="sttp_airport[]" value="all" data-way=""><span class="checkmark fcheckbox"></span></label>
                            </div>
				            <?php
				            foreach ($filter_data['airport'] as $sk => $sv){
					            ?>
                                <div class="st-icheck-item">
                                    <label><span class="sttp-airport-name"><?php echo $sv; ?></span> <input type="checkbox" onchange="this.form.submit();" <?php if(isset($_GET['sttp_airport'])) echo in_array($sv, $_GET['sttp_airport']) ? 'checked' : ''; ?> name="sttp_airport[]" value="<?php echo $sv; ?>" data-way=""><span class="checkmark fcheckbox"></span></label>
                                </div>
					            <?php
				            }
			            }
			            ?>
                    </div>
                </div>
                <br />
                <div class="sidebar-item range-slider">
                    <div class="item-title">
                        <h5><?php echo __('Departures', ST_TRAVELER_DIR); ?></h5>
                    </div>
                    <div class="item-content">
                        <input id="sttp-ranger-departure" type="text" name="sttp_departure" value="<?php if(isset($_GET['sttp_departure'])) echo $_GET['sttp_departure']; ?>">
                        <button type="submit" class="btn btn-primary btn-xs"><?php echo __('APPLY', 'st_travelport'); ?></button>
                    </div>
                </div>

                <br />
                <div class="sidebar-item range-slider">
                    <div class="item-title">
                        <h5><?php echo __('Duration', 'st_travelport'); ?></h5>
                    </div>
                    <div class="item-content">
                        <input id="sttp-ranger-duration" type="text" name="sttp_duration" value="<?php if(isset($_GET['sttp_duration'])) echo $_GET['sttp_duration']; ?>" data-duration="<?php echo implode(',', $filter_data['duration']); ?>">
                        <button type="submit" class="btn btn-primary btn-xs"><?php echo __('APPLY', 'st_travelport'); ?></button>
                    </div>
                </div>

            </form>
        </div>
        <div class="col-lg-9">
            <div class="sttp-sortby">
                <label><?php echo __('Sort by', 'st_travelport') ?>: </label>
                <!--Departure-->
                <div class="btn-group">
                    <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <?php echo __('Departure', 'st_travelport') ?> <i class="fa fa-caret-down"></i>
                    </button>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="<?php echo add_query_arg('sort_by', 'departure_early', $this_link) ?>"><?php echo __('Early', 'st_travelport') ?></a>
                        <a class="dropdown-item" href="<?php echo add_query_arg('sort_by', 'departure_late', $this_link) ?>"><?php echo __('Late', 'st_travelport') ?></a>
                    </div>
                </div>

                <!--Arrival-->
                <div class="btn-group">
                    <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <?php echo __('Arrival', 'st_travelport') ?> <i class="fa fa-caret-down"></i>
                    </button>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="<?php echo add_query_arg('sort_by', 'arrival_early', $this_link) ?>"><?php echo __('Early', 'st_travelport') ?></a>
                        <a class="dropdown-item" href="<?php echo add_query_arg('sort_by', 'arrival_late', $this_link) ?>"><?php echo __('Late', 'st_travelport') ?></a>
                    </div>
                </div>
                <!--Duration-->
                <div class="btn-group">
                    <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <?php echo __('Duration', 'st_travelport') ?> <i class="fa fa-caret-down"></i>
                    </button>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="<?php echo add_query_arg('sort_by', 'duration_lth', $this_link) ?>"><?php echo __('Low To High', 'st_travelport') ?></a>
                        <a class="dropdown-item" href="<?php echo add_query_arg('sort_by', 'duration_htl', $this_link) ?>"><?php echo __('High To Low', 'st_travelport') ?></a>
                    </div>
                </div>

                <!--Price-->
                <div class="btn-group">
                    <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <?php echo __('Price', 'st_travelport') ?> <i class="fa fa-caret-down"></i>
                    </button>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="<?php echo add_query_arg('sort_by', 'price_lth', $this_link) ?>"><?php echo __('Low To High', 'st_travelport') ?></a>
                        <a class="dropdown-item" href="<?php echo add_query_arg('sort_by', 'price_htl', $this_link) ?>"><?php echo __('High To Low', 'st_travelport') ?></a>
                    </div>
                </div>

                <div id="sttp-clear-filter">
                    <?php echo __('Clear filter', 'st_travelport') ?>
                </div>
            </div>
	        <?php
	        //if (!empty($slice)) {
	        //    foreach ($slice as $k => $v) {
	        if (!empty($posts)) {
		        foreach ($posts as $k => $v) {
			        $arr_carries = array();
			        $arr_show_carries = array();
			        ?>
                    <div class="item item-search" data-index="<?php echo $k; ?>">
                        <div class="row">
                            <div class="col-sm-10 program">
                                <div class="outbound-list">
							        <?php
							        $first_item = $v['FlightList'][0]['Options'][0];
							        //dd($first_item);
							        $itnObj = STTP_Search::inst()->getItnObj($first_item['BookingInfo'], $segments);
							        ?>
                                    <div class="itn" data-index="0">
                            <span class="time">
                                <?php
                                $dateTime = STTP_Search::inst()->getDateTime($itnObj['time'][0]);
                                echo $dateTime['time'];
                                ?>
                                <span class="date"><?php echo $dateTime['time_zone'] . '<br />' . $dateTime['date']; ?></span>
                            </span>
                                        <div class="int-item">
                                <span class="hour">
                                    <?php echo STTP_Search::inst()->formatTravelTime($first_item['Attributes']['TravelTime']); ?>
                                </span>
                                            <div class="int-process">
                                                <div class="flight-info">
											        <?php
											        for ($i = 0; $i < count($itnObj['proccess']) - 1; $i++) {
												        if(!in_array($itnObj['carrier'][$i], $arr_show_carries))
													        array_push($arr_show_carries, $itnObj['carrier'][$i]);
												        if(!in_array($itnObj['carrier'][$i], $arr_carries))
													        array_push($arr_carries, $itnObj['carrier'][$i]);
												        //$flight_data_info = $flights[$itnObj['segment'][$i]['FlightDetailsRef']];

												        ?>
                                                        <div class="f-item">
													        <?php
													        /*$atem = "Flight: " . $itnObj['segment'][$i]['FlightNumber'] . " /
															Class: " . $first_item['BookingInfo'][$i]['BookingCode'] . "<br />
															" . $first_item['BookingInfo'][$i]['CabinClass'] . "<br /><span class='sttp-cabin-class'>" . $flight_data_info['Equipment'] . "</span><br/>Flight Time: " . STTP_Search::inst()->minuteToHourMinute($flight_data_info['FlightTime']) . "<br/><span class='sttp-carrier'>Carrier: <span>" . $itnObj['carrier'][$i] . "</span></span>";
															*/?><!--

                                                <a data-toggle="tooltip" data-html="true" title="<?php /*echo $atem; */?>">More Info</a>-->
                                                            <?php echo __('Flight number', 'st_travelport') ?>: <?php echo $itnObj['segment'][$i]['FlightNumber']; ?>
                                                        </div>

												        <?php
											        }
											        ?>
                                                </div>
                                                <div class="flight-info-2">
											        <?php
											        for ($i = 0; $i < count($itnObj['proccess']) - 1; $i++) {
												        ?>
                                                        <div class="f-item">
                                                            <img src="<?php echo 'http://pics.avs.io/100/30/'. $itnObj['carrier'][$i] .'.png' ?>"/>
                                                        </div>
												        <?php
											        }
											        ?>
                                                </div>
										        <?php
										        if (!empty($itnObj['proccess'])) {
											        foreach ($itnObj['proccess'] as $kk => $vv) {
												        ?>
                                                        <span class="itn-circle">
                                                <a class="sttp-airport-tooltip" data-html="true" title="<?php echo $vv; ?>"><?php echo $vv; ?><span class="sttp-airport-name hidden"><?php echo $vv; ?></span></a>
                                                <!--<span class="sub-time">
                                                    <?php
/*                                                    if ($kk > 0 && $kk < count($itnObj['proccess']) - 1) {
	                                                    $dateTime = STTP_Search::inst()->getDateTime($itnObj['time'][$kk]);
	                                                    $dateTime1 = STTP_Search::inst()->getDateTime($itnObj['time'][$kk + 1]);
	                                                    echo '<span>' . $dateTime['time'];
	                                                    echo '<small>' . $dateTime['time_zone'] . '</small></span> - ';
	                                                    echo '<span>' . $dateTime1['time'];
	                                                    echo '<small>' . $dateTime1['time_zone'] . '</small></span>';

                                                    }
                                                    */?>
                                                </span>-->
                                            </span>
												        <?php
											        }
										        }
										        ?>
                                            </div>
                                        </div>
                                        <span class="time">
                                <?php
                                $dateTime = STTP_Search::inst()->getDateTime($itnObj['time'][count($itnObj['time']) - 1]);
                                echo $dateTime['time'];
                                ?>
                                            <span class="date"><?php echo $dateTime['time_zone'] . '<br />' . $dateTime['date']; ?></span>
                            </span>

                                        <div class="sttp-selection">
                                            <div class="st-icheck-item">
                                                <label><input type="checkbox" name="sttp_selection" value="1" data-way="<?php echo $flight_way; ?>"><span
                                                            class="checkmark"></span></label>
                                            </div>
									        <?php if(!empty($fly_back) && $flight_way == 'roundtrip'){
										        $inbound = count($v['FlightList'][1]['Options']);
										        ?>
                                                <div class="inbound-data">
                                    <span>
                                        <?php echo __('Inbound options', 'st_travelport') ?>
                                    </span>
                                                    <span class="inbound-num">
                                        <?php echo $inbound; ?>
                                    </span>
                                                </div>
									        <?php } ?>
                                        </div>
                                        <a href="#" class="sttp-flight-details" data-toggle="modal" data-target="#stFlightDetails<?php echo $k . '0outbound' ?>"><?php echo __('Flight Details', 'st_travelport') ?></a>
                                        <!-- Modal -->
                                        <div class="modal fade" id="stFlightDetails<?php echo $k . '0outbound' ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel"><?php echo __('Flight Details', 'st_travelport') ?></h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
												        <?php
												        if(!empty($first_item['BookingInfo'])){
													        ?>
                                                            <ul class="nav nav-tabs">
                                                                <li class="active"><a data-toggle="tab" href="#pflight-details<?php echo $k . '0outbound' ?>"><?php echo __('Flight Details', 'st_travelport') ?></a></li>
                                                                <li><a data-toggle="tab" href="#pfare-info<?php echo $k . '0outbound' ?>"><?php echo __('Fare Info', 'st_travelport') ?></a></li>
                                                            </ul>

                                                            <div class="tab-content">
                                                                <div id="pflight-details<?php echo $k . '0outbound' ?>" class="tab-pane fade in active">
															        <?php
															        foreach ($first_item['BookingInfo'] as $a => $b){
																        $flight_data_info = $flights[$itnObj['segment'][$a]['FlightDetailsRef']];
																        ?>
                                                                        <div class="popup-item">
                                                                            <div class="row">
                                                                                <div class="col-lg-3">
                                                                                    <img src="<?php echo 'http://pics.avs.io/100/30/'. $itnObj['carrier'][$a] .'.png' ?>"/>
                                                                                    <p><span class="sttp-carrier"><?php echo $itnObj['carrier'][$a]; ?></span></p>
                                                                                </div>
                                                                                <div class="col-lg-3">
                                                                                    <p><?php echo $flight_data_info['Origin'] . ' -> ' . $flight_data_info['Destination']; ?></p>
                                                                                </div>
                                                                                <div class="col-lg-6">
                                                                                    <p><?php echo __('Flight Number', 'st_travelport') ?>: <?php echo $itnObj['segment'][$a]['FlightNumber']; ?></p>
                                                                                    <p><?php echo __('Class', 'st_travelport') ?>: <?php echo $first_item['BookingInfo'][$a]['BookingCode'] . ' / ' . $first_item['BookingInfo'][$a]['CabinClass']; ?></p>
                                                                                    <p><?php echo __('Flight Time', 'st_travelport') ?>: <?php echo STTP_Search::inst()->minuteToHourMinute($flight_data_info['FlightTime']); ?></p>
                                                                                    <p><?php echo __('Equipment', 'st_travelport') ?>: <span class="sttp-cabin-class"><?php echo $flight_data_info['Equipment']; ?></span></p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
																        <?php
															        }
															        ?>
                                                                </div>
                                                                <div id="pfare-info<?php echo $k . '0outbound' ?>" class="tab-pane fade">
															        <?php
															        $fareData = $fares[$first_item['BookingInfo'][0]['FareInfoRef']];
															        ?>
                                                                    <div class="popup-item">
                                                                        <div class="row">

                                                                            <div class="col-lg-6">
                                                                                <p><?php echo __('Fare Basis', 'st_travelport') ?>: <?php echo $fareData['FareBasis']; ?></p>
                                                                                <p><?php echo __('Passenger Code', 'st_travelport') ?>: <?php echo $fareData['PassengerTypeCode']; ?></p>
                                                                                <p><?php echo __('Effective Date', 'st_travelport') ?>: <?php echo STTP_Search::inst()->getDateTime($fareData['EffectiveDate'], 'Y-m-d')['date']; ?></p>
                                                                                <p><?php echo __('Departure Date', 'st_travelport') ?>: <?php echo $fareData['DepartureDate']; ?></p>
                                                                            </div>
                                                                            <div class="col-lg-6">
                                                                                <p><?php echo __('Amount', 'st_travelport') ?>: <?php echo $fareData['Amount']; ?></p>
                                                                                <p><?php echo __('Not Valid Before', 'st_travelport') ?>: <?php echo $fareData['NotValidBefore']; ?></p>
                                                                                <p><?php echo __('Not Valid After', 'st_travelport') ?>: <?php echo $fareData['NotValidAfter']; ?></p>
																		        <?php
																		        if((isset($fareData['BaggageAllowance']) && !empty($fareData['BaggageAllowance'])) || (isset($fareData['NumberOfPieces']) && !empty($fareData['NumberOfPieces']))){
																		            ?>
																		            <p><?php echo __('BaggageAllowance', 'st_travelport') ?></p>
                                                                                    <ul>
                                                                                    <?php
																			        if(isset($fareData['BaggageAllowance']['MaxWeight']) && !empty($fareData['BaggageAllowance']['MaxWeight'])){
																				        ?>
                                                                                        <li><?php echo __('Max Weight', 'st_travelport') ?>: <?php echo $fareData['BaggageAllowance']['MaxWeight']['Value'] . ' ' . $fareData['BaggageAllowance']['MaxWeight']['Unit']; ?></li>
																				        <?php
																			        }
                                                                                    if(isset($fareData['BaggageAllowance']['NumberOfPieces']) && !empty($fareData['BaggageAllowance']['NumberOfPieces'])){
                                                                                        ?>
                                                                                        <li><?php echo __('Number of Pieces', 'st_travelport') ?>: <?php echo $fareData['BaggageAllowance']['NumberOfPieces']; ?></li>
                                                                                        <?php
                                                                                    }
																			        ?>
                                                                                    </ul>
                                                                                    <?php
																		        }
																		        ?>

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
													        <?php
												        }
												        ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
							        <?php
							        if (count($v['FlightList'][0]['Options']) > 1) {
								        ?>
                                        <div class="sttp-more-option col-lg-10">
                                <span><span class="subset">+</span> <?php echo count($v['FlightList'][0]['Options']) - 1; ?>
                                    <?php echo __('more options same price', 'st_travelport') ?></span>
                                        </div>
                                        <div class="sub-item-wrapper">
									        <?php
									        for ($i = 1; $i < count($v['FlightList'][0]['Options']); $i++) {
										        $remain_item = $v['FlightList'][0]['Options'][$i];
										        $itnObj = STTP_Search::inst()->getItnObj($remain_item['BookingInfo'], $segments);
										        ?>
                                                <div class="sub-item">
                                                    <div class="itn" data-index="<?php echo $i; ?>">
                                        <span class="time">
                                            <?php
                                            $dateTime = STTP_Search::inst()->getDateTime($itnObj['time'][0]);
                                            echo $dateTime['time'];
                                            ?>
                                            <span class="date"><?php echo $dateTime['time_zone'] . '<br />' . $dateTime['date']; ?></span>
                                        </span>
                                                        <div class="int-item">
                                            <span class="hour">
                                                <?php echo STTP_Search::inst()->formatTravelTime($remain_item['Attributes']['TravelTime']); ?>
                                            </span>
                                                            <div class="int-process">
                                                                <div class="flight-info">
															        <?php
															        for ($ii = 0; $ii < count($itnObj['proccess']) - 1; $ii++) {
																        if(!in_array($itnObj['carrier'][$ii], $arr_carries))
																	        array_push($arr_carries, $itnObj['carrier'][$ii]);
																        $flight_data_info = $flights[$itnObj['segment'][$ii]['FlightDetailsRef']];
																        ?>
                                                                        <div class="f-item">
																	        <?php
																	        /*$atem = "Flight: " . $itnObj['segment'][$ii]['FlightNumber'] . " /
															Class: " . $remain_item['BookingInfo'][$ii]['BookingCode'] . "<br />
															" . $remain_item['BookingInfo'][$ii]['CabinClass'] . "<br /><span class='sttp-cabin-class'>" . $flight_data_info['Equipment'] . "</span><br/>Flight Time: " . STTP_Search::inst()->minuteToHourMinute($flight_data_info['FlightTime']) . "<br/><span class='sttp-carrier'>Carrier: <span>" . $itnObj['carrier'][$ii] . "</span></span>";
																			*/?><!--

                                                                <a data-toggle="tooltip" data-html="true" title="<?php /*echo $atem; */?>">More Info</a>-->
                                                                            <?php echo __('Flight number', 'st_travelport') ?>: <?php echo $itnObj['segment'][$ii]['FlightNumber']; ?>
                                                                        </div>
																        <?php
															        }
															        ?>
                                                                </div>
                                                                <div class="flight-info-2">
															        <?php
															        for ($ii = 0; $ii < count($itnObj['proccess']) - 1; $ii++) {
																        ?>
                                                                        <div class="f-item">
                                                                            <img src="<?php echo 'http://pics.avs.io/100/30/'. $itnObj['carrier'][$ii] .'.png' ?>"/>
                                                                        </div>
																        <?php
															        }
															        ?>
                                                                </div>
														        <?php
														        if (!empty($itnObj['proccess'])) {
															        foreach ($itnObj['proccess'] as $kk => $vv) {
																        ?>
                                                                        <span class="itn-circle">
                                                                <a class="sttp-airport-tooltip" data-html="true" title="<?php echo $vv; ?>"><?php echo $vv; ?><span class="sttp-airport-name hidden"><?php echo $vv; ?></span></a>
                                                                <!--<span class="sub-time">
                                                                    <?php
/*                                                                    if ($kk > 0 && $kk < count($itnObj['proccess']) - 1) {
	                                                                    $dateTime = STTP_Search::inst()->getDateTime($itnObj['time'][$kk]);
	                                                                    $dateTime1 = STTP_Search::inst()->getDateTime($itnObj['time'][$kk + 1]);
	                                                                    echo '<span>' . $dateTime['time'];
	                                                                    echo '<small>' . $dateTime['time_zone'] . '</small></span> - ';
	                                                                    echo '<span>' . $dateTime1['time'];
	                                                                    echo '<small>' . $dateTime1['time_zone'] . '</small></span>';

                                                                    }
                                                                    */?>
                                                                </span>-->
                                                            </span>
																        <?php
															        }
														        }
														        ?>
                                                            </div>
                                                        </div>
                                                        <span class="time">
                                            <?php
                                            $dateTime = STTP_Search::inst()->getDateTime($itnObj['time'][count($itnObj['time']) - 1]);
                                            echo $dateTime['time'];
                                            ?>
                                                            <span class="date"><?php echo $dateTime['time_zone'] . '<br />' . $dateTime['date']; ?></span>
                                        </span>
                                                        <div class="sttp-selection">
                                                            <div class="st-icheck-item">
                                                                <label><input type="checkbox" name="sttp_selection" value="1" data-way="<?php echo $flight_way; ?>"><span
                                                                            class="checkmark"></span></label>
                                                            </div>
													        <?php if(!empty($fly_back) && $flight_way == 'roundtrip'){ ?>
                                                                <div class="inbound-data">
                                                    <span>
                                                       <?php echo __(' Inbound options', 'st_travelport') ?>
                                                    </span>
                                                                    <span class="inbound-num">
                                                        <?php echo $inbound; ?>
                                                    </span>
                                                                </div>
													        <?php } ?>
                                                        </div>
                                                        <a href="#" class="sttp-flight-details" data-toggle="modal" data-target="#stFlightDetails<?php echo $k . $i . 'outbound' ?>">Flight Details</a>
                                                        <!-- Modal -->
                                                        <div class="modal fade" id="stFlightDetails<?php echo $k . $i . 'outbound' ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel"><?php echo __('Flight Details', 'st_travelport') ?></h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body">
																        <?php
																        if(!empty($remain_item['BookingInfo'])){
																	        ?>
                                                                            <ul class="nav nav-tabs">
                                                                                <li class="active"><a data-toggle="tab" href="#pflight-details<?php echo $k . $i . 'outbound' ?>">Flight Details</a></li>
                                                                                <li><a data-toggle="tab" href="#pfare-info<?php echo $k . $i . 'outbound' ?>"><?php echo __('Fare Info', 'st_travelport'); ?></a></li>
                                                                            </ul>

                                                                            <div class="tab-content">
                                                                                <div id="pflight-details<?php echo $k . $i . 'outbound' ?>" class="tab-pane fade in active">
																			        <?php
																			        foreach ($remain_item['BookingInfo'] as $a => $b){
																				        $flight_data_info = $flights[$itnObj['segment'][$a]['FlightDetailsRef']];
																				        ?>
                                                                                        <div class="popup-item">
                                                                                            <div class="row">
                                                                                                <div class="col-lg-3">
                                                                                                    <img src="<?php echo 'http://pics.avs.io/100/30/'. $itnObj['carrier'][$a] .'.png' ?>"/>
                                                                                                    <p><span class="sttp-carrier"><?php echo $itnObj['carrier'][$a]; ?></span></p>
                                                                                                </div>
                                                                                                <div class="col-lg-3">
                                                                                                    <p><?php echo $flight_data_info['Origin'] . ' -> ' . $flight_data_info['Destination']; ?></p>
                                                                                                </div>
                                                                                                <div class="col-lg-6">
                                                                                                    <p><?php echo __('Flight Number', 'st_travelport') ?>: <?php echo $itnObj['segment'][$a]['FlightNumber']; ?></p>
                                                                                                    <p><?php echo __('Class', 'st_travelport') ?>: <?php echo $remain_item['BookingInfo'][$a]['BookingCode'] . ' / ' . $remain_item['BookingInfo'][$a]['CabinClass']; ?></p>
                                                                                                    <p><?php echo __('Flight Time', 'st_travelport') ?>: <?php echo STTP_Search::inst()->minuteToHourMinute($flight_data_info['FlightTime']); ?></p>
                                                                                                    <p><?php echo __('Equipment', 'st_travelport') ?>: <span class="sttp-cabin-class"><?php echo $flight_data_info['Equipment']; ?></span></p>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
																				        <?php
																			        }
																			        ?>
                                                                                </div>
                                                                                <div id="pfare-info<?php echo $k . $i . 'outbound' ?>" class="tab-pane fade">
																			        <?php
																			        $fareData = $fares[$remain_item['BookingInfo'][0]['FareInfoRef']];
																			        ?>
                                                                                    <div class="popup-item">
                                                                                        <div class="row">

                                                                                            <div class="col-lg-6">
                                                                                                <p><?php echo __('Fare Basis', 'st_travelport') ?>: <?php echo $fareData['FareBasis']; ?></p>
                                                                                                <p><?php echo __('Passenger Code', 'st_travelport') ?>: <?php echo $fareData['PassengerTypeCode']; ?></p>
                                                                                                <p><?php echo __('Effective Date', 'st_travelport') ?>: <?php echo STTP_Search::inst()->getDateTime($fareData['EffectiveDate'], 'Y-m-d')['date']; ?></p>
                                                                                                <p><?php echo __('Departure Date', 'st_travelport') ?>: <?php echo $fareData['DepartureDate']; ?></p>
                                                                                            </div>
                                                                                            <div class="col-lg-6">
                                                                                                <p><?php echo __('Amount', 'st_travelport') ?>: <?php echo $fareData['Amount']; ?></p>
                                                                                                <p><?php echo __('Not Valid Before', 'st_travelport') ?>: <?php echo $fareData['NotValidBefore']; ?></p>
                                                                                                <p><?php echo __('Not Valid After', 'st_travelport') ?>: <?php echo $fareData['NotValidAfter']; ?></p>
																						        <?php
																						        if(isset($fareData['BaggageAllowance']) && !empty($fareData['BaggageAllowance'])){
																							        if(isset($fareData['BaggageAllowance']['MaxWeight']) && !empty($fareData['BaggageAllowance']['MaxWeight'])){
																								        ?>
                                                                                                        <p><?php echo __('Max Weight', 'st_travelport') ?>: <?php echo $fareData['BaggageAllowance']['MaxWeight']['Value'] . ' ' . $fareData['BaggageAllowance']['MaxWeight']['Unit']; ?></p>
																								        <?php
																							        }
																						        }
																						        ?>

                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
																	        <?php
																        }
																        ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
									        <?php } ?>
                                        </div>
								        <?php
							        }
							        ?>
                                    <input type="hidden" class="sttp-show-by-carries" value="<?php echo implode(',', $arr_show_carries); ?>"/>
                                </div>
						        <?php if(!empty($fly_back) && $flight_way == 'roundtrip'){ ?>
                                    <div class="inbound-list">
                                        <span class="text-center title"><?php echo __('Inbound List', 'st_travelport') ?></span>
								        <?php
								        for ($i = 0; $i < count($v['FlightList'][1]['Options']); $i++) {
									        $remain_item = $v['FlightList'][1]['Options'][$i];
									        $itnObj = STTP_Search::inst()->getItnObj($remain_item['BookingInfo'], $segments);
									        ?>
                                            <div class="sub-item">
                                                <div class="itn" data-index="<?php echo $i; ?>">
                                        <span class="time">
                                            <?php
                                            $dateTime = STTP_Search::inst()->getDateTime($itnObj['time'][0]);
                                            echo $dateTime['time'];
                                            ?>
                                            <span class="date"><?php echo $dateTime['time_zone'] . '<br />' . $dateTime['date']; ?></span>
                                        </span>
                                                    <div class="int-item">
                                            <span class="hour">
                                                <?php echo STTP_Search::inst()->formatTravelTime($remain_item['Attributes']['TravelTime']); ?>
                                            </span>
                                                        <div class="int-process">
                                                            <div class="flight-info">
														        <?php
														        for ($ii = 0; $ii < count($itnObj['proccess']) - 1; $ii++) {
															        $flight_data_info = $flights[$itnObj['segment'][$ii]['FlightDetailsRef']];
															        ?>
                                                                    <div class="f-item">
																        <?php
																        /*$atem = "Flight: " . $itnObj['segment'][$ii]['FlightNumber'] . " /
															Class: " . $remain_item['BookingInfo'][$ii]['BookingCode'] . "<br />
															" . $remain_item['BookingInfo'][$i]['CabinClass'] . "<br /><span class='sttp-cabin-class'>" . $flight_data_info['Equipment'] . "</span><br/>Flight Time: " . STTP_Search::inst()->minuteToHourMinute($flight_data_info['FlightTime']) . "<br/><span class='sttp-carrier'>Carrier: <span>" . $itnObj['carrier'][$ii] . "</span></span>";
																		*/?><!--

                                                            <a data-toggle="tooltip" data-html="true" title="<?php /*echo $atem; */?>">More Info</a>-->
                                                                        <?php echo __('Flight number', 'st_travelport') ?>: <?php echo $itnObj['segment'][$ii]['FlightNumber']; ?>
                                                                    </div>
															        <?php
														        }
														        ?>
                                                            </div>
                                                            <div class="flight-info-2">
														        <?php
														        for ($ii = 0; $ii < count($itnObj['proccess']) - 1; $ii++) {
															        ?>
                                                                    <div class="f-item">
                                                                        <img src="<?php echo 'http://pics.avs.io/100/30/'. $itnObj['carrier'][$ii] .'.png' ?>"/>
                                                                    </div>
															        <?php
														        }
														        ?>
                                                            </div>
													        <?php
													        if (!empty($itnObj['proccess'])) {
														        foreach ($itnObj['proccess'] as $kk => $vv) {
															        ?>
                                                                    <span class="itn-circle">
                                                                <a class="sttp-airport-tooltip" data-html="true" title="<?php echo $vv; ?>"><?php echo $vv; ?><span class="sttp-airport-name hidden"><?php echo $vv; ?></span></a>
                                                            <!--<span class="sub-time">
                                                                    <?php
/*                                                                    if ($kk > 0 && $kk < count($itnObj['proccess']) - 1) {
	                                                                    $dateTime = STTP_Search::inst()->getDateTime($itnObj['time'][$kk]);
	                                                                    $dateTime1 = STTP_Search::inst()->getDateTime($itnObj['time'][$kk + 1]);
	                                                                    echo '<span>' . $dateTime['time'];
	                                                                    echo '<small>' . $dateTime['time_zone'] . '</small></span> - ';
	                                                                    echo '<span>' . $dateTime1['time'];
	                                                                    echo '<small>' . $dateTime1['time_zone'] . '</small></span>';

                                                                    }
                                                                    */?>
                                                                </span>-->
                                                            </span>
															        <?php
														        }
													        }
													        ?>
                                                        </div>
                                                    </div>
                                                    <span class="time">
                                            <?php
                                            $dateTime = STTP_Search::inst()->getDateTime($itnObj['time'][count($itnObj['time']) - 1]);
                                            echo $dateTime['time'];
                                            ?>
                                                        <span class="date"><?php echo $dateTime['time_zone'] . '<br />' . $dateTime['date']; ?></span>
                                        </span>
                                                    <div class="sttp-selection">
                                                        <div class="st-icheck-item">
                                                            <label><input type="radio" name="sttp_selection_<?php echo $k; ?>" value=""><span
                                                                        class="checkmark"></span></label>
                                                        </div>
                                                    </div>
                                                    <a href="#" class="sttp-flight-details" data-toggle="modal" data-target="#stFlightDetails<?php echo $k . $i . 'inbound' ?>"><?php echo __('Flight Details', 'st_travelport') ?></a>
                                                    <!-- Modal -->
                                                    <div class="modal fade" id="stFlightDetails<?php echo $k . $i . 'inbound' ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title" id="exampleModalLabel"><?php echo __('Flight Details', 'st_travelport') ?></h5>
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body">
															        <?php
															        if(!empty($remain_item['BookingInfo'])){
																        ?>
                                                                        <ul class="nav nav-tabs">
                                                                            <li class="active"><a data-toggle="tab" href="#pflight-details<?php echo $k . $i . 'inbound' ?>">Flight Details</a></li>
                                                                            <li><a data-toggle="tab" href="#pfare-info<?php echo $k . $i . 'inbound' ?>"><?php echo __('Fare Info', 'st_travelport') ?></a></li>
                                                                        </ul>

                                                                        <div class="tab-content">
                                                                            <div id="pflight-details<?php echo $k . $i . 'inbound' ?>" class="tab-pane fade in active">
																		        <?php
																		        foreach ($remain_item['BookingInfo'] as $a => $b){
																			        $flight_data_info = $flights[$itnObj['segment'][$a]['FlightDetailsRef']];
																			        ?>
                                                                                    <div class="popup-item">
                                                                                        <div class="row">
                                                                                            <div class="col-lg-3">
                                                                                                <img src="<?php echo 'http://pics.avs.io/100/30/'. $itnObj['carrier'][$a] .'.png' ?>"/>
                                                                                                <p><span class="sttp-carrier"><?php echo $itnObj['carrier'][$a]; ?></span></p>
                                                                                            </div>
                                                                                            <div class="col-lg-3">
                                                                                                <p><?php echo $flight_data_info['Origin'] . ' -> ' . $flight_data_info['Destination']; ?></p>
                                                                                            </div>
                                                                                            <div class="col-lg-6">
                                                                                                <p><?php echo __('Flight Number', 'st_travelport') ?>: <?php echo $itnObj['segment'][$a]['FlightNumber']; ?></p>
                                                                                                <p><?php echo __('Class', 'st_travelport') ?>: <?php echo $remain_item['BookingInfo'][$a]['BookingCode'] . ' / ' . $remain_item['BookingInfo'][$a]['CabinClass']; ?></p>
                                                                                                <p><?php echo __('Flight Time', 'st_travelport') ?>: <?php echo STTP_Search::inst()->minuteToHourMinute($flight_data_info['FlightTime']); ?></p>
                                                                                                <p><?php echo __('Equipment', 'st_travelport') ?>: <span class="sttp-cabin-class"><?php echo $flight_data_info['Equipment']; ?></span></p>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
																			        <?php
																		        }
																		        ?>
                                                                            </div>
                                                                            <div id="pfare-info<?php echo $k . $i . 'inbound' ?>" class="tab-pane fade">
																		        <?php
																		        $fareData = $fares[$remain_item['BookingInfo'][0]['FareInfoRef']];
																		        ?>
                                                                                <div class="popup-item">
                                                                                    <div class="row">

                                                                                        <div class="col-lg-6">
                                                                                            <p><?php echo __('Fare Basis', 'st_travelport') ?>: <?php echo $fareData['FareBasis']; ?></p>
                                                                                            <p><?php echo __('Passenger Code', 'st_travelport') ?>: <?php echo $fareData['PassengerTypeCode']; ?></p>
                                                                                            <p><?php echo __('Effective Date', 'st_travelport') ?>: <?php echo STTP_Search::inst()->getDateTime($fareData['EffectiveDate'], 'Y-m-d')['date']; ?></p>
                                                                                            <p><?php echo __('Departure Date', 'st_travelport') ?>: <?php echo $fareData['DepartureDate']; ?></p>
                                                                                        </div>
                                                                                        <div class="col-lg-6">
                                                                                            <p><?php echo __('Amount', 'st_travelport') ?>: <?php echo $fareData['Amount']; ?></p>
                                                                                            <p><?php echo __('Not Valid Before', 'st_travelport') ?>: <?php echo $fareData['NotValidBefore']; ?></p>
                                                                                            <p><?php echo __('Not Valid After', 'st_travelport') ?>: <?php echo $fareData['NotValidAfter']; ?></p>
																					        <?php
																					        if(isset($fareData['BaggageAllowance']) && !empty($fareData['BaggageAllowance'])){
																						        if(isset($fareData['BaggageAllowance']['MaxWeight']) && !empty($fareData['BaggageAllowance']['MaxWeight'])){
																							        ?>
                                                                                                    <p><?php echo __('Max Weight', 'st_travelport') ?>: <?php echo $fareData['BaggageAllowance']['MaxWeight']['Value'] . ' ' . $fareData['BaggageAllowance']['MaxWeight']['Unit']; ?></p>
																							        <?php
																						        }
																					        }
																					        ?>

                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
																        <?php
															        }
															        ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
								        <?php } ?>
                                    </div>
						        <?php } ?>
                            </div>
                            <div class="col-sm-2 info">
                                <div class="price">
                                    <!--1.031,02 <span>GBP</span>-->
							        <?php echo st_travelport_format_currency($v['Attributes']['TotalPrice']); ?>
                                </div>
                                <div class="carret">
							        <?php
							        //dd($arr_carries);
							        foreach ($arr_carries as $kkk => $vvv){
								        if(!in_array($vvv, $arr_carries_total)){
									        array_push($arr_carries_total, $vvv);
								        }
							        }
							        ?>
                                    <!--<img src="https://goprivate.wspan.com/sharedservices/images/airlineimages/logoAirEK.gif"
										 class="img-responsive"/>
									<span>Vietnam Airline</span>-->
                                    <button class="sttp-add-to-cart"><?php echo __('Add to cart', 'st_travelport'); ?> <i class="fa fa-spinner fa-spin"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
			        <?php
		        }
	        }

	        if($total_rows > $per_page) {
		        //echo st_travelport_load_view( 'pagination', array( 'pages' => $pages, 'current_page' => $current_page ) );
	        }
	        ?>

            <?php
            $data_carries = array();
            if(!empty($arr_carries_total)){
                foreach ($arr_carries_total as $k => $v){
                    array_push($data_carries, $v . '|' . 'http://pics.avs.io/200/50/'. $v .'.png');
                }
            }
            echo '<input type="hidden" class="sttp-data-carries" value="'. implode(',', $data_carries) .'"/>';
            ?>
        </div>

    </div>
</div>
    <div id="sttp-currency-symbol" data-value="<?php echo get_option('st_travelport_currency'); ?>" style="display: none"></div>
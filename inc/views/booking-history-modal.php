<?php
$booking = json_decode($res['booking_info'], true);
$order_id = $res['order_id'];

$passenger = json_decode($res['checkout_info'], true);
?>
<div>
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab"><?php echo __('Detail', 'st_travelport'); ?></a></li>
        <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab"><?php echo __('Passenger', 'st_travelport'); ?></a></li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="home">
            <br />
            <h4><b class="fw450"><?php echo __('Itinerary', 'st_travelport'); ?></b></h4><br />
	        <?php
	        if ( ! empty( $booking['cartOutBound'] ) ) {
		        echo '<b class="fw450 sttp-mb-20 clearfix">'. __('OutBound', 'st_travelport') .'</b><br />';
		        if ( ! empty( $booking['cartOutBound']['BookingInfo'] ) ) {
			        foreach ( $booking['cartOutBound']['BookingInfo'] as $k => $v ) {
				        $seg          = $v['SegmentRef'];
				        $depart_time  = STTP_Search::inst()->getDateTime( $seg['DepartureTime'] );
				        $arrival_time = STTP_Search::inst()->getDateTime( $seg['ArrivalTime'] );
				        ?>
                        <p>
                            <b class="fw450"><?php echo '<span class="sttp-airport-name">' . $seg['Origin'] . '</span> (' . $seg['Origin'] . ') -> <span class="sttp-airport-name">' . $seg['Destination'] . '</span> (' . $seg['Destination'] . ')'; ?></b>
                        </p>
                        <p>
                            <?php echo __('Departure', 'st_travelport'); ?>: <?php echo $depart_time['time'] . ' <small>(' . $depart_time['time_zone'] . ')</small> ' . $depart_time['date']; ?></p>
                        <p>
                            <?php echo __('Arrival', 'st_travelport'); ?>: <?php echo $arrival_time['time'] . ' <small>(' . $arrival_time['time_zone'] . ')</small> ' . $arrival_time['date']; ?></p>
                        <p>
                            <?php
                            if(!empty($passenger) && isset($passenger[$seg['Origin'] . '|' . $seg['Destination']])){
                                echo '<h5>'. __('Seat of passengers', 'st_travelport') .'</h5>';
                                foreach ($passenger[$seg['Origin'] . '|' . $seg['Destination']] as $kk => $vv){
                                    if($vv['coderef'] == $seg['Origin'] . '|' . $seg['Destination']){
                                        echo '<p>' . $vv['fullName'] . ': ' . $vv['seatCode'] . '</p>';
                                    }
                                }
                                echo '<br />';
                            }
                            ?>
                        </p>
				        <?php

			        }
		        }
	        }

            if ( ! empty( $booking['cartInBound'] ) ) {
            ?>
            <hr />
		        <?php
	            echo '<b class="fw450 sttp-mb-20 clearfix">'. __('InBound', 'st_travelport') .'</b><br />';
		        if ( ! empty( $booking['cartInBound']['BookingInfo'] ) ) {
			        foreach ( $booking['cartInBound']['BookingInfo'] as $k => $v ) {
				        $seg          = $v['SegmentRef'];
				        $depart_time  = STTP_Search::inst()->getDateTime( $seg['DepartureTime'] );
				        $arrival_time = STTP_Search::inst()->getDateTime( $seg['ArrivalTime'] );
				        ?>
                        <p><b class="fw450"><?php echo '<span class="sttp-airport-name">' . $seg['Origin'] . '</span> (' . $seg['Origin'] . ') -> <span class="sttp-airport-name">' . $seg['Destination'] . '</span> (' . $seg['Destination'] . ')'; ?></b></p>
                        <p>
                            <?php echo __('Departure', 'st_travelport'); ?>: <?php echo $depart_time['time'] . ' <small>(' . $depart_time['time_zone'] . ')</small> ' . $depart_time['date']; ?></p>
                        <p>
                            <?php echo __('Arrival', 'st_travelport'); ?>: <?php echo $arrival_time['time'] . ' <small>(' . $arrival_time['time_zone'] . ')</small> ' . $arrival_time['date']; ?></p>
                        <p>
					        <?php
					        if(!empty($passenger) && isset($passenger[$seg['Origin'] . '|' . $seg['Destination']])){
						        echo '<h5>'. __('Seat of passengers', 'st_travelport') .'</h5>';
						        foreach ($passenger[$seg['Origin'] . '|' . $seg['Destination']] as $kk => $vv){
							        if($vv['coderef'] == $seg['Origin'] . '|' . $seg['Destination']){
								        echo '<p>' . $vv['fullName'] . ': ' . $vv['seatCode'] . '</p>';
							        }
						        }
						        echo '<br />';
					        }
					        ?>
                        </p>
				        <?php

			        }
		        }
	        }
	        ?>
        </div>
        <div role="tabpanel" class="tab-pane" id="profile">
            <br />
            <h4><?php echo __('Creator Information', 'st_travelport') ?></h4>
            <p><?php echo __('Full Name', 'st_travelport') ?>: <?php echo get_post_meta($order_id, 'st_first_name', true) . ' ' . get_post_meta($order_id, 'st_last_name', true) ?></p>
            <p><?php echo __('Email', 'st_travelport') ?>: <?php echo get_post_meta($order_id, 'st_email', true); ?></p>
            <p><?php echo __('Phone', 'st_travelport') ?>: <?php echo get_post_meta($order_id, 'st_phone', true); ?></p>
            <p><?php echo __('Address', 'st_travelport') ?>: <?php echo get_post_meta($order_id, 'st_address', true); ?></p>
            <p><?php echo __('City', 'st_travelport') ?>: <?php echo get_post_meta($order_id, 'st_city', true); ?></p>
            <p><?php echo __('Country', 'st_travelport') ?>: <?php echo get_post_meta($order_id, 'st_country', true); ?></p>
            <br />
            <h4><?php echo __('Passenger', 'st_travelport') ?></h4>
            <?php
            $traveler = STTP_Search::inst()->getBookingTravelerData($order_id);
            echo '<table class="sttp-history-passenger">';
            echo '<tr>';
            echo '<th>'. __('Type', 'st_travelport') .'</th>';
            echo '<th>'. __('Full Name', 'st_travelport') .'</th>';
            echo '<th>'. __('Email', 'st_travelport') .'</th>';
            echo '<th>'. __('Phone', 'st_travelport') .'</th>';
            echo '<th>'. __('Age', 'st_travelport') .'</th>';
            echo '</tr>';
            foreach ($traveler as $k => $v){
                echo '<tr>';
                    echo '<td>'. $v['type'] .'</td>';
                    echo '<td>'. $v['first_name'][0] . ' ' . $v['last_name'][0] .'</td>';
                    echo '<td>'. $v['email'][0] .'</td>';
                    echo '<td>'. $v['phone'][0] .'</td>';
                    echo '<td>';
                    if(isset($v['age'])){
                        echo $v['age'];
                    }
                    echo '</td>';
                echo '</tr>';
            }
            echo '</table>';
            ?>
        </div>
    </div>

</div>
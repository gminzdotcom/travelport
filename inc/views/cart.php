<?php
/**
 * Created by PhpStorm.
 * User: Jream
 * Date: 4/7/2019
 * Time: 8:12 AM
 */
$cart = $_SESSION['sttp_cart'];
$data = STTP_Search::inst()->getAirPrice();
?>
<div class="sttp-cart">
    <?php if(!empty($data['warning']['Warning'])){ ?>
	<div class="warning">
		<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
			<div class="panel panel-default">
				<div class="panel-heading" role="tab" id="headingOne">
					<h4 class="panel-title">
						<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
							Warning
						</a>
					</h4>
				</div>
				<div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
					<div class="panel-body">
						<?php

							foreach ($data['warning']['Warning'] as $k => $v){
								echo '<p>'. $v['Content'][0] .'</p>';
							}

						?>
					</div>
				</div>
			</div>
		</div>
	</div>
    <?php } ?>


	<div class="outbound-list item-list">
		<?php
		foreach ($data['data'] as $k => $v){
			if($k == 'OutBound'){
				echo '<h3 class="title">OutBound</h3>';
			}else{
				echo '<h3 class="title">InBound</h3>';
			}
			foreach ($v as $kk => $vv) {
				?>
				<div class="item">
					<div class="item-carrier">
                        <?php
                        echo '<img src="http://pics.avs.io/100/30/'. $vv['Attributes']['Carrier'] .'.png"/>';
                        ?>
						<?php echo $vv['CodeshareInfoName'][0]; ?><br/>
						<!--Class: --><?php /*//echo $v['CabinClass'];*/
						?>
					</div>
					<div class="item-date">
						<?php
						$start = STTP_Search::inst()->getDateTime( $vv['Attributes']['DepartureTime'], 'd/m/Y' );
						$end   = STTP_Search::inst()->getDateTime( $vv['Attributes']['ArrivalTime'], 'd/m/Y' );
						?>
						<span class="date"><?php echo $start['date']; ?></span><br/>
						<b>Depart:</b>
						<span class="time"><?php echo $start['time']; ?>
							<small><?php echo $start['time_zone']; ?></small></span><br/>
						<b>Arrive:</b>
						<span class="time"><?php echo $end['time']; ?>
							<small><?php echo $end['time_zone']; ?></small></span>
					</div>
					<div class="item-airport">
						<span class="title">Airport</span><br/>
						<span class="origin"><?php echo $vv['Attributes']['Origin']; ?></span><br/>
						<span class="destination"><?php echo $vv['Attributes']['Destination']; ?></span>
					</div>
					<div class="item-number">
						Flight: <?php echo $vv['Attributes']['FlightNumber']; ?> Class: <?php echo $vv['Attributes']['ClassOfService']; ?><br/>
						<?php echo $vv['Attributes']['Equipment']; ?><br/>
						Journey
						Time: <?php echo STTP_Search::inst()->minuteToHourMinute( $vv['Attributes']['FlightTime'] ); ?>
					</div>
					<div class="item-available">
						<span class="title">Available Ancillaries</span><br/>
						<i class="fa fa-braille" aria-hidden="true"></i> <i class="fa fa-spinner fa-spin loading"></i>
					</div>
					<div class="sttp-show-seat" data-id="<?php echo $vv['Attributes']['Origin'] . '|' . $vv['Attributes']['Destination']; ?>"></div>
				</div>
				<?php
			}
		}
		?>
	</div>

	<div class="total-price">
		<div class="text-right">
			<p>Base Rate: <b><?php echo $data['prices'][0]['Attributes']['ApproximateBasePrice']; ?></b></p>
			<p>Taxes, Surchanges, and Fees: <b><?php echo $data['prices'][0]['Attributes']['Taxes']; ?></b></p>
            <p class="seat-price"></p>
			<p class="total"><b id="cart-total-price" data-value="<?php echo $data['prices'][0]['Attributes']['ApproximateTotalPrice']; ?>"><?php echo $data['prices'][0]['Attributes']['ApproximateTotalPrice']; ?></b></p>
		</div>
	</div>

	<div class="sttp-passenger-form">
		<h3 class="title">
			<?php echo __('Passenger Information', ST_TEXTDOMAIN); ?>
		</h3>
		<div class="inner-form">
			<form method="post" id="sttp-checkout-form">
				<?php
				$arr_passenger = array();
				if(!empty($cart['adultNumber'])){
					for($i = 0; $i < $cart['adultNumber']; $i++){
						array_push($arr_passenger, array(
							'text' => __('Adult', ST_TEXTDOMAIN),
							'key' => 'adult',
							'age' => ''
						));
					}
				}

				if(!empty($cart['childNumber'])){
					$child_age_arr = explode(',', $cart['childAge']);
					for($i = 0; $i < $cart['childNumber']; $i++){
						array_push($arr_passenger, array(
							'text' => __('Children', ST_TEXTDOMAIN),
							'key' => 'children',
							'age' => $child_age_arr[$i]
						));
					}
				}

				if(!empty($cart['infantNumber'])){
					$infant_age_arr = explode(',', $cart['infantAge']);
					for($i = 0; $i < $cart['infantNumber']; $i++){
						array_push($arr_passenger, array(
							'text' => __('Infant', ST_TEXTDOMAIN),
							'key' => 'infant',
							'age' => $infant_age_arr[$i]
						));
					}
				}

				if(!empty($arr_passenger)) {
					foreach ($arr_passenger as $k => $v) {
						$age_temp = $v['age'];
						?>
						<div class="item">
							<div class="passenger-title">
								<?php
								$age = '';
								if($v['key'] != 'adult'){
									$age = '('. $v['age'] .')';
								}else{
									$age_temp = 0;
								}
								echo 'Passenger '. ($k + 1) .' - '. $v['text'] . $age;
								?>
							</div>
							<div class="row">
								<div class="col-lg-2">
									<div class="form-group">
										<label>Title</label>
										<select class="form-control" name="title[<?php echo $v['key'] ?>][<?php echo $age_temp; ?>][]">
											<option value="Mr">Mr.</option>
											<option value="Ms">Ms.</option>
											<option value="Master">Master.</option>
											<option value="Miss">Miss.</option>
											<option value="Mrs">Mrs.</option>
										</select>
									</div>
								</div>
								<div class="col-lg-5">
									<div class="form-group">
										<label>First Name</label>
										<input type="text" class="form-control passenger" name="first_name[<?php echo $v['key'] ?>][<?php echo $age_temp; ?>][]" />
									</div>
								</div>
								<div class="col-lg-5">
									<div class="form-group">
										<label>Last Name</label>
										<input type="text" class="form-control passenger" name="last_name[<?php echo $v['key'] ?>][<?php echo $age_temp; ?>][]" />
									</div>
								</div>
								<div class="col-lg-4">
									<div class="form-group">
										<label>Phone</label>
										<input type="text" class="form-control passenger" name="phone[<?php echo $v['key'] ?>][<?php echo $age_temp; ?>][]" />
									</div>
								</div>
								<div class="col-lg-4">
									<div class="form-group">
										<label>Email</label>
										<input type="text" class="form-control passenger" name="email[<?php echo $v['key'] ?>][<?php echo $age_temp; ?>][]" />
									</div>
								</div>
								<div class="col-lg-4">
									<div class="form-group">
										<label>Nationality</label>
										<input type="text" class="form-control passenger" name="nationality[<?php echo $v['key'] ?>][<?php echo $age_temp; ?>][]" />
									</div>
								</div>
							</div>
						</div>
						<?php
					}
				}
				?>
				<div class="item">
					<div class="passenger-title"><?php echo __('Address', ST_TEXTDOMAIN); ?></div>
					<div class="row">
						<div class="col-lg-8">
							<div class="form-group">
								<label>Address</label>
								<input type="text" class="form-control passenger" name="address" />
							</div>
						</div>
						<div class="col-lg-4">
							<div class="form-group">
								<label>City</label>
								<input type="text" class="form-control passenger" name="city" />
							</div>
						</div>
						<div class="col-lg-4">
							<div class="form-group">
								<label>ZipCode</label>
								<input type="text" class="form-control passenger" name="zip_code" />
							</div>
						</div>
						<div class="col-lg-4">
							<div class="form-group">
								<label>Country</label>
								<input type="text" class="form-control passenger" name="country" />
							</div>
						</div>
						<div class="col-lg-4">
							<div class="form-group">
								<label>State</label>
								<input type="text" class="form-control passenger" name="state" />
							</div>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="passenger-title"><?php echo __('Payment Information', ST_TEXTDOMAIN); ?></div>
                    <div class="item-pay">
                        <div class="st-icheck-item">
                            <label>
                                Cash payment
                                <input type="radio" name="sttp_payment" value="cash" checked>
                                <span class="checkmark"></span>
                            </label>
                        </div>
                    </div>
                    <div class="item-pay">
                        <div class="st-icheck-item">
                            <label>
                                Credit Card
                                <input type="radio" name="sttp_payment" value="credit_card">
                                <span class="checkmark"></span>
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-lg-2">
                                <div class="form-group">
                                    <label>Card Type</label>
                                    <select name="card_type" class="form-control">
                                        <option value="VI">Visa</option>
                                        <option value="CA">MasterCard</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label>CardHolder Name</label>
                                    <input type="text" class="form-control cardinfo" name="card_holder" />
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label>Card number</label>
                                    <input type="text" class="form-control cardinfo" name="card_number" />
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="form-group">
                                    <label>Expires</label>
                                    <input type="text" class="form-control cardinfo" name="card_expires" />
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="form-group">
                                    <label>CVV</label>
                                    <input type="text" class="form-control cardinfo" name="card_cvv" />
                                </div>
                            </div>
                        </div>
                    </div>
				</div>
                <div class="purchase-message mt10 mb10"></div>
				<div class="total-purchase">
					<div class="text-right">
						<p>Base Rate: <b><?php echo $data['prices'][0]['Attributes']['ApproximateBasePrice']; ?></b></p>
						<p>Taxes, Surchanges, and Fees: <b><?php echo $data['prices'][0]['Attributes']['Taxes']; ?></b></p>
                        <p class="seat-price"></p>
						<p class="total">Total: <b id="payment-total-price" data-value="<?php echo $data['prices'][0]['Attributes']['ApproximateTotalPrice']; ?>"><?php echo $data['prices'][0]['Attributes']['ApproximateTotalPrice']; ?></b></p>
						<button type="submit" class="sttp-purchase"><?php echo __('Purchase', ST_TEXTDOMAIN); ?> <i class="fa fa-spinner fa-spin"></i></button>
					</div>


				</div>
			</form>
		</div>
	</div>
</div>
<div class="sttp-popup-seat-box">
	<span class="close"><i class="fa fa-times" aria-hidden="true"></i></span>
	<div class="content"></div>
</div>
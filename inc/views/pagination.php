<?php
/**
 * Created by PhpStorm.
 * User: HanhDo
 * Date: 4/8/2019
 * Time: 2:20 PM
 */
?>
<div class="sttp-pag">
    <?php
    $currentLink = get_the_permalink();
    $argsUrl = $_GET;
    if (!empty($argsUrl)) {
        $currentLink = add_query_arg($argsUrl, $currentLink);
    }
    for ($i = 1; $i <= $pages; $i++) {
        if ($i == $current_page) {
            echo '<span class="page-item">' . $i . '</span>';
        } else {
            if ($i > 1) {
                $linkAddedParams = add_query_arg('sttp_page', $i, $currentLink);
            } else {
                $linkAddedParams = remove_query_arg('sttp_page', $currentLink);
            }
            echo '<a href="' . esc_url($linkAddedParams) . '" class="page-item">' . $i . '</a>';
        }
    }
    ?>
</div>
<?php
/**
 * Created by PhpStorm.
 * User: HanhDo
 * Date: 4/18/2019
 * Time: 10:00 AM
 */
$a = new Travelport();
$booking_data_arr = $a->getBookingHistory();
$booking_data = $booking_data_arr['res'];
$total_page = $booking_data_arr['total_page'];
?>

<div class="sttp-booking">
<div class="st-create">
    <h2><?php echo __('Travelport Booking History', 'st_travelport'); ?></h2>
</div>
<div class="sttp-booking-history-content">
    <table>
        <tr>
            <th><?php echo __('Origin', 'st_travelport'); ?></th>
            <th><?php echo __('Destination', 'st_travelport'); ?></th>
            <th><?php echo __('Fly Out - Back/Type', 'st_travelport'); ?></th>
            <th><?php echo __('Passengers', 'st_travelport'); ?></th>
            <th><?php echo __('Total Price', 'st_travelport'); ?></th>
            <th><?php echo __('Status', 'st_travelport'); ?></th>
            <th><?php echo __('Action', 'st_travelport'); ?></th>
        </tr>

        <?php
        if(!empty($booking_data)){
            foreach ($booking_data as $k => $v){
	            $order_id = $v['order_id'];
	            $status = STTP_Search::inst()->getOrderStatus($order_id);
                echo '<tr>';
                echo '<td>'. $v['origin'] .'<br /> <span class="sttp-airport-name hide-temp">'. $v['origin'] .'</span></td>';
                echo '<td>'. $v['destination'] .'<br /> <span class="sttp-airport-name hide-temp">'. $v['destination'] .'</span></td>';
                echo '<td>'. $v['fly_out'] . ' - ' . $v['fly_back'] .'<br />'. ucfirst($v['flight_type']) .'</td>';
                ?>
                <td>
                    <?php
                    echo __('No. Adult', 'st_travelport') . ': ' . $v['number_adult'];
                    if (!empty($v['number_child'])) {
                        echo '<br />'. __('No. Children', 'st_travelport') .': ' . $v['number_child'];
                        echo ' - '. __('Age', 'st_travelport') .': ' . str_replace(',', ', ', $v['age_child']);
                    }
                    if (!empty($v['number_infant'])) {
                        echo '<br />'. __('No. Infant', 'st_travelport') .': ' . $v['number_infant'];
                        echo ' - '. __('Age', 'st_travelport') .': ' . str_replace(',', ', ', $v['age_infant']);
                    }
                    ?>
                </td>
                <td>
                    <?php echo TravelHelper::format_money($v['price']); ?>
                </td>
                <td>
                    <?php echo ucfirst($status); ?>
                </td>
                <?php
                echo '<td><button type="button" class="btn btn-primary btn-xs btn-sttp-booking-details" data-id="'. $v['id'] .'" data-toggle="modal" data-target="#sttpModal">'. __('Details', 'st_travelport') .'</button> </td>';
                echo '</tr>';
            }
        }
        ?>
    </table>
</div>
</div>

<?php
if($total_page > 1){
?>
<div class="sttp-pagination">
    <ul>
        <?php
        if(isset($_GET['flpage']))
            $currentPage = $_GET['flpage'];
        else
            $currentPage = 1;

        $link = get_the_permalink();
        $link = add_query_arg('sc', $_GET['sc'], $link);
        if($currentPage > 1){
            $prev_i = $currentPage - 1;
            if($prev_i > 1)
                $link = add_query_arg('flpage', $prev_i, $link);

            echo '<li><a class="prev page-numbers" href="'. esc_url($link) .'"><i class="fa fa-angle-left"></i></a></li>';
        }
        for ($i = 1; $i <= $total_page; $i++){
	        $link = add_query_arg('flpage', $i, $link);
            if($i == 1)
                $link = remove_query_arg('flpage');
            if($i == $currentPage){
                echo '<li><a aria-current="page" class="page-numbers current">'. $i .'</a></li>';
            }else{
                echo '<li><a class="page-numbers" href="'. esc_url($link) .'">'. $i .'</a></li>';
            }
        }

        if($currentPage < $total_page){
	        $next_i = $currentPage + 1;
            $link = add_query_arg('flpage', $next_i, $link);

	        echo '<li><a class="next page-numbers" href="'. esc_url($link) .'"><i class="fa fa-angle-right"></i></a></li>';
        }
        ?>
    </ul>
</div>
<?php } ?>

<!-- Modal -->
<div id="sttpModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><?php echo __('Booking History', 'st_travelport'); ?></h4>
            </div>
            <div class="modal-body">
                <div class="sttp-overlay">
                    <i class="fa fa-spinner fa-spin"></i>
                </div>
                <div id="sttp-booking-detail"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo __('Close', 'st_travelport'); ?></button>
            </div>
        </div>

    </div>
</div>


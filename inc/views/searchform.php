<?php
/**
 * Created by PhpStorm.
 * User: HanhDo
 * Date: 4/1/2019
 * Time: 11:23 AM
 */
$result_page = get_the_permalink(get_option('st_travelport_search_page'));

$from_code = STInput::get('from_code');
$from_name = STInput::get('from_name');
$from_country_code = STInput::get('from_country_code');
$location_from = '';
if(!empty($from_code) || !empty($from_name)){
	$location_from = $from_code . ' ('. $from_name .')';
}

$to_code = STInput::get('to_code');
$to_name = STInput::get('to_name');
$to_country_code = STInput::get('to_country_code');
$location_to = '';
if(!empty($to_code) || !empty($to_name)){
	$location_to = $to_code . ' ('. $to_name .')';
}

$adult_number = STInput::get('adult_number', 1);
$child_number = STInput::get('child_number', 0);
$infant_number = STInput::get('infant_number', 0);
$preferred_cabin = STInput::get('preferred_cabin', '');

$search_by = st_travelport_get_option('search_by', 'country');
?>
<div class="search-form-wrapper sttp-search-form" data-search="<?php echo esc_attr($search_by); ?>">
    <div class="row">
        <form action="<?php echo esc_url($result_page); ?>" class="form" method="get">
            <div class="col-lg-12 tour-search-form-home">
                <div class="search-form">
                    <div class="row">
                        <div class="col-md-5 border-right">
                            <div class="form-group form-extra-field dropdown clearfix field-detination has-icon"
                                 data-value="from">
                                <?php echo TravelHelper::getNewIcon('ico_maps_search_box'); ?>
                                <div class="dropdown" data-toggle="dropdown" id="dropdown-destination">
                                    <label><?php echo __('From', 'st_travelport'); ?></label>
                                    <div class="render">
                                        <span class="destination">
                                            <?php
                                            if (empty($location_from)) {
                                                echo __('Departure Airport', 'st_travelport');
                                            } else {
                                                echo $location_from;
                                            }
                                            ?>
                                        </span>
                                    </div>
                                </div>
                                <input type="hidden" value="<?php echo $from_code; ?>" name="from_code" class="sttp-field-airport-code"/>
                                <input type="hidden" value="<?php echo $from_name ?>" name="from_name" class="sttp-field-airport-name"/>
                                <input type="hidden" value="<?php echo $from_country_code; ?>"  name="from_country_code" class="sttp-field-country-code"/>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group form-extra-field dropdown clearfix field-detination has-icon"
                                 data-value="to">
                                <?php echo TravelHelper::getNewIcon('ico_maps_search_box'); ?>
                                <div class="dropdown" data-toggle="dropdown" id="dropdown-destination">
                                    <label><?php echo __('To', 'st_travelport'); ?></label>
                                    <div class="render">
                                        <span class="destination">
                                            <?php
                                            if (empty($location_to)) {
                                                echo __('Destination Airport', 'st_travelport');
                                            } else {
                                                echo $location_to;
                                            }
                                            ?>
                                        </span>
                                    </div>
                                </div>
                                <input type="hidden" value="<?php echo $to_code; ?>" name="to_code" class="sttp-field-airport-code"/>
                                <input type="hidden" value="<?php echo $to_name; ?>" name="to_name" class="sttp-field-airport-name"/>
                                <input type="hidden" value="<?php echo $to_country_code; ?>" name="to_country_code" class="sttp-field-country-code"/>
                            </div>
                        </div>
                        <div class="col-md-2 sttp-button">
                            <button class="btn btn-primary btn-search"
                                    type="submit"><?php echo __('Search', 'st_travelport'); ?></button>
                        </div>
                    </div>

                    <!--Dropdown country and airport-->
                    <ul class="sttp-dropdown-menu" aria-labelledby="dropdown-destination">
                        <div class="st-loader"></div>
                        <div class="row">
                            <div class="col-sm-9 country">
                                <?php
                                if($search_by != 'city') {
                                    ?>
                                    <label><?php echo __('Pick a country', 'st_travelport'); ?></label>
                                    <?php
                                }else{
                                  ?>
                                    <label><?php echo __('Pick a city', 'st_travelport'); ?> &nbsp;<i class="fa fa-spinner fa-spin fa-loader"></i></label>
                                    <?php
                                }
                                ?>
                                <input type="text" class="form-control sttp-search-js"/>
                                <div class="sttp-dropdown-menu-country">
                                    <div class="row">
                                        <?php
                                        if($search_by != 'city') {
                                            $all_country = STTP_CountryModel::inst()->getAllDataCountry();
                                            if (!empty($all_country)) {
                                                foreach ($all_country as $k => $v) {
                                                    echo '<div class="col-sm-4 col-xs-6 item-wrapper" data-name="' . $v['country_name'] . '"><span class="item" data-value="' . $v['country_code'] . '">' . $v['country_name'] . '</span></div>';
                                                }
                                            }
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3 airport">
                                <label><?php echo __('Pick a airport', 'st_travelport'); ?></label>
                                <input type="text" class="form-control sttp-search-js"/>
                                <div id="sttp-airport-list"></div>
                            </div>
                        </div>
                    </ul>
                    <!--End dropdown country and airport-->

                </div>
                <span id="show-advance" class="show-advance-search"><?php echo __('Advance search', 'st_travelport'); ?><i class="fa fa-caret-down"></i> </span>
                <div class="advance-search">
                    <div class="advance-search-box">
                        <input type="hidden" name="flight_way" value="roundtrip" />
                        <?php
                        $start = STInput::get('fly_out', date(TravelHelper::getDateFormat()));
                        $end = STInput::get('fly_back', date(TravelHelper::getDateFormat()));
                        $date_start = STInput::get('date', $start . '-' . $start);
                        $date_end = STInput::get('date', $end . '-' . $end);
                        ?>
                        <div class="flight-way">
                            <ul>
                                <li data-value="roundtrip" class="active"><?php echo __('Roundtrip', 'st_travelport'); ?></li>
                                <li data-value="oneway"><?php echo __('Oneway', 'st_travelport'); ?></li>
                            </ul>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="sttp-date" data-format="<?php echo TravelHelper::getDateFormatMoment(); ?>">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <div class="sttp-out sttp-date-item">
                                                <div class="label">
                                                    <?php echo __('Fly Out', 'st_travelport'); ?>
                                                </div>
                                                <div class="render">
                                                    <span><?php echo $start; ?></span>
                                                </div>
                                                <input type="hidden" name="fly_out" class="sttp-fly-out" value="<?php echo $start; ?>" />
                                                <input type="text" class="sttp-input-date sttp-input-out-date" value="<?php echo $date_start; ?>" data-format="<?php echo TravelHelper::getDateFormatMoment(); ?>"/>
                                            </div>
                                        </div>
                                        <div class="col-xs-6">
                                            <div class="sttp-back sttp-date-item">
                                                <div class="label">
                                                    <?php echo __('Fly Back', 'st_travelport'); ?>
                                                </div>
                                                <div class="render">
                                                    <span><?php echo $end; ?></span>
                                                </div>
                                                <input type="hidden" name="fly_back" class="sttp-fly-back" value="<?php echo $end; ?>" />
                                                <input type="text" class="sttp-input-date sttp-input-back-date" value="<?php echo $date_end; ?>" data-format="<?php echo TravelHelper::getDateFormatMoment(); ?>"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-2 people-item">
                                <label><?php echo __('Adult', 'st_travelport') ?></label>
                                <select name="adult_number" class="form-control">
                                    <?php
                                    for ($i = 1; $i < 10; $i++) {
                                        echo '<option value="' . $i . '" '. selected($adult_number, $i) .'>' . $i . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-sm-2 people-item">
                                <label><?php echo __('Children', 'st_travelport') ?></label>
                                <select name="child_number" class="form-control child">
                                    <?php
                                    for ($i = 0; $i < 10; $i++) {
                                        echo '<option value="' . $i . '" '. selected($child_number, $i) .'>' . $i . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-sm-2 people-item">
                                <label><?php echo __('Infant', 'st_travelport') ?></label>
                                <select name="infant_number" class="form-control infant">
                                    <?php
                                    for ($i = 0; $i < 10; $i++) {
                                        echo '<option value="' . $i . '" '. selected($infant_number, $i) .'>' . $i . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-sm-3 people-item">
                                <label><?php echo __('Preferred Cabin', 'st_travelport') ?></label>
                                <?php
                                $arr_class = array(
                                    'Premium First',
                                    'First',
                                    'Business',
                                    'Premium Economy',
                                    'Economy'
                                );
                                ?>
                                <select name="preferred_cabin" class="form-control">
                                    <option value="" <?php echo selected($preferred_cabin, ''); ?>><?php echo __('Select Cabin Class', 'st_travelport'); ?></option>
			                        <?php
			                        foreach ($arr_class as $v) {
				                        echo '<option value="' . $v . '" '. selected($preferred_cabin, $v) .'>' . $v . '</option>';
			                        }
			                        ?>
                                </select>
                            </div>
                        </div>
                        <div class="sttp-age-item child">
                            <label><?php echo __('Age of children at time of travel', 'st_travelport'); ?></label>
                            <div class="row">

                            </div>
                        </div>

                        <div class="sttp-age-item infant">
                            <label><?php echo __('Age of infant at time of travel', 'st_travelport'); ?></label>
                            <div class="row"></div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

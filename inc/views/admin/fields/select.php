<?php
/**
 * Created by PhpStorm.
 * User: Jream
 * Date: 4/13/2019
 * Time: 3:34 PM
 */
$data=wp_parse_args($data,array(
	'placeholder'=>''
));
$data_value = st_travelport_get_option($data['id'],$data['std']);
$name = 'st_travelport_'.$data['id'];

if(!empty($data['element_list_item'])){
	$name = $data['custom_name'];
}
if(!empty($data['element_list_item'])){
	$data_value = $data['custom_value'];
}

$class = $name;
$data_class = '';
if(!empty($data['condition'])){
	$class .= ' wpbooking-condition wpbooking-form-group ';
	$data_class .= ' data-condition=wpbooking_'.$data['condition'].' ' ;
}

?>
<tr class="<?php echo esc_html($class) ?>" <?php echo esc_attr($data_class) ?>>
	<th scope="row">
		<label for="<?php echo esc_html($data['id']) ?>"><?php echo esc_html($data['label']) ?>:</label>
	</th>
	<td>
		<select id="<?php echo esc_attr($name) ?>" class="form-control  min-width-500" name="<?php echo esc_html($name) ?>">
			<?php
			foreach ($data['options'] as $k => $v){
				echo '<option value="'. $v['value'] .'" '. selected($data_value, $v['value']) .'>'. $v['label'] .'</option>';
			}
			?>
		</select>
		<i class="wpbooking-desc"><?php echo do_shortcode($data['desc']) ?></i>
	</td>
</tr>
<?php
$data=wp_parse_args($data,array(
	'placeholder'=>''
));
$data_value = st_travelport_get_option($data['id'],$data['std']);
$name = 'st_travelport_'.$data['id'];

if(!empty($data['element_list_item'])){
    $name = $data['custom_name'];
}
if(!empty($data['element_list_item'])){
    $data_value = $data['custom_value'];
}

$class = $name;
$data_class = '';
if(!empty($data['condition'])){
    $class .= ' wpbooking-condition wpbooking-form-group ';
    $data_class .= ' data-condition=wpbooking_'.$data['condition'].' ' ;
}

?>
<tr class="<?php echo esc_html($class) ?>" <?php echo esc_attr($data_class) ?>>
    <th scope="row">
        <label for="<?php echo esc_html($data['id']) ?>"><?php echo esc_html($data['label']) ?>:</label>
    </th>
    <td>
        <?php
        $args = array(
            'depth'                 => 0,
            'child_of'              => 0,
            'selected'              => esc_html($data_value),
            'echo'                  => 1,
            'name'                  => esc_html($name),
            'id'                    => esc_attr($name), // string
            'class'                 => 'form-control  min-width-500', // string
            'show_option_none'      => null, // string
            'show_option_no_change' => null, // string
            'option_none_value'     => null, // string
        );
        wp_dropdown_pages( $args );
        ?>
        <i class="wpbooking-desc"><?php echo do_shortcode($data['desc']) ?></i>
    </td>
</tr>
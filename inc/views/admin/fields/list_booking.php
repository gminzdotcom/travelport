<h3><?php echo $data['label']; ?></h3>
<?php
$a            = new Travelport();
$booking_data = $a->getBookingHistory();
?>
<div class="sttp-booking-history-content">
    <table>
        <tr>
            <th><?php echo __( 'Origin', 'st_travelport' ); ?></th>
            <th><?php echo __( 'Destination', 'st_travelport' ); ?></th>
            <th><?php echo __( 'Fly Out - Back/Type', 'st_travelport' ); ?></th>
            <th><?php echo __( 'Passengers', 'st_travelport' ); ?></th>
            <th><?php echo __( 'Total Price', 'st_travelport' ); ?></th>
            <th><?php echo __( 'Action', 'st_travelport' ); ?></th>
        </tr>

		<?php
		if ( ! empty( $booking_data ) ) {
			foreach ( $booking_data as $k => $v ) {
				echo '<tr>';
				echo '<td>' . $v['origin'] . '<br /> <span class="sttp-airport-name hide-temp">' . $v['origin'] . '</span></td>';
				echo '<td>' . $v['destination'] . '<br /> <span class="sttp-airport-name hide-temp">' . $v['destination'] . '</span></td>';
				echo '<td>' . $v['fly_out'] . ' - ' . $v['fly_back'] . '<br />' . ucfirst( $v['flight_type'] ) . '</td>';
				?>
                <td>
					<?php
					echo 'No. Adult: ' . $v['number_adult'];
					if ( ! empty( $v['number_child'] ) ) {
						echo '<br />No. Children: ' . $v['number_child'];
						echo ' - Age: ' . str_replace( ',', ', ', $v['age_child'] );
					}
					if ( ! empty( $v['number_infant'] ) ) {
						echo '<br />No. Infant: ' . $v['number_infant'];
						echo ' - Age: ' . str_replace( ',', ', ', $v['age_infant'] );
					}
					?>
                </td>
                <td>
					<?php echo TravelHelper::format_money( $v['price'] ); ?>
                </td>
				<?php
				echo '<td><button type="button" class="btn btn-primary btn-xs btn-sttp-booking-details" data-id="' . $v['id'] . '" data-toggle="modal" data-target="#sttpModal">' . __( 'Details', 'st_travelport' ) . '</button> </td>';
				echo '</tr>';
			}
		}
		?>
    </table>
</div>

<!-- Modal -->
<div id="sttpModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><?php echo __( 'Booking History', 'st_travelport' ); ?></h4>
            </div>
            <div class="modal-body">
                <div class="sttp-overlay">
                    <i class="fa fa-spinner fa-spin"></i>
                </div>
                <div id="sttp-booking-detail"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default"
                        data-dismiss="modal"><?php echo __( 'Close', 'st_travelport' ); ?></button>
            </div>
        </div>

    </div>
</div>

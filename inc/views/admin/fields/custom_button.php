<?php

$data = wp_parse_args( $data, array(
	'placeholder' => ''
) );
$name = 'st_pkfare_' . $data['id'];

if ( ! empty( $data['id'] ) ) {
	$data_id = $data['id'];
}
if ( ! empty( $data['text'] ) ) {
	$data_text = $data['text'];
}

$class = $name;

?>
<tr class="<?php echo esc_html( $class ) ?>">
    <th scope="row">
        <label for="<?php echo esc_html( $data['id'] ) ?>"><?php echo esc_html( $data['label'] ) ?>:</label>
    </th>
    <td>
        <button class="btn button button-primary"
                id="<?php echo esc_html( $data_id ) ?>"><?php echo esc_html( $data_text ); ?></button>
        <span class="spinner"></span>
		<?php
		if ( ImporterController::inst()->getStatusDataImport( $data['table'] ) ) {
			$span_class = 'dashicons-yes yes';
			$text       = esc_html__( 'Data has imported', 'st_travelport' );
		} else {
			$span_class = 'dashicons-no-alt no';
			$text       = esc_html__( 'Data not yet imported', 'st_travelport' );
		}
		?>
        <div class="status-import" data-text-yes="<?php echo esc_html__( 'Data has imported', 'st_travelport' ); ?>" data-text-no="<?php echo esc_html__( 'Data not yet imported', 'st_travelport' ); ?>"><span
                    class="dashicons <?php echo $span_class; ?>"></span> <?php echo $text; ?>
        </div>
        <div class="message"></div>

    </td>
</tr>
<?php
$order_code = STInput::get('order_code');
$order_token_code=STInput::get('order_token_code');

if($order_token_code)
{
	$order_code=STOrder::get_order_id_by_token($order_token_code);

}

$booking_info = STTP_OrderModel::inst()
->where('order_id', $order_code)
->get()->result();
$price = $cart_info['travelport_api']['price'];
if(!empty($booking_info)){
    $booking_info = array_shift($booking_info);
    $price = $booking_info['price'];
}
?>
<ul>
	<li class="payment-amount">
		<span class="label"><?php echo __('Pay Amount', 'st_travelport') ?></span>
		<span class="value"><?php echo TravelHelper::format_money($price); ?></span>
	</li>
</ul>
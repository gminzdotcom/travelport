<div class="service-section">
	<div class="service-left">
		<h4 class="title">
			<?php echo __('Departure - Destination', 'st_travelport') ?><br><span class="sttp-airport-name hide-temp"><?php echo $cart_info['travelport_api']['data']['fromCode'] ?></span> (<?php echo $cart_info['travelport_api']['data']['fromCode'] ?>) -&gt; <span class="sttp-airport-name hide-temp"><?php echo $cart_info['travelport_api']['data']['toCode'] ?></span> (<?php echo $cart_info['travelport_api']['data']['toCode'] ?>)		</h4>
	</div>
</div>
<div class="info-section">
	<ul>
		<li>
			<span class="label"><?php echo __('Flight Out', 'st_travelport') ?></span>
			<span class="value"><?php echo $cart_info['travelport_api']['data']['flyOut'] ?></span>
		</li>
        <?php
        if($cart_info['travelport_api']['data']['flightWay'] != 'oneway') {
	        ?>
            <li>
                <span class="label"><?php echo __( 'Flight Back', 'st_travelport' ) ?></span>
                <span class="value"><?php echo $cart_info['travelport_api']['data']['flyBack'] ?></span>
            </li>
	        <?php
        }
        ?>
		<li>
			<span class="label"><?php echo __('No.Adult', 'st_travelport') ?></span>
			<span class="value"><?php echo $cart_info['travelport_api']['data']['adultNumber'] ?></span>
		</li>
		<li>
			<span class="label"><?php echo __('No.Children', 'st_travelport') ?></span>
			<span class="value"><?php echo $cart_info['travelport_api']['data']['childNumber'] ?>
				<?php if($cart_info['travelport_api']['data']['childNumber'] > 0){ ?>
				<small>(<?php echo __('Age', 'st_travelport') ?>: <?php echo str_replace(',', ', ', $cart_info['travelport_api']['data']['childAge']); ?>)</small>
				<?php } ?>
			</span>
		</li>
		<li>
			<span class="label"><?php echo __('No.Infant', 'st_travelport') ?></span>
			<span class="value"><?php echo $cart_info['travelport_api']['data']['infantNumber'] ?>
				<?php if($cart_info['travelport_api']['data']['infantNumber'] > 0){ ?>
				<small>(<?php echo __('Age', 'st_travelport') ?>: <?php echo str_replace(',', ', ', $cart_info['travelport_api']['data']['infantAge']); ?>)</small>
				<?php } ?>
			</span>
		</li>
	</ul>
</div>

<?php
$cart = STCart::get_cart_item();
$data = STTP_Search::inst()->getAirPrice();
$sttp_price_cart = $data['data'];

unset($_SESSION['sttp_seat_passenger']);
unset($_SESSION['sttp_total_price']);
?>
<div class="service-section">
	<div class="service-left">
		<h4 class="title">
			<?php
			echo __('Departure - Destination', 'st_travelport') . '<br /><span class="sttp-airport-name hide-temp">' . $cart['value']['data']['fromCode'] . '</span> ('. $cart['value']['data']['fromCode'] .') -> <span class="sttp-airport-name hide-temp">' . $cart['value']['data']['toCode'] . '</span> ('. $cart['value']['data']['toCode'] .')';
			?>
		</h4>
	</div>
</div>
<div class="info-section">
	<ul>
		<li>
            <span class="label"><?php echo __('Flight Out', 'st_travelport'); ?></span>
			<span class="value"><?php echo $cart['value']['data']['flyOut']; ?></span>
		</li>
        <?php if($cart['value']['data']['flightWay'] != 'oneway'){ ?>
		<li>
			<span class="label"><?php echo __('Flight Back', 'st_travelport'); ?></span>
			<span class="value"><?php echo $cart['value']['data']['flyBack']; ?></span>
		</li>
        <?php } ?>
		<li>
			<span class="label"><?php echo __('No.Adult', 'st_travelport'); ?></span>
			<span class="value">
				<?php echo $cart['value']['data']['adultNumber']; ?>
			</span>
		</li>
		<li>
			<span class="label"><?php echo __('No.Children', 'st_travelport'); ?></span>
			<span class="value">
				<?php
				echo $cart['value']['data']['childNumber'];
				if($cart['value']['data']['childNumber'] > 0){
					?>
					 <small>(<?php echo __('Age', 'st_travelport'); ?>: <?php echo str_replace(',', ', ', $cart['value']['data']['childAge']); ?>)</small>
					<?php
				}
				?>
			</span>
		</li>
		<li>
			<span class="label"><?php echo __('No.Infant', 'st_travelport'); ?></span>
			<span class="value">
				<?php
				echo $cart['value']['data']['infantNumber'];
				if($cart['value']['data']['infantNumber'] > 0){
					?>
					 <small>(<?php echo __('Age', 'st_travelport'); ?>: <?php echo str_replace(',', ', ', $cart['value']['data']['infantAge']); ?>)</small>
					<?php
				}
				?>
			</span>
		</li>
		<hr />
		<li>
			<p><b><?php echo __('Itinerary', 'st_travelport'); ?></b></p>
			<?php
			if ( ! empty( $sttp_price_cart['OutBound'] ) ) {
				echo '<b>'. __('OutBound', 'st_travelport') .'</b>';
				if ( ! empty( $sttp_price_cart['OutBound'] ) ) {
					foreach ( $sttp_price_cart['OutBound'] as $k => $v ) {
                        $start = STTP_Search::inst()->getDateTime( $v['Attributes']['DepartureTime'], 'd/m/Y' );
                        $end   = STTP_Search::inst()->getDateTime( $v['Attributes']['ArrivalTime'], 'd/m/Y' );
						?>
						<p>
							<b><?php echo '<span class="sttp-airport-name">' . $v['Attributes']['Origin'] . '</span> (' . $v['Attributes']['Origin'] . ') -> <span class="sttp-airport-name">' . $v['Attributes']['Destination'] . '</span> (' . $v['Attributes']['Destination'] . ')'; ?></b>
						</p>
						<p>
							<?php echo __('Departure', 'st_travelport'); ?>: <?php echo $start['time'] . ' <small>(' . $start['time_zone'] . ')</small> ' . $start['date']; ?></p>
						<p>
							<?php echo __('Arrival', 'st_travelport') ?>: <?php echo $end['time'] . ' <small>(' . $end['time_zone'] . ')</small> ' .$end['date']; ?></p>
						<p><?php echo __('Flight Time', 'st_travelport') ?>: <?php echo STTP_Search::inst()->minuteToHourMinute( $v['Attributes']['FlightTime'] ); ?></p>
                        <div class="sttp-seat-data" data-value="<?php echo $v['Attributes']['Origin'] . '|' . $v['Attributes']['Destination'] ?>"></div><br />
						<?php

					}
				}
			}
			?>
		</li>
		<?php
		if ( ! empty( $sttp_price_cart['InBound'] ) ) {
			?>
			<hr />
			<li>
				<?php
				echo '<b>'. __('InBound', 'st_travelport') .'</b>';
				if ( ! empty( $sttp_price_cart['InBound'] ) ) {
					foreach ( $sttp_price_cart['InBound'] as $k => $v ) {
                        $start = STTP_Search::inst()->getDateTime( $v['Attributes']['DepartureTime'], 'd/m/Y' );
                        $end   = STTP_Search::inst()->getDateTime( $v['Attributes']['ArrivalTime'], 'd/m/Y' );
						?>
						<p>
							<b><?php echo '<span class="sttp-airport-name">' . $v['Attributes']['Origin'] . '</span> (' . $v['Attributes']['Origin'] . ') -> <span class="sttp-airport-name">' . $v['Attributes']['Destination'] . '</span> (' . $v['Attributes']['Destination'] . ')'; ?></b>
						</p>
						<p>
							<?php echo __('Departure', 'st_travelport') ?>: <?php echo $start['time'] . ' <small>(' . $start['time_zone'] . ')</small> ' . $start['date']; ?></p>
						<p>
							<?php echo __('Arrival', 'st_travelport') ?>: <?php echo $end['time'] . ' <small>(' . $end['time_zone'] . ')</small> ' . $end['date']; ?></p>
						<p><?php echo __('Flight Time', 'st_travelport') ?>: <?php echo STTP_Search::inst()->minuteToHourMinute( $v['Attributes']['FlightTime'] ); ?></p>
                        <div class="sttp-seat-data" data-value="<?php echo $v['Attributes']['Origin'] . '|' . $v['Attributes']['Destination'] ?>"></div><br />
						<?php

					}
				}
				?>
			</li>
			<?php
		}
		?>
        <li><button class="btn btn-primary btn-sm btn-get-seat"><?php echo __('Get Seat', 'st_travelport') ?> <i class="fa fa-spinner fa-spin loading"></i></button></li>
        <div class="ct-seat-message alert alert-warning"></div>
	</ul>
</div>
<div class="total-section">
	<ul>
        <li class="seat-price"></li>
        <li class="sub-total"></li>
		<li class="payment-amount">
			<span class="label"><?php echo __('Pay Amount', 'st_travelport') ?></span>
			<span class="value"><?php echo TravelHelper::format_money($cart['value']['price']); ?></span>
		</li>
	</ul>
</div>

<div class="sttp-popup-seat-box">
    <span class="close"><i class="fa fa-times" aria-hidden="true"></i></span>
    <div class="content"></div>
</div>
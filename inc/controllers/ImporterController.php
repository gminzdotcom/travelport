<?php

class ImporterController extends STTP_BaseImporter {
	static $_inst = false;
	private $plugin_url;

	function __construct() {
		add_action( 'wp_ajax_sttp_import_city_data', array( $this, 'importDataCity' ) );
		add_action( 'wp_ajax_sttp_import_airplane_data', array( $this, 'importDataAirplane' ) );
		add_action( 'wp_ajax_sttp_import_airport_data', array( $this, 'importDataAirport' ) );
		add_action( 'wp_ajax_sttp_import_carrier_data', array( $this, 'importDataCarrier' ) );
		add_action( 'wp_ajax_sttp_import_country_data', array( $this, 'importDataCountry' ) );
		add_action( 'wp_ajax_test', array( $this, 'test' ) );
		$this->plugin_url = plugin_dir_path(__FILE__);
	}

	function test(){
		$res = array();
		array_push($res, array(
			'value' => 'HAN',
			'label' => 'Noi Bai'
		));
		return json_encode(res);
		die;
	}

	private function deleteDataTable($table){
        switch ( $table ) {
            case 'sttp_city':
                STTP_CityModel::inst()->delete();
                break;
            case 'sttp_airplane':
                STTP_AirplaneModel::inst()->delete();
                break;
            case 'sttp_airport':
                STTP_AirportModel::inst()->delete();
                break;
            case 'sttp_carrier':
                STTP_CarrierModel::inst()->delete();
                break;
            case 'sttp_country':
                STTP_CountryModel::inst()->delete();
                break;
        }
    }

    public function importDataAirplane() {
        check_ajax_referer( 'sttp-security-nonce', 'security' );
        $this->deleteDataTable('sttp_airplane');
        $this->setFileUrl( $this->plugin_url . 'db/RAEQ.csv' );
        $this->setTableName( 'sttp_airplane' );
        $result = $this->runImport();
        echo json_encode( $result );
        die;
    }

    public function importDataAirport() {
        check_ajax_referer( 'sttp-security-nonce', 'security' );
        $this->deleteDataTable('sttp_airport');
        $this->setFileUrl( $this->plugin_url . 'db/RAPT.csv' );
        $this->setTableName( 'sttp_airport' );
        $result = $this->runImport();
        echo json_encode( $result );
        die;
    }

    public function importDataCarrier() {
        check_ajax_referer( 'sttp-security-nonce', 'security' );
        $this->deleteDataTable('sttp_carrier');
        $this->setFileUrl( $this->plugin_url . 'db/RAIR.csv' );
        $this->setTableName( 'sttp_carrier' );
        $result = $this->runImport();
        echo json_encode( $result );
        die;
    }

    public function importDataCountry() {
        check_ajax_referer( 'sttp-security-nonce', 'security' );
        $this->deleteDataTable('sttp_country');
        $this->setFileUrl( $this->plugin_url . 'db/RCNT.csv' );
        $this->setTableName( 'sttp_country' );
        $result = $this->runImport();
        echo json_encode( $result );
        die;
    }

	public function importDataCity() {
		check_ajax_referer( 'sttp-security-nonce', 'security' );
        $this->deleteDataTable('sttp_city');
		$this->setFileUrl( $this->plugin_url . 'db/RCTY.csv' );
		$this->setTableName( 'sttp_city' );
		$result = $this->runImport();
		echo json_encode( $result );
		die;
	}

	public function getStatusDataImport( $table ) {
		if(!empty($table)){
			$number_of_row = 0;
			switch ( $table ) {
				case 'sttp_city':
					$number_of_row = STTP_CityModel::inst()->getNumRows();
					break;
                case 'sttp_airplane':
                    $number_of_row = STTP_AirplaneModel::inst()->getNumRows();
                    break;
                case 'sttp_airport':
                    $number_of_row = STTP_AirportModel::inst()->getNumRows();
                    break;
                case 'sttp_carrier':
                    $number_of_row = STTP_CarrierModel::inst()->getNumRows();
                    break;
                case 'sttp_country':
                    $number_of_row = STTP_CountryModel::inst()->getNumRows();
                    break;
			}
			if ( $number_of_row > 0 ) {
				return true;
			} else {
				return false;
			}
		}else{
			return false;
		}
	}

	static function inst() {

		if ( ! self::$_inst ) {
			self::$_inst = new self();
		}

		return self::$_inst;
	}
}

ImporterController::inst();
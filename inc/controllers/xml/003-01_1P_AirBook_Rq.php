<?php
/* Seat data */
$seat_passenger = $_SESSION['sttp_seat_passenger'];

$xmlSeat = '';
if(!empty($seat_passenger)){
	foreach ($seat_passenger as $kk => $vv){
		if(!empty($vv)){
			foreach ($vv as $kkk => $vvv){
				$xmlSeat .= '<SpecificSeatAssignment xmlns="http://www.travelport.com/schema/air_v42_0" BookingTravelerRef="'. $vvv->key .'" SegmentRef="'. $vvv->segment .'" SeatId="'. $vvv->seatCode .'" />';
			}
		}
	}
}

$seat_price = $_SESSION['sttp_total_price'];
/* End seat data */
$airPriceData = $_SESSION['sttp_AirPrice'];
$cart = $_SESSION['sttp_cart'];
$air_res 			= $air_price_data['SOAPBody']['airAirPriceRsp'];

$air_segment 		= $air_res['airAirItinerary']['airAirSegment'];

$air_segment_reverb = array();
if(!empty($air_segment)){
	foreach ($air_segment as $k => $v){
		$air_segment_reverb[$v['@attributes']['Key']] = $v;
	}
}


$air_pricing_sol 	= $air_res['airAirPriceResult']['airAirPricingSolution'][0]['@attributes'];

$air_pricing_sol_seg 	= $air_res['airAirPriceResult']['airAirPricingSolution'][0]['airAirSegmentRef'];

$air_pricing_info 	= $air_res['airAirPriceResult']['airAirPricingSolution'][0]['airAirPricingInfo'];
$air_pricing_info_origin 	= $airPriceData['prices'];

$keyTraveler = array();
foreach ($air_pricing_info as $aa => $bb){
	$keyTraveler[] = $bb['@attributes']['Key'] . uniqid();
}
//$air_pricing_host 	= $air_res['airAirPriceResult']['airAirPricingSolution'][0]['common_v42_0HostToken'];
$air_pricing_host 	= $airPriceData['prices'][0]['HostToken'];

//dd($air_pricing_info);die;

$air_fare_info 		= $air_res['airAirPriceResult']['airAirPricingSolution'][0]['airAirPricingInfo'][0]['airFareInfo'];
$air_booking_info 	= $air_res['airAirPriceResult']['airAirPricingSolution'][0]['airAirPricingInfo'][0]['airBookingInfo'];
$air_fare_calc 		= $air_res['airAirPriceResult']['airAirPricingSolution'][0]['airAirPricingInfo'][0]['airFareCalc'];
$air_passenger_type = $air_res['airAirPriceResult']['airAirPricingSolution'][0]['airAirPricingInfo'][0]['airPassengerType'];

$air_baggage_info 	= $air_res['airAirPriceResult']['airAirPricingSolution'][0]['airAirPricingInfo'][0]['airBaggageAllowances']['airBaggageAllowanceInfo'][0];

$air_tax_info 	= $air_res['airAirPriceResult']['airAirPricingSolution'][0]['airAirPricingInfo'][0]['airTaxInfo'];

$bookingTraveler = '';

if(!empty($form['passenger'])){
    $childAge = isset($cart['childAge']) ? $cart['childAge'] : '';
    $childAgeArr = explode(',', $childAge);
    $infantAge = isset($cart['infantAge']) ? $cart['infantAge'] : '';
    $infantAgeArr = explode(',', $infantAge);
    $iKey = 0;
    foreach ($form['passenger'] as $k => $v){
        $codePass = 'ADT';
        if($k == 'children') {
            $codePass = 'CNN';
        }
        if($k == 'infant')
            $codePass = 'INF';
        if(!empty($v)){
            $ii = 0;
            foreach ($v as $kk => $vv){
                $currentYear = date('Y');
                $currentMonth = date('m');
                $currentDay = date('d');
                $age = '40';
                if($k == 'adult'){
                    $dobYear = '1979';
                }elseif($k == 'children'){
                    $dobYear = $currentYear - $childAgeArr[$ii];
                    $age = $childAgeArr[$ii];
                }else{
                    $dobYear = $currentYear - $infantAgeArr[$ii];
                    $age = $infantAgeArr[$ii];
                }

                $dobData = $dobYear . '-' . $currentMonth . '-' . $currentDay;
                $bookingTraveler .= '<BookingTraveler xmlns="http://www.travelport.com/schema/common_v42_0"  TravelerType="'. $codePass .'" Age="'. $age .'" DOB="'. $dobData .'" Gender="M" Nationality="'. $vv['nationality'] .'" Key="' . $keyTraveler[$iKey] .'">
        <BookingTravelerName Prefix="'. $vv['title'] .'" First="'. $vv['first_name'] .'" Last="'. $vv['last_name'] .'" />
        <DeliveryInfo>
            <ShippingAddress Key="'. uniqid($iKey) .'">
                <Street>'. $form['address']['address'] .'</Street>
                <City>'. $form['address']['city'] .'</City>
                <State>'. $form['address']['state'] .'</State>
                <PostalCode>'. $form['address']['zip_code'] .'</PostalCode>
                <Country>'. $vv['nationality'] .'</Country>
            </ShippingAddress>
        </DeliveryInfo>
        <PhoneNumber Location="DEN" CountryCode="1" AreaCode="303" Number="'. $vv['phone'] .'" />
        <Email EmailID="'. $vv['email'] .'" />
        <Address>
            <AddressName>'. $form['address']['address'] .'</AddressName>
            <Street>'. $form['address']['address'] .'</Street>
            <City>'. $form['address']['city'] .'</City>
            <State>'. $form['address']['state'] .'</State>
            <PostalCode>'. $form['address']['zip_code'] .'</PostalCode>
            <Country>'. $vv['nationality'] .'</Country>
        </Address>
    </BookingTraveler>';
                $ii++;
                $iKey++;
            }
        }
    }
}

if($form['paymentType'] == 'credit_card') {
	$a = '<com:FormOfPayment xmlns="http://www.travelport.com/schema/common_v42_0" Type="Credit" Key="1">
                <com:CreditCard Type="' . $form['card']['card_type'] . '" CVV="' . $form['card']['card_cvv'] . '" ExpDate="' . $form['card']['card_expires'] . '" Name="'. $form['card']['card_holder'] .'" Number="' . $form['card']['card_number'] . '">
                    <com:BillingAddress>
                        <com:AddressName>' . $form['address']['address'] . '</com:AddressName>
                        <com:Street>' . $form['address']['address'] . '</com:Street>
                        <com:City>' . $form['address']['city'] . '</com:City>
                        <com:PostalCode>' . $form['address']['zip_code'] . '</com:PostalCode>
                        <com:Country>' . $vv['nationality'] . '</com:Country>
                    </com:BillingAddress>
                </com:CreditCard>
            </com:FormOfPayment>';
}else{
	$a = '<FormOfPayment xmlns="http://www.travelport.com/schema/common_v42_0" Type="Cash" Key="1" />';
}


$message='<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
    <soapenv:Header />
    <soapenv:Body>
        <AirCreateReservationReq xmlns="http://www.travelport.com/schema/universal_v42_0" TraceId="'. $trace_id .'" AuthorizedBy="'. $user .'" TargetBranch="'. $target_branch .'" ProviderCode="1G" RetainReservation="Both">
    <BillingPointOfSaleInfo xmlns="http://www.travelport.com/schema/common_v42_0" OriginApplication="UAPI" />
    '. $bookingTraveler . $a .'

    <AirPricingSolution xmlns="http://www.travelport.com/schema/air_v42_0" Key="'. $air_pricing_sol['Key'] .'" TotalPrice="'. $air_pricing_sol['TotalPrice'] .'" BasePrice="'. $air_pricing_sol['BasePrice'] .'" ApproximateTotalPrice="'. $air_pricing_sol['ApproximateTotalPrice'] .'" ApproximateBasePrice="'. $air_pricing_sol['ApproximateBasePrice'] .'" EquivalentBasePrice="'. $air_pricing_sol['EquivalentBasePrice'] .'" Taxes="'. $air_pricing_sol['Taxes'] .'" Fees="'. $air_pricing_sol['Fees'] .'">';

		if(!empty($air_pricing_sol_seg)){
			foreach ($air_pricing_sol_seg as $k => $v){
				if(isset($air_segment_reverb[$v['@attributes']['Key']])){
					$air_segment_temp = $air_segment_reverb[$v['@attributes']['Key']]['@attributes'];
					$message .= '<AirSegment Key="'. $air_segment_temp['Key'] .'" OptionalServicesIndicator="'. $air_segment_temp['OptionalServicesIndicator'] .'" AvailabilityDisplayType="'. $air_segment_temp['AvailabilityDisplayType'] .'" Group="'. $air_segment_temp['Group'] .'" Carrier="'. $air_segment_temp['Carrier'] .'" FlightNumber="'. $air_segment_temp['FlightNumber'] .'" Origin="'. $air_segment_temp['Origin'] .'" Destination="'. $air_segment_temp['Destination'] .'" DepartureTime="'. $air_segment_temp['DepartureTime'] .'" ArrivalTime="'. $air_segment_temp['ArrivalTime'] .'" FlightTime="'. $air_segment_temp['FlightTime'] .'" TravelTime="'. $air_segment_temp['TravelTime'] .'" Distance="'. $air_segment_temp['Distance'] .'" ProviderCode="'. $air_segment_temp['ProviderCode'] .'" ClassOfService="'. $air_segment_temp['ClassOfService'] .'">
            <CodeshareInfo OperatingCarrier="'. $air_segment_temp['Carrier'] .'" />
        </AirSegment>';
				}
			}
		}

		if(!empty($air_pricing_info)){
			foreach ($air_pricing_info as $k => $v){
				$air_pricing_info_attr = $v['@attributes'];
				$message .= '<AirPricingInfo PricingMethod="'. $air_pricing_info_attr['PricingMethod'] .'" Key="'. $air_pricing_info_attr['Key'] .'" TotalPrice="'. $air_pricing_info_attr['TotalPrice'] .'" BasePrice="'. $air_pricing_info_attr['BasePrice'] .'" ApproximateTotalPrice="'. $air_pricing_info_attr['ApproximateTotalPrice'] .'" ApproximateBasePrice="'. $air_pricing_info_attr['ApproximateBasePrice'] .'" Taxes="'. $air_pricing_info_attr['Taxes'] .'" ProviderCode="'. $air_pricing_info_attr['ProviderCode'] .'">';

				$fare_info = $v['airFareInfo'];

				if(!empty($fare_info)){
					foreach ($fare_info as $kk => $vv){
						$fare_info_attr = $vv['@attributes'];
						$message .= '<FareInfo PromotionalFare="false" FareFamily="Economy Flex Plus" DepartureDate="'. $fare_info_attr['DepartureDate'] .'" Amount="'. $fare_info_attr['Amount'] .'" EffectiveDate="'. $fare_info_attr['EffectiveDate'] .'" Destination="'. $fare_info_attr['Destination'] .'" Origin="'. $fare_info_attr['Origin'] .'" PassengerTypeCode="'. $fare_info_attr['PassengerTypeCode'] .'" FareBasis="'. $fare_info_attr['FareBasis'] .'" Key="'. $fare_info_attr['Key'] .'">
                <FareRuleKey FareInfoRef="'. $fare_info_attr['Key'] .'" ProviderCode="'. $air_pricing_info_attr['ProviderCode'] .'">'. $vv['airFareRuleKey'] .'</FareRuleKey>';

						$brand = $vv['airBrand'];
						$brand_attr = $brand['@attributes'];
						if(isset($brand_attr['BrandFound']) && $brand_attr['BrandFound'] == 'false'){
							$message .= '<Brand Key="'. $brand_attr['Key'] .'" BrandFound="false">';
						}else{
							$message .= '<Brand Key="'. $brand_attr['Key'] .'" BrandID="'. $brand_attr['BrandID'] .'" Name="'. $brand_attr['Name'] .'" Carrier="'. $brand_attr['Carrier'] .'">';
							$brand_title = $brand['airTitle'];
							if(!empty($brand_title)){
								foreach ($brand_title as $kkk => $vvv){
									$message .= '';
								}
							}
						}


						$brand_optional = $brand['airOptionalServices'];
						if(isset($brand_optional['airOptionalService']) && !empty($brand_optional['airOptionalService'])){
							$message .= '<OptionalServices>';
							foreach ($brand_optional['airOptionalService'] as $a => $b){
								$message .= '<OptionalService AssessIndicator="MileageAndCurrency" Chargeable="'. $b['@attributes']['Chargeable'] .'" Key="'. $b['@attributes']['Key'] .'" Type="'. $b['@attributes']['Type'] .'">';

								if(isset($b['common_v42_0ServiceData']) && !empty($b['common_v42_0ServiceData'])){
									foreach ($b['common_v42_0ServiceData'] as $c => $d){
										$message .= '<ServiceData xmlns="http://www.travelport.com/schema/common_v42_0" AirSegmentRef="'. $d['@attributes']['AirSegmentRef'] .'" />';
									}
								}
								if(isset($b['common_v42_0ServiceInfo'])){
									$message .= '<ServiceInfo xmlns="http://www.travelport.com/schema/common_v42_0">
                                <Description>'. $b['common_v42_0ServiceInfo']['common_v42_0Description'] .'</Description>
                                </ServiceInfo>';
								}

								if(isset($b['airEMD'])){
									$message .= '<EMD AssociatedItem="'. $b['airEMD']['@attributes']['AssociatedItem'] .'" />';
								}
                        		$message .= '</OptionalService>';
							}
							$message .= '</OptionalServices>';
							$message .= '</Brand>';
						}
                        $message .= '</FareInfo>';
					}
				}


				$booking_info = $v['airBookingInfo'];
				if(!empty($booking_info)){
					foreach ($booking_info as $kb => $vb){
						$message .= '<BookingInfo BookingCode="'. $vb['@attributes']['BookingCode'] .'" CabinClass="'. $vb['@attributes']['CabinClass'] .'" FareInfoRef="'. $vb['@attributes']['FareInfoRef'] .'" SegmentRef="'. $vb['@attributes']['SegmentRef'] .'" HostTokenRef="'. $vb['@attributes']['HostTokenRef'] .'" />';
					}
				}

				$tax_info = $v['airTaxInfo'];
				if(!empty($tax_info)){
					foreach ($tax_info as $kt => $vt){
						$message .= '<TaxInfo Amount="'. $vt['@attributes']['Amount'] .'" Category="'. $vt['@attributes']['Category'] .'" Key="'. $vt['@attributes']['Key'] .'" />';
					}
				}

				$passenger_info = $v['airPassengerType'];
				$message .= '<PassengerType Code="'. $passenger_info['@attributes']['Code'] .'" BookingTravelerRef="'. $keyTraveler[$k] .'"/>';
                $message .= '</AirPricingInfo>';
			}
		}

		if(!empty($air_pricing_host)){
			foreach ($air_pricing_host as $k => $v){
				$message .= '<HostToken xmlns="http://www.travelport.com/schema/common_v42_0" Key="'. $v['Attributes']['Key'] .'">'. $v['Text'][0] .'</HostToken>';
			}
		}

       $message .= '
    </AirPricingSolution>
    <ActionStatus xmlns="http://www.travelport.com/schema/common_v42_0" Type="ACTIVE" TicketDate="T*" ProviderCode="1G" />
    <Payment xmlns="http://www.travelport.com/schema/common_v42_0" Key="2" Type="Itinerary" FormOfPaymentRef="1" Amount="'. $air_pricing_sol['TotalPrice'] .'" />
    '. $xmlSeat .'
</AirCreateReservationReq>
    </soapenv:Body>
</soapenv:Envelope>';
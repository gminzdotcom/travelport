<?php
/**
 * Created by PhpStorm.
 * User: Jream
 * Date: 3/26/2019
 * Time: 3:09 PM
 */
class ST_TravelPort_Settings extends BaseController {
	private static $_inst;
	public function __construct() {
		parent::__construct();
		add_action( 'admin_menu', array( $this, '__addAdminMenu' ), 50 );
		add_action( 'admin_init', array( $this, '__saveSettings' ) );

		add_action('st_more_user_menu', array($this, '__addMenuUserDashboard'), 10, 2);
		add_filter('st_more_user_menu_link_page', array($this, '__addMoreLinkPage'));
        add_filter('st_more_content_page', array($this, '__addContentLinkPage'), 10, 2);
        add_filter('st_mixed_search_form_tab', array($this, '__addMixedSearchTab'));
        add_filter('st_mixed_search_form_group_fields', array($this, '__addMixedGroupFields'));
	}

	public function __addMixedGroupFields($fields){
	    array_push($fields, [
            'type' => 'textfield',
            'heading' => __('Shortcode content', ST_TEXTDOMAIN),
            'param_name' => 'shortcode',
            'dependency' => array(
                'element' => 'tab_service',
                'value' => array('st_shortcode')
            ),
        ]);

	    return $fields;
    }

    public function __addMixedSearchTab($fields){
	    $fields[__('Car', ST_TEXTDOMAIN)] = 'st_cars';
        $fields[__('Shortcode', ST_TEXTDOMAIN)] = 'st_shortcode';

        return $fields;
    }

    public function __addContentLinkPage($content, $sc){
	    if($sc == 'my-travelport'){
            $content = st_travelport_load_view('booking-history');
        }

        return $content;
    }

	public function __addMoreLinkPage($links){
	    array_push($links, 'my-travelport');
	    return $links;
    }

	public function __addMenuUserDashboard($user_link, $sc){
        ?>
            <li class="<?php if ($sc == 'my-travelport'){echo 'active';}  ?>">
                <a href="<?php echo TravelHelper::get_user_dashboared_link($user_link, 'my-travelport'); ?>">
                    <img src="<?php echo get_template_directory_uri()."/v2/images/dashboard/ico_hotel.svg";?>" alt="" class="st-icon-menu">
                    <span><?php _e("Flight Booking", ST_TEXTDOMAIN) ?></span>
                </a>
            </li>
        <?php
    }

	public function __addAdminMenu() {
		if ( current_user_can( 'manage_options' ) && class_exists( 'ST_TravelPort_API' ) ) {
			$menu_page = $this->getMenuPage();
			add_submenu_page(
				apply_filters( 'ot_theme_options_menu_slug', 'st_traveler_option' ),
				$menu_page['page_title'],
				$menu_page['menu_title'],
				$menu_page['capability'],
				$menu_page['menu_slug'],
				$menu_page['function'] );
		}
	}

	function getMenuPage() {
		$page = array(
			'page_title' => esc_html__( "ST TravelPort", 'st_travelport' ),
			'menu_title' => esc_html__( "ST TravelPort", 'st_travelport' ),
			'capability' => 'manage_options',
			'menu_slug'  => 'st-travelport',
			'function'   => [ $this, '__callbackTravelPortPage' ],
			'icon_url'   => 'dashicons-chart-line',
			'position'   => 35
		);

		return $page;
	}

	function __callbackTravelPortPage() {
		echo st_travelport_load_view( 'settings', '', true);
	}

	public function getSettings() {
		$config = array(
			"general" => array(
				"name"     => esc_html__( 'General', 'st_travelport' ),
				"sections" => array(
					"general_option" => array(
						'id'     => 'general_option',
						'label'  => esc_html__( 'General Options', 'st_travelport' ),
						'fields' => array(
                            array(
                                'id'    => 'sanbox',
                                'label' => esc_html__( "Use demo data", 'st_travelport' ),
                                'type'  => 'select',
                                'options' => array(
                                    array(
                                        'value' => 'yes',
                                        'label' => __('Yes', 'st_travelport')
                                    ),
                                    array(
                                        'value' => 'no',
                                        'label' => __('No', 'st_travelport')
                                    ),
                                )
                            ),
							array(
								'label' => esc_html__( 'TravelPort Account', 'st_travelport' ),
								'type'  => 'title'
							),
							array(
								'id'    => 'user_id',
								'label' => esc_html__( "Universal API User ID", 'st_travelport' ),
								'type'  => 'text',
							),
							array(
								'id'    => 'password',
								'label' => esc_html__( "Universal API Password", 'st_travelport' ),
								'type'  => 'text',
							),
							array(
								'id'    => 'targer_branch',
								'label' => esc_html__( "Target Branch", 'st_travelport' ),
								'type'  => 'text',
							),
							array(
								'id'    => 'branch_code',
								'label' => esc_html__( "Branch Code", 'st_travelport' ),
								'type'  => 'text',
							),
							array(
								'id'    => 'endpoint',
								'label' => esc_html__( "Endpoint", 'st_travelport' ),
								'type'  => 'select',
								'options' => array(
									array(
										'value' => 'https://emea.universal-api.pp.travelport.com/B2BGateway/connect/uAPI/',
										'label' => 'https://emea.universal-api.pp.travelport.com/B2BGateway/connect/uAPI/'
									),
									array(
										'value' => 'https://apac.universal-api.pp.travelport.com/B2BGateway/connect/uAPI/',
										'label' => 'https://apac.universal-api.pp.travelport.com/B2BGateway/connect/uAPI/'
									)
								)
							),
                            array(
                                'id'    => 'search_item',
                                'label' => esc_html__( "Number of item per page search", 'st_travelport' ),
                                'type'  => 'text',
                            ),
							array(
								'id'    => 'currency',
								'label' => esc_html__( "Currency", 'st_travelport' ),
								'type'  => 'select',
								'options' => TravelHelper::ot_all_currency()
							),
							array(
								'id'    => 'search_by',
								'label' => esc_html__( "Search Airport by", 'st_travelport' ),
								'type'  => 'select',
								'options' => array(
									array(
										'value' => 'country',
										'label' => esc_html__( 'Country', 'st_travelport' )
									),
									array(
										'value' => 'city',
										'label' => esc_html__( 'City', 'st_travelport' )
									)
								)
							),
						)
					),
				),
			),
            "page" => array(
                "name"     => esc_html__( 'Pages', 'st_travelport' ),
                "sections" => array(
                    "page_option" => array(
                        'id'     => 'page_option',
                        'label'  => '',
                        'fields' => array(
                            array(
                                'label' => esc_html__( 'TravelPort Pages', 'st_travelport' ),
                                'type'  => 'title'
                            ),
                            array(
                                'id'    => 'search_page',
                                'label' => esc_html__( "Search Result Page", 'st_travelport' ),
                                'type'  => 'select_page',
                            ),
                        )
                    ),
                ),
            ),
            'import'  => array(
                'name'     => esc_html__( "Import", 'st_travelport' ),
                'sections' => array(
                    'city_import' => array(
                        'id'     => 'city_import',
                        'label'  => esc_html__( "City import", 'st_travelport' ),
                        'fields' => array(
                            array(
                                'label' => esc_html__( 'Import City data', 'st_travelport' ),
                                'type'  => 'title'
                            ),
                            array(
                                'id'    => 'sttp_import_city',
                                'label' => esc_html__( "Import City Data", 'st_travelport' ),
                                'type'  => 'custom_button',
                                'text'  => esc_html__( 'Run Import', 'st_travelport' ),
                                'table' => 'sttp_city'
                            ),
                            array(
                                'id'    => 'sttp_import_airplane',
                                'label' => esc_html__( "Import Airplane Data", 'st_travelport' ),
                                'type'  => 'custom_button',
                                'text'  => esc_html__( 'Run Import', 'st_travelport' ),
                                'table' => 'sttp_airplane'
                            ),
                            array(
                                'id'    => 'sttp_import_airport',
                                'label' => esc_html__( "Import Airport Data", 'st_travelport' ),
                                'type'  => 'custom_button',
                                'text'  => esc_html__( 'Run Import', 'st_travelport' ),
                                'table' => 'sttp_airport'
                            ),
                            array(
                                'id'    => 'sttp_import_carrier',
                                'label' => esc_html__( "Import Carrier Data", 'st_travelport' ),
                                'type'  => 'custom_button',
                                'text'  => esc_html__( 'Run Import', 'st_travelport' ),
                                'table' => 'sttp_carrier'
                            ),
                            array(
                                'id'    => 'sttp_import_country',
                                'label' => esc_html__( "Import Country Data", 'st_travelport' ),
                                'type'  => 'custom_button',
                                'text'  => esc_html__( 'Run Import', 'st_travelport' ),
                                'table' => 'sttp_country'
                            )
                        )
                    ),
                )
            ),
		);

		return $config;
	}

	function __saveSettings() {
		if ( ! empty( $_POST['st_travelport_save_settings'] ) and wp_verify_nonce( $_REQUEST['st_travelport_save_settings_field'], "st_travelport_action" ) ) {
			$full_settings = $this->getSettings();
			if ( ! empty( $full_settings ) ) {
				$is_tab = '';
				if ( isset( $_GET['st_tab'] ) ) {
					$is_tab = $_GET['st_tab'];
				}
				$is_section = '';
				if ( isset( $_GET['st_section'] ) ) {
					$is_section = $_GET['st_section'];
				}
				if ( empty( $is_tab ) and ! empty( $full_settings ) ) {
					$tmp_tab = $full_settings;
					$tmp_key = array_keys( $tmp_tab );
					$is_tab  = array_shift( $tmp_key );
				}
				if ( empty( $is_section ) and ! empty( $full_settings[ $is_tab ]['sections'] ) ) {
					$tmp_section = $full_settings[ $is_tab ]['sections'];
					$tmp_key     = array_keys( $tmp_section );
					$is_section  = array_shift( $tmp_key );
				}
				$custom_settings = $full_settings[ $is_tab ]['sections'][ $is_section ]['fields'];


				foreach ( $custom_settings as $key => $value ) {
					switch ( $value['type'] ) {
						case "multi-checkbox":
							$custom_multi_checkbox = $value['value'];
							foreach ( $custom_multi_checkbox as $key_multi => $value_multi ) {
								$key_request   = 'st_travelport_' . $value_multi['id'];
								$value_request = STInput::request( $key_request );
								update_option( $key_request, $value_request );
							}
							break;
						case "list-item":
							$id_list_item  = $value['id'];
							$data          = array();
							$key_request   = 'wpbooking_list_item';
							$value_request = STInput::request( $key_request );
							if ( ! empty( $value_request[ $id_list_item ] ) ) {
								$data = $value_request[ $id_list_item ];
							}
							unset( $data['__number_list__'] );
							$id_save = 'st_travelport_' . $value['id'];
							update_option( $id_save, $data );
							break;
						case "checkbox":
							if ( isset( $value['id'] ) ) {
								$key_request   = 'st_travelport_' . $value['id'];
								$value_request = STInput::post( $key_request );
								if ( ! $value_request ) {
									delete_option( $key_request );
								} else {
									update_option( $key_request, $value_request );
								}
							}
							break;
						default:
							if ( isset( $value['id'] ) ) {
								$key_request   = 'st_travelport_' . $value['id'];
								$value_request = STInput::post( $key_request );
								update_option( $key_request, $value_request );
							}
							break;
					}
				}
			}
		}
	}

	public static function inst(){
		if(!self::$_inst)
			self::$_inst = new self();

		return self::$_inst;
	}
}

ST_TravelPort_Settings::inst();
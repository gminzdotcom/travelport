<?php
/**
 * Created by PhpStorm.
 * User: Jream
 * Date: 3/27/2019
 * Time: 9:30 PM
 */
class BaseController{
	private $endpoint;
	private $xml;
	private $target_branch;
	private $credential;
	private $provider;
	private $respondXML;

	public function __construct() {
		//$this->endpoint = 'https://emea.universal-api.pp.travelport.com/B2BGateway/connect/uAPI/';
        //$this->endpoint = 'https://apac.universal-api.pp.travelport.com/B2BGateway/connect/uAPI/';
		$this->endpoint = get_option('st_travelport_endpoint');
		$this->target_branch = get_option('st_travelport_targer_branch');
		$username = get_option('st_travelport_user_id');
		$password = get_option('st_travelport_password');
		$this->credential = "$username:$password";
		$this->provider = get_option('st_travelport_branch_code');
	}

	public function sttpGetOption($key){
        return get_option('st_travelport_' . $key);
    }

	public function setEndPoint($url){
		$this->endpoint = $url;
	}

	public function setXML($xml){
		$this->xml = $xml;
	}

	public function getTargetBranch(){
		return $this->target_branch;
	}

	public function getProvider(){
		return $this->provider;
	}

	public function sendRequest($api){
		$auth = base64_encode("$this->credential");
		$soap_do = curl_init ($this->endpoint . $api);
		$header = array(
			"Content-Type: text/xml;charset=UTF-8",
			"Accept: gzip,deflate",
			"Cache-Control: no-cache",
			"Pragma: no-cache",
			"SOAPAction: \"\"",
			"Authorization: Basic $auth",
			"Content-length: ".strlen($this->xml),
		);

		curl_setopt($soap_do, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($soap_do, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($soap_do, CURLOPT_POST, true );
		curl_setopt($soap_do, CURLOPT_POSTFIELDS, $this->xml);
		curl_setopt($soap_do, CURLOPT_HTTPHEADER, $header);
		curl_setopt($soap_do, CURLOPT_RETURNTRANSFER, true);
		$return = curl_exec($soap_do);

		$this->respondXML = $this->prettyPrint($return);

	}

	function prettyPrint($result){
		$dom = new DOMDocument;
		$dom->preserveWhiteSpace = false;
		$dom->loadXML($result);
		$dom->formatOutput = true;
		return $dom->saveXML();
	}

    public function getRespondSeatMapArray(){
	    $sanbox = $this->sttpGetOption('sanbox');
	    if($sanbox == 'yes'){
            $rp = file_get_contents(ST_TravelPort()->_dir_path . '/inc/files/demo/3.SeatMap/SeatMapRsp.xml');
        }else{
            $this->setXML($this->setSeatMapXML());
            $this->sendRequest('AirService');
            $rp = $this->respondXML;
        }

        $xml = simplexml_load_String("$rp", null, null, 'SOAP', true);
        if($xml) {
            //echo "Processing! Please wait!";
        }else{
            //trigger_error("Encoding Error!", E_USER_ERROR);
            $arr = [];
            $arr['faultcode'] = __('Encoding Error!', 'st_travelport');
            $arr['faultstring'] = __('Have an error when get search result', 'st_travelport');
            return $arr;
        }

        $Results = $xml->children('SOAP',true);
        foreach($Results->children('SOAP',true) as $fault){
            if(strcmp($fault->getName(),'Fault') == 0){
                trigger_error("Error occurred request/response processing!", E_USER_ERROR);
            }
        }

        $segment = array();
        $traveler = array();
        $seat = array();
        $optinal = array();
        foreach($Results->children('air',true) as $nodes) {
            foreach ($nodes->children('air', true) as $hsr) {
                //AirSegmentList
                if (strcmp($hsr->getName(), 'AirSegment') == 0) {
                    $arr_temp = array();
                    foreach($hsr->attributes() as $a => $b	){
                        $arr_temp['Attributes'][$a] = "$b";
                    }
                    foreach($hsr->children('air',true) as $codeshareref) {
                        if (strcmp($codeshareref->getName(), 'CodeshareInfo') == 0) {
                            foreach ($codeshareref->attributes() as $aa => $bb) {
                                $arr_temp['CodeshareInfo'][$aa] = "$bb";
                            }
                        }
                    }
                    $segment[] = $arr_temp;
                }

                //Traveler
                if (strcmp($hsr->getName(), 'SearchTraveler') == 0) {
                    $arr_temp = array();
                    foreach($hsr->attributes() as $a => $b	){
                        $arr_temp['Attributes'][$a] = "$b";
                    }
                    foreach($hsr->children('common_v42_0', true) as $travelerref) {
                        if (strcmp($travelerref->getName(), 'Name') == 0) {
                            foreach ($travelerref->attributes() as $aa => $bb) {
                                $arr_temp['Name'][$aa] = "$bb";
                            }
                        }
                    }
                    $traveler[] = $arr_temp;
                }

                //Optinal
                if (strcmp($hsr->getName(), 'OptionalServices') == 0) {
                    foreach($hsr->children('air', true) as $optinalref) {
                        if (strcmp($optinalref->getName(), 'OptionalService') == 0) {
                            $arr_temp = array();
                            foreach ($optinalref->attributes() as $aa => $bb) {
                                $arr_temp[$aa] = "$bb";
                            }
                            foreach($optinalref->children('common_v42_0', true) as $servicedataref) {
                                if (strcmp($servicedataref->getName(), 'ServiceData') == 0) {
                                    foreach ($servicedataref->attributes() as $aaa => $bbb) {
                                        $arr_temp[$aaa] = "$bbb";
                                    }
                                }
                            }
                            $optinal[] = $arr_temp;
                        }
                    }
                }

                //Seat
                if (strcmp($hsr->getName(), 'Rows') == 0) {
                    $arr_temp = array();
                    foreach($hsr->attributes() as $a => $b	){
                        $arr_temp['Attributes'][$a] = "$b";
                    }

                    foreach($hsr->children('air', true) as $rowref) {
                        if (strcmp($rowref->getName(), 'Row') == 0) {
                            $row_temp = array();
                            foreach ($rowref->attributes() as $aa => $bb) {
                                $row_temp[$aa] = "$bb";
                            }
                            foreach($rowref->children('air', true) as $facilityref) {
                                if (strcmp($facilityref->getName(), 'Facility') == 0) {
                                    $facility_temp = array();
                                    foreach ($facilityref->attributes() as $aaa => $bbb) {
                                        $facility_temp['Attributes'][$aaa] = "$bbb";
                                    }
                                    foreach($facilityref->children('air', true) as $characteristicref) {
                                        if (strcmp($characteristicref->getName(), 'Characteristic') == 0) {
                                            $charac_temp = array();
                                            foreach ($characteristicref->attributes() as $aaaa => $bbbb) {
                                                $charac_temp = "$bbbb";
                                            }
                                            $facility_temp['Characteristic'][] = $charac_temp;
                                        }
                                    }
                                    $row_temp['Facility'][] = $facility_temp;
                                }
                            }
                            $arr_temp['Row'][] = $row_temp;
                        }
                    }
                    $seat[] = $arr_temp;
                }
            }
        }

        $optinal_reverb = array();
        if(!empty($optinal)){
            foreach ($optinal as $k => $v){
                $optinal_reverb[$v['Key']] = $v;
            }
        }

        $seat_reverb = array();
        if(!empty($seat)){
           foreach ($seat as $k => $v){
               $seat_reverb[$v['Attributes']['SegmentRef']] = $v['Row'];
           }
        }

        $traveler_reverb = array();
        if(!empty($traveler)){
            foreach ($traveler as $k => $v){
                if($v['Attributes']['Code'] != 'INF'){
                    $traveler_reverb[] = $v;
                }
            }
        }


        $_SESSION['sttp_seat'] = array(
            'segment' => $segment,
            'traveler' => $traveler_reverb,
            'seat' => $seat_reverb,
            'optional' => $optinal_reverb
        );

        return array(
	        'segment' => $segment,
	        'traveler' => $traveler_reverb,
	        'seat' => $seat_reverb,
	        'optional' => $optinal_reverb
        );
    }

	public function getRespondAirBookArray(){
        $rp = $this->respondXML;
        $xml = simplexml_load_String("$rp", null, null, 'SOAP', true);

        if($xml) {
            //echo "Processing! Please wait!";
        }else{
            trigger_error("Encoding Error!", E_USER_ERROR);
        }
        $Results = $xml->children('SOAP',true);
        foreach($Results->children('SOAP',true) as $fault){
            if(strcmp($fault->getName(),'Fault') == 0){
                //trigger_error("Error occurred request/response processing!", E_USER_ERROR);
                return $this->getFaultMessage($fault);
            }
        }

        //Continue handler

    }

    private function getFaultMessage($fault){
	    $arr = [];
        foreach($fault->children() as $k => $faultref) {
            if (strcmp($faultref->getName(), 'faultcode') == 0) {
                $arr['faultcode'] = (array)$faultref[0];
            }
            if (strcmp($faultref->getName(), 'faultstring') == 0) {
                $arr['faultstring'] = (array)$faultref[0];
            }
        }

        $res = array();
        if(!empty($arr)){
            foreach ($arr as $k => $v){
                $res[$k] = $v[0];
            }
        }
        return $res;
    }

	public function getRespondAirPriceArray(){
        $sanbox = $this->sttpGetOption('sanbox');
        if($sanbox == 'yes'){
            $rp = file_get_contents(ST_TravelPort()->_dir_path . '/inc/files/demo/2.AirPrice/AirPriceRsp.xml');
        }else{
            $rp = $this->respondXML;
        }
		$a = new Travelport();
		$dataAirPrice = $a->XML2ARR($rp);
		$_SESSION['sttp_AirPrice2'] = $dataAirPrice;

		$xml = simplexml_load_String("$rp", null, null, 'SOAP', true);

		if($xml) {
            //echo "Processing! Please wait!";
        }else{
			//trigger_error("Encoding Error!", E_USER_ERROR);
            return false;
		}
		$Results = $xml->children('SOAP',true);
		foreach($Results->children('SOAP',true) as $fault){
			if(strcmp($fault->getName(),'Fault') == 0){
                dd($rp);die;
			}
		}
		$message = array();
		foreach($Results->children('air',true) as $nodes) {
			foreach ( $nodes->children( 'common_v42_0', true ) as $hsr ) {
				$arr_temp = array();
				if(strcmp($hsr->getName(),'ResponseMessage') == 0){
					$arr_temp['Content'] = (array)$hsr[0];
					foreach($hsr->attributes() as $a => $b	){
						$arr_temp['Attributes'][$a] = "$b";
					}
				}
				$message[$arr_temp['Attributes']['Type']][] = $arr_temp;
			}
		}

		$rs = array();
		$segment = array();
		$price = array();
		foreach($Results->children('air',true) as $nodes) {
			foreach ( $nodes->children( 'air', true ) as $hsr ) {
				//AirSegmentList
				if(strcmp($hsr->getName(),'AirItinerary') == 0){
					foreach($hsr->children('air',true) as $hp){
						$arr_temp = array();
						if(strcmp($hp->getName(),'AirSegment') == 0){
							foreach($hp->attributes() as $a => $b	){
								$arr_temp['Attributes'][$a] = "$b";
							}
							foreach($hp->children('air',true) as $k => $flyref) {
								if ( strcmp( $flyref->getName(), 'FlightDetails' ) == 0 ) {
									foreach($flyref->attributes() as $aa => $bb	){
										$arr_temp['FlightDetails'][$aa] = "$bb";
									}
								}
								if ( strcmp( $flyref->getName(), 'CodeshareInfo' ) == 0 ) {
									$arr_temp['CodeshareInfoName'] = (array)$flyref[0];
									foreach($flyref->attributes() as $aa => $bb	){
										$arr_temp['CodeshareInfo'][$aa] = "$bb";
									}
								}
							}
						}
                        $segment[] = $arr_temp;
						if($arr_temp['Attributes']['Group'] == 0){
							$rs['OutBound'][] = $arr_temp;
						}else{
							$rs['InBound'][] = $arr_temp;
						}

					}
				}
				if(strcmp($hsr->getName(),'AirPriceResult') == 0){
					foreach($hsr->children('air',true) as $hp){
						$arr_temp = array();
						if(strcmp($hp->getName(),'AirPricingSolution') == 0){
							foreach($hp->attributes() as $a => $b	){
								$arr_temp['Attributes'][$a] = "$b";
							}
                            foreach($hp->children('common_v42_0',true) as $k => $tokenref) {
                                if (strcmp($tokenref->getName(), 'HostToken') == 0) {
                                    $arr_token = array();
                                    $arr_token['Text'] = (array)$tokenref[0];
                                    foreach($tokenref->attributes() as $aa => $bb	){
                                        $arr_token['Attributes'][$aa] = "$bb";
                                    }
                                }
                                $arr_temp['HostToken'][] = $arr_token;
                            }
                            $price[] = $arr_temp;
						}
					}
				}
			}
		}

		return array(
			'message' => $message,
			'data' => $rs,
			'prices' => $price,
            'segment' => $segment
		);
	}

	public function getRespondArray($api = 'LowFareSearchRsp', $type = 'flight_option'){
        $sanbox = $this->sttpGetOption('sanbox');
        if($sanbox == 'yes'){
            $rp = file_get_contents(ST_TravelPort()->_dir_path . '/inc/files/demo/1.LowFareSearch/LowFareSearchRsp.xml');
        }else{
            $rp = $this->respondXML;
        }

		$xml = simplexml_load_String("$rp", null, null, 'SOAP', true);
		if($xml) {
            //echo "Processing! Please wait!";
        }else{
            $arr = [];
            $arr['faultcode'] = __('Encoding Error!', 'st_travelport');
            $arr['faultstring'] = __('Have an error when get search result', 'st_travelport');
            return $arr;
			//trigger_error("Encoding Error!", E_USER_ERROR);
		}
		$Results = $xml->children('SOAP',true);
		foreach($Results->children('SOAP',true) as $fault){
			if(strcmp($fault->getName(),'Fault') == 0){
				return $this->getFaultMessage($fault);
			}
		}

		$rs = array();
		$flyrs = array();
		$pointrs = array();
		$farers = array();
		foreach($Results->children('air',true) as $nodes){
			foreach($nodes->children('air',true) as $hsr){
				//AirSegmentList
				if(strcmp($hsr->getName(),'AirSegmentList') == 0){
					foreach($hsr->children('air',true) as $hp){
						if(strcmp($hp->getName(),'AirSegment') == 0){
							foreach($hp->attributes() as $a => $b	){
								$rs[$a][] = "$b";
							}
							foreach($hp->children('air',true) as $flyref) {
								if ( strcmp( $flyref->getName(), 'FlightDetailsRef' ) == 0 ) {
									foreach($flyref->attributes() as $aa => $bb	){
										$rs['FlightDetailsRef'][] = "$bb";
									}
								}
							}
						}
					}
				}

                //FareInfoList
                if(strcmp($hsr->getName(),'FareInfoList') == 0){
                    foreach($hsr->children('air',true) as $hp){
                        if(strcmp($hp->getName(),'FareInfo') == 0){
                            foreach($hp->attributes() as $a => $b	){
                                $farers[$a][] = "$b";
                            }
                            foreach($hp->children('air',true) as $fareref) {
                            	$arr_fare_temp = array();
                                if ( strcmp( $fareref->getName(), 'BaggageAllowance' ) == 0 ) {
	                                foreach($fareref->children('air',true) as $bagref) {
		                                if ( strcmp( $bagref->getName(), 'MaxWeight' ) == 0 ) {
			                                foreach($bagref->attributes() as $aa => $bb	){
				                                $arr_fare_temp['MaxWeight'][$aa] = "$bb";
			                                }
		                                }
                                        if ( strcmp( $bagref->getName(), 'NumberOfPieces' ) == 0 ) {
                                            if($bagref[0]->asXML() != '') {
                                                $arr_fare_temp['NumberOfPieces'] = $bagref[0]->asXML();
                                            }
                                        }
	                                }
                                }
	                            $farers['BaggageAllowance'][] = $arr_fare_temp;
                            }
                        }
                    }
                }

				//FlightDetailsList
			    if(strcmp($hsr->getName(),'FlightDetailsList') == 0){
					foreach($hsr->children('air',true) as $hp){
						if(strcmp($hp->getName(),'FlightDetails') == 0){
							foreach($hp->attributes() as $a => $b	){
								$flyrs[$a][] = "$b";
							}
						}
					}
				}

				//PointList
				$flight_item_option = array();
				$flight_option = array();
				if(strcmp($hsr->getName(),'AirPricePointList') == 0){
					$k = 0;
					foreach($hsr->children('air',true) as $hp){
						$arr_temp = array();
						if(strcmp($hp->getName(),'AirPricePoint') == 0){
							foreach($hp->attributes() as $a => $b	){
								$arr_temp[$a] = "$b";
							}
							foreach($hp->children('air',true) as $pointref) {
								if ( strcmp( $pointref->getName(), 'AirPricingInfo' ) == 0 ) {
									foreach($pointref->children('air',true) as $inforef) {
										if ( strcmp( $inforef->getName(), 'FlightOptionsList' ) == 0 ) {
											$flight_option = array();
											$kk = 0;
											foreach($inforef->children('air',true) as $flightref) {
												$flight_option_temp = array();
												if ( strcmp( $flightref->getName(), 'FlightOption' ) == 0 ) {
													foreach($flightref->attributes() as $aa => $bb	){
														$flight_option_temp[$aa] = "$bb";
													}
													$flight_item_option = array();
													$kkk = 0;
													foreach($flightref->children('air',true) as $flightOptionref) {
														$flight_item_option_temp = array();
														if ( strcmp( $flightOptionref->getName(), 'Option' ) == 0 ) {
															foreach($flightOptionref->attributes() as $aaa => $bbb	){
																$flight_item_option_temp[$aaa] = "$bbb";
															}
															$booking_info = array();
															$kkkk = 0;
															foreach($flightOptionref->children('air',true) as $bookingInforef) {
																$booking_info_temp = array();
																if ( strcmp( $bookingInforef->getName(), 'BookingInfo' ) == 0 ) {
																	foreach($bookingInforef->attributes() as $aaaa => $bbbb	){
																		$booking_info_temp[$aaaa] = "$bbbb";
																	}
																	$booking_info[$kkkk] = $booking_info_temp;
																	$kkkk++;
																}
															}

															$flight_item_option[$kkk]['Attributes'] = $flight_item_option_temp;
															$flight_item_option[$kkk]['BookingInfo'] = $booking_info;
															$kkk++;
														}
													}
												}
												$flight_option[$kk]['Attributes'] = $flight_option_temp;
												$flight_option[$kk]['Options'] = $flight_item_option;
												$kk++;
											}
										}
									}
								}
							}
						}
						$pointrs[$k]['Attributes'] = $arr_temp;
						$pointrs[$k]['FlightList'] = $flight_option;
						$k++;
					}
				}
			}
		}
		$segment = array();
		if(!empty($rs)){
			$arr_key = array_keys($rs);
			foreach ($rs['Key'] as $k => $v){
				$sub_arr = array();
				foreach ($arr_key as $kk => $vv){
				    if(isset($rs[$vv][$k]))
					    $sub_arr[$vv] = $rs[$vv][$k];
				}
				$segment[$v] = $sub_arr;
			}
		}

        $flight = array();
        if(!empty($flyrs)){
            $arr_key = array_keys($flyrs);
            foreach ($flyrs['Key'] as $k => $v){
                $sub_arr = array();
                foreach ($arr_key as $kk => $vv){
                    $sub_arr[$vv] = isset($flyrs[$vv][$k]) ? $flyrs[$vv][$k] : '';
                }
                $flight[$v] = $sub_arr;
            }
        }

        $fare = array();
        if(!empty($farers)){
            $arr_key = array_keys($farers);
            foreach ($farers['Key'] as $k => $v){
                $sub_arr = array();
                foreach ($arr_key as $kk => $vv){
                    $sub_arr[$vv] = isset($farers[$vv][$k]) ? $farers[$vv][$k] : '';
                }
                $fare[$v] = $sub_arr;
            }
        }

		return array(
			'posts' => $pointrs,
			'segments' => $segment,
            'flights' => $flight,
            'fare' => $fare
		);
	}

	public function setSeatMapXML(){
        $targetBranch = $this->getTargetBranch();
        $provider = $this->getProvider();

        $airPrice = STTP_Search::inst()->getAirPrice();

        $cart = $_SESSION['sttp_cart'];
        
        $segmentXML = '';
        $hostTokenXML = '';
        $passengerXML = '';

        if(!empty($airPrice['prices'][0]['HostToken'])){
			foreach ($airPrice['prices'][0]['HostToken'] as $k => $v){
				$hostTokenXML .= '<HostToken xmlns="http://www.travelport.com/schema/common_v42_0" Key="'. $v['Attributes']['Key'] .'">'. $v['Text'][0] .'</HostToken>';
			}
		}

        if(!empty($airPrice['segment'])){
            foreach ($airPrice['segment'] as $k => $v){
                $segmentXML .= '<AirSegment Key="'. $v['Attributes']['Key'] .'" HostTokenRef="9rNxKo3R2BKAD9aICAAAAA==" Equipment="'. $v['Attributes']['Equipment'] .'" AvailabilityDisplayType="'. $v['Attributes']['AvailabilityDisplayType'] .'" Group="'. $v['Attributes']['Group'] .'" Carrier="'. $v['Attributes']['Carrier'] .'" FlightNumber="'. $v['Attributes']['FlightNumber'] .'" Origin="'. $v['Attributes']['Origin'] .'" Destination="'. $v['Attributes']['Destination'] .'" DepartureTime="'. $v['Attributes']['DepartureTime'] .'" ArrivalTime="'. $v['Attributes']['ArrivalTime'] .'" FlightTime="'. $v['Attributes']['FlightTime'] .'" TravelTime="'. $v['Attributes']['TravelTime'] .'" Distance="'. $v['Attributes']['Distance'] .'" ProviderCode="'. $provider .'" ClassOfService="'. $v['Attributes']['ClassOfService'] .'">
        <CodeshareInfo OperatingCarrier="'. $v['CodeshareInfo']['OperatingCarrier'] .'" />
    </AirSegment>';
            }
        }

        for($i = 0; $i < $cart['adultNumber']; $i++){
            $BookingTravelerRef = base64_encode(uniqid('ADT' . $i));
            $KeyPassenger = base64_encode(uniqid('ADT' . $i));
            $passengerXML .= '<SearchTraveler Code="ADT" Age="40" Key="'.$KeyPassenger.'">
        <Name xmlns="http://www.travelport.com/schema/common_v42_0" Prefix="Mr" First="John" Last="Smith" />
    </SearchTraveler>';
        }

        $childAge = explode(',', $cart['childAge']);
        for($i = 0; $i < $cart['childNumber']; $i++){
            $BookingTravelerRef = base64_encode(uniqid('CNN' . $i));
            $KeyPassenger = base64_encode(uniqid('CNN' . $i));
            $passengerXML .='<SearchTraveler Code="CNN" Age="'. $childAge[$i] .'" Key="'.$KeyPassenger.'">
        <Name xmlns="http://www.travelport.com/schema/common_v42_0" Prefix="MSTR" First="Michael" Last="Scott" />
    </SearchTraveler>';
        }

        $infantAge = explode(',', $cart['infantAge']);
        for($i = 0; $i < $cart['infantNumber']; $i++){
            $BookingTravelerRef = base64_encode(uniqid('INF' . $i));
            $KeyPassenger = base64_encode(uniqid('INF' . $i));
            $passengerXML .= ' <SearchTraveler Code="INF" Age="'. $infantAge[$i] .'" Key="'. $BookingTravelerRef .'">
        <Name xmlns="http://www.travelport.com/schema/common_v42_0" Prefix="Miss" First="Chloe" Last="Spencer" />
    </SearchTraveler>';
        }

        $xml = '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">
       <s:Body xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><SeatMapReq xmlns="http://www.travelport.com/schema/air_v42_0" TraceId="021674bb-7768-4762-a74b-d67eaf4ba111" AuthorizedBy="Travelport" TargetBranch="'. $targetBranch .'" ReturnSeatPricing="true" ReturnBrandingInfo="true">
    <BillingPointOfSaleInfo xmlns="http://www.travelport.com/schema/common_v42_0" OriginApplication="uAPI" />
    '. $segmentXML . $hostTokenXML . $passengerXML .'
</SeatMapReq></s:Body>
    </s:Envelope>';
        return $xml;

    }

	public function setAirPriceXML(){
		$targetBranch = $this->getTargetBranch();
		$provider = $this->getProvider();

		$cart = $_SESSION['sttp_cart'];

		$segmentXML = '';
        $airPricingCommandXML = '';
        $passengerXML = '';
		if(!empty($cart['cartOutBound'])) {
            foreach ($cart['cartOutBound']['BookingInfo'] as $k => $v){
                $segmentTemp = $cart['segments'][$v['SegmentRef']];
                $fareTemp = $cart['fares'][$v['FareInfoRef']];
                $segmentXML .= '<AirSegment Key="'. $segmentTemp['Key'] .'" AvailabilitySource="'. $segmentTemp['AvailabilitySource'] .'" Equipment="'. $segmentTemp['Equipment'] .'" AvailabilityDisplayType="'. $segmentTemp['AvailabilityDisplayType'] .'" Group="'. $segmentTemp['Group'] .'" Carrier="'. $segmentTemp['Carrier'] .'" FlightNumber="'. $segmentTemp['FlightNumber'] .'" Origin="'. $segmentTemp['Origin'] .'" Destination="'. $segmentTemp['Destination'] .'" DepartureTime="'. $segmentTemp['DepartureTime'] .'" ArrivalTime="'. $segmentTemp['ArrivalTime'] .'" FlightTime="'. $segmentTemp['FlightTime'] .'" Distance="'. $segmentTemp['Distance'] .'" ProviderCode="'. $provider .'" ClassOfService="'. $v['BookingCode'] .'" />';
                $airPricingCommandXML .= ' <AirSegmentPricingModifiers AirSegmentRef="'. $v['SegmentRef'] .'" FareBasisCode="'. $fareTemp['FareBasis'] .'"><PermittedBookingCodes><BookingCode Code="'. $v['BookingCode'] .'" /></PermittedBookingCodes></AirSegmentPricingModifiers>';
            }
		}
        if(!empty($cart['cartInBound'])) {
            foreach ($cart['cartInBound']['BookingInfo'] as $k => $v){
                $segmentTemp = $cart['segments'][$v['SegmentRef']];
                $fareTemp = $cart['fares'][$v['FareInfoRef']];
                $segmentXML .= '<AirSegment Key="'. $segmentTemp['Key'] .'" AvailabilitySource="'. $segmentTemp['AvailabilitySource'] .'" Equipment="'. $segmentTemp['Equipment'] .'" AvailabilityDisplayType="'. $segmentTemp['AvailabilityDisplayType'] .'" Group="'. $segmentTemp['Group'] .'" Carrier="'. $segmentTemp['Carrier'] .'" FlightNumber="'. $segmentTemp['FlightNumber'] .'" Origin="'. $segmentTemp['Origin'] .'" Destination="'. $segmentTemp['Destination'] .'" DepartureTime="'. $segmentTemp['DepartureTime'] .'" ArrivalTime="'. $segmentTemp['ArrivalTime'] .'" FlightTime="'. $segmentTemp['FlightTime'] .'" Distance="'. $segmentTemp['Distance'] .'" ProviderCode="'. $provider .'" ClassOfService="'. $v['BookingCode'] .'" />';
                $airPricingCommandXML .= ' <AirSegmentPricingModifiers AirSegmentRef="'. $v['SegmentRef'] .'" FareBasisCode="'. $fareTemp['FareBasis'] .'"><PermittedBookingCodes><BookingCode Code="'. $v['BookingCode'] .'" /></PermittedBookingCodes></AirSegmentPricingModifiers>';
            }
        }

        for($i = 0; $i < $cart['adultNumber']; $i++){
            $BookingTravelerRef = base64_encode(uniqid('ADT' . $i));
            $KeyPassenger = base64_encode(uniqid('ADT' . $i));
                $passengerXML .= '<SearchPassenger xmlns="http://www.travelport.com/schema/common_v42_0" Code="ADT" BookingTravelerRef="'. $BookingTravelerRef .'" Key="'.$KeyPassenger.'" />';
        }

        $childAge = explode(',', $cart['childAge']);
        for($i = 0; $i < $cart['childNumber']; $i++){
            $BookingTravelerRef = base64_encode(uniqid('CNN' . $i));
            $KeyPassenger = base64_encode(uniqid('CNN' . $i));
            $passengerXML .= '<SearchPassenger xmlns="http://www.travelport.com/schema/common_v42_0" Code="CNN" Age="'. $childAge[$i] .'" BookingTravelerRef="'. $BookingTravelerRef .'" Key="'.$KeyPassenger.'" />';
        }

        $infantAge = explode(',', $cart['infantAge']);
        for($i = 0; $i < $cart['infantNumber']; $i++){
            $BookingTravelerRef = base64_encode(uniqid('INF' . $i));
            $KeyPassenger = base64_encode(uniqid('INF' . $i));
            $passengerXML .= '<SearchPassenger xmlns="http://www.travelport.com/schema/common_v42_0" Code="INF" Age="'. $infantAge[$i] .'" BookingTravelerRef="'. $BookingTravelerRef .'" Key="'.$KeyPassenger.'" />';
        }

		$xml = '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">
       <s:Body xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <AirPriceReq xmlns="http://www.travelport.com/schema/air_v42_0" TraceId="98dec312-faeb-4e26-b599-e759a287863f" AuthorizedBy="Travelport" TargetBranch="'. $targetBranch .'">
  <BillingPointOfSaleInfo xmlns="http://www.travelport.com/schema/common_v42_0" OriginApplication="uAPI" />
  <AirItinerary>'. $segmentXML .'</AirItinerary>
  <AirPricingModifiers InventoryRequestType="DirectAccess">
    <BrandModifiers ModifierType="FareFamilyDisplay" />
  </AirPricingModifiers>
  '. $passengerXML .'
  <AirPricingCommand>'. $airPricingCommandXML .'</AirPricingCommand>
  <FormOfPayment xmlns="http://www.travelport.com/schema/common_v42_0" Type="Credit" />
</AirPriceReq>
      </s:Body>
    </s:Envelope>';

		return $xml;
	}

	public function setLowFareAirXML($origin, $destination, $start, $end,  $adult_number, $child_number = 0, $infant_number = 0, $child_age = '', $infant_age = '', $cabin_class = '',  $roundtrip = true){
        $targetBranch = $this->getTargetBranch();
        $provider = $this->getProvider();
		$currentCurrency = get_option('st_travelport_currency');
		if(empty($currentCurrency))
            $currentCurrency = 'GBP';

        $start = date('Y-m-d', strtotime(TravelHelper::convertDateFormat($start)));

		$airLegModifiers = '';
		if(!empty($cabin_class)) {
			$airLegModifiers = '<AirLegModifiers>
  <PreferredCabins>
    <CabinClass Type="'. $cabin_class .'" xmlns="http://www.travelport.com/schema/common_v42_0"/>
  </PreferredCabins>
</AirLegModifiers>';
		}

        $roundtripXML = '';
        if($roundtrip){
            $end = date('Y-m-d', strtotime(TravelHelper::convertDateFormat($end)));
            $roundtripXML = '<SearchAirLeg>
        <SearchOrigin>
            <CityOrAirport xmlns="http://www.travelport.com/schema/common_v42_0" Code="'. $destination .'" PreferCity="true" />
        </SearchOrigin>
        <SearchDestination>
            <CityOrAirport xmlns="http://www.travelport.com/schema/common_v42_0" Code="'. $origin .'" PreferCity="true" />
        </SearchDestination>
        <SearchDepTime PreferredTime="'. $end .'" />
        '. $airLegModifiers .'
    </SearchAirLeg>';
        }

        $adultXML = '';
        for($i = 0; $i < $adult_number; $i++){
            $adultXML .= '<SearchPassenger xmlns="http://www.travelport.com/schema/common_v42_0" Code="ADT" />';
        }

        $childXML = '';
        for($i = 0; $i < $child_number; $i++){
            $currentYear = date('Y');
            $currentMonth = date('m');
            $currentDay = date('d');
            $dobYear = $currentYear - $child_age[$i];
            $dobData = $dobYear . '-' . $currentMonth . '-' . $currentDay;
            $childXML .= '<SearchPassenger xmlns="http://www.travelport.com/schema/common_v42_0" Code="CNN" Age="'. $child_age[$i] .'" DOB="'. $dobData .'" />';
        }

        $infantXML = '';
        for($i = 0; $i < $infant_number; $i++){
            $currentYear = date('Y');
            $currentMonth = date('m');
            $currentDay = date('d');
            $dobYear = $currentYear - $infant_age[$i];
            $dobData = $dobYear . '-' . $currentMonth . '-' . $currentDay;
            $infantXML .= '<SearchPassenger xmlns="http://www.travelport.com/schema/common_v42_0" Code="INF" Age="'. $infant_age[$i] .'" DOB="'. $dobData .'" />';
        }

        $xml = '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">
       <s:Body xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <LowFareSearchReq xmlns="http://www.travelport.com/schema/air_v42_0" TraceId="021674bb-7768-4762-a74b-d67eaf4ba111" TargetBranch="' . $targetBranch . '" ReturnUpsellFare="true">
    <BillingPointOfSaleInfo xmlns="http://www.travelport.com/schema/common_v42_0" OriginApplication="uAPI" />
    <SearchAirLeg>
        <SearchOrigin>
            <CityOrAirport xmlns="http://www.travelport.com/schema/common_v42_0" Code="'. $origin .'" PreferCity="true" />
        </SearchOrigin>
        <SearchDestination>
            <CityOrAirport xmlns="http://www.travelport.com/schema/common_v42_0" Code="'. $destination .'" PreferCity="true" />
        </SearchDestination>
        <SearchDepTime PreferredTime="'. $start .'" />
        '. $airLegModifiers .'
    </SearchAirLeg>
    '. $roundtripXML .'
    <AirSearchModifiers>
        <PreferredProviders>
            <Provider xmlns="http://www.travelport.com/schema/common_v42_0" Code="' . $provider . '" />
        </PreferredProviders>
    </AirSearchModifiers>
    ' . $adultXML . $childXML . $infantXML . '  
    <AirPricingModifiers CurrencyType="'. $currentCurrency .'" FaresIndicator="AllFares">
        <AccountCodes>
            <AccountCode xmlns="http://www.travelport.com/schema/common_v42_0" Code="-" />
        </AccountCodes>
    </AirPricingModifiers>
</LowFareSearchReq>
      </s:Body>
    </s:Envelope>';
        return $xml;
    }
}
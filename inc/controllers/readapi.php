<?php
/**
 * Created by PhpStorm.
 * User: Jream
 * Date: 3/27/2019
 * Time: 9:29 PM
 */
class STTP_Search extends BaseController {
    static $_inst;
    static $data_static = array();
    public function __construct()
    {
        parent::__construct();
        add_action('wp_ajax_sttp_get_airport_by_country_code', array($this, '__getAirportByCountryCode'));
        add_action('wp_ajax_nopriv_sttp_get_airport_by_country_code', array($this, '__getAirportByCountryCode'));

        add_action('wp_ajax_sttp_get_airport_by_city_code', array($this, '__getAirportByCityCode'));
        add_action('wp_ajax_nopriv_sttp_get_airport_by_city_code', array($this, '__getAirportByCityCode'));

        add_action('wp_ajax_sttp_get_equipment_data', array($this, '__getAirPlaneData'));
        add_action('wp_ajax_nopriv_sttp_get_equipment_data', array($this, '__getAirPlaneData'));
        add_action('wp_ajax_sttp_get_carrier_data', array($this, '__getCarrierData'));
        add_action('wp_ajax_nopriv_sttp_get_carrier_data', array($this, '__getCarrierData'));

        add_action('wp_ajax_sttp_get_airport_data', array($this, '__getAirportData'));
        add_action('wp_ajax_nopriv_sttp_get_airport_data', array($this, '__getAirportData'));

        add_action('wp_ajax_sttp_add_cart_data', array($this, '__addCartData'));
	    add_action('wp_ajax_nopriv_sttp_add_cart_data', array($this, '__addCartData'));
        add_action('wp_ajax_sttp_create_air_booking', array($this, '__createAirBooking'));
        add_action('wp_ajax_nopriv_sttp_create_air_booking', array($this, '__createAirBooking'));
        add_action('wp_ajax_sttp_get_seatmap', array($this, '__getSeatMap'));
        add_action('wp_ajax_nopriv_sttp_get_seatmap', array($this, '__getSeatMap'));
        add_action('wp_ajax_sttp_set_seatmap', array($this, '__setSeatMap'));
        add_action('wp_ajax_nopriv_sttp_set_seatmap', array($this, '__setSeatMap'));
        add_action('wp_ajax_sttp_create_reservation', array($this, '__createReservation'));
        add_action('wp_ajax_nopriv_sttp_create_reservation', array($this, '__createReservation'));

        add_action('wp_ajax_sttp_get_booking_history', array($this, '__getBookingHistoryByID'));

        add_action('st_cart_item_html_travelport_api', array($this, '__getCartItemHtml'));
        add_action('st_more_fields_checkout_form', array($this, '__getMoreFielsCheckoutForm'));


        add_filter('st_booking_form_fields', array($this, '__addBookingFieldForm'));
        add_action('st_save_order_other_table', array($this, '__saveBookingSTTPTable'));

	    add_action( 'admin_menu', array($this, '__addFlightBookingMenu') );

	    add_action('st_payment_success_cart_info', array($this, '__addPaymentSuccessCartInfo'), 10, 2);
	    add_filter('st_array_item_custom_payment_info', array($this, '__addItemCustomPayment'));

	    add_action('st_custom_payment_info', array($this, '__addCustomPaymentInfo'), 10, 2);

	    add_filter('st_order_success_custommer_billing', array($this, '__showPassengerInfo'), 10,2);

	    add_action('wp_ajax_sttp_get_city_data', array($this, '__getCityDataByText'));
        add_action('wp_ajax_nopriv_sttp_get_city_data', array($this, '__getCityDataByText'));
    }

    public function __getCityDataByText(){
        $text = STInput::post('search_text', '');
        if(!empty($text)){
            $city_data = STTP_CityModel::inst()->getCityByName($text);
            if(!empty($city_data)){
                echo json_encode(array(
                    'status' => true,
                    'data' => $city_data
                ));die;
            }else{
                echo json_encode(array(
                    'status' => false,
                ));die;
            }
        }
    }

    public function getOrderStatus($order_id){
    	$res = ST_Order_Item_Model::inst()
		    ->select(array('status'))
		    ->where('order_item_id', $order_id)
		    ->where('st_booking_post_type', 'travelport_api')
		    ->get()->result();
    	if(!empty($res)){
    		$res = array_shift($res);
    		return $res['status'];
	    }
    	return '';
    }

    public function __showPassengerInfo($customer_infomation, $order_code){
    	$order_meta = get_post_meta($order_code, 'item_id', true);
    	if($order_meta == 'travelport_api'){
		    $customer_infomation .= st_travelport_load_view('custom_passenger_info', array('order_code' => $order_code));
	    }
		return $customer_infomation;
    }

    public function __addCustomPaymentInfo($key, $order_id){
	    if($key == 'travelport_api'){
		    $cart_info = get_post_meta($order_id, 'st_cart_info', true);
		    echo st_travelport_load_view('custom_payment', array('cart_info' => $cart_info));
	    }
    }

    public function __addItemCustomPayment($arr){
    	array_push($arr, 'travelport_api');
    	return $arr;
    }

    public function __addPaymentSuccessCartInfo($key, $order_id){
    	if($key == 'travelport_api'){
    		$cart_info = get_post_meta($order_id, 'st_cart_info', true);
		    echo st_travelport_load_view('payment_success_cart_info', array('cart_info' => $cart_info));
	    }
    }

    public function __addFlightBookingMenu(){
	    add_menu_page(
		    __( 'Flight Booking', 'textdomain' ),
		    'Flight Booking',
		    'manage_options',
		    'sttp-flight-booking',
		    array($this, '__callbackFlightBookingFunc'),
		    'dashicons-flight-alt-st',
		    35
	    );
    }

    public function __callbackFlightBookingFunc(){
    	echo st_travelport_load_view('flight_booking', '', true);
    }

    public function getBookingTravelerData($order_id){
    	$adult_number = get_post_meta($order_id, 'adultNumber', true);
	    $child_number = get_post_meta($order_id, 'childNumber', true);
	    $infant_number = get_post_meta($order_id, 'infantNumber', true);
	    $child_age = get_post_meta($order_id, 'childAge', true);
	    $infant_age = get_post_meta($order_id, 'infantAge', true);

	    $traveler = array();
	    if(!empty($adult_number)){
	    	for($i = 0; $i < $adult_number; $i++){
				array_push($traveler, array(
					'type' => 'Adult',
					'first_name' => get_post_meta($order_id, 'st_first_name_adult_' . $i),
					'last_name' => get_post_meta($order_id, 'st_last_name_adult_' . $i),
					'email' => get_post_meta($order_id, 'st_email_adult_' . $i),
					'phone' => get_post_meta($order_id, 'st_phone_adult_' . $i),
				));
		    }
	    }

	    if(!empty($child_number)){
	    	$child_age = explode(',', $child_age);
		    for($i = 0; $i < $child_number; $i++){
			    array_push($traveler, array(
				    'type' => 'Children',
				    'first_name' => get_post_meta($order_id, 'st_first_name_child_' . $i),
				    'last_name' => get_post_meta($order_id, 'st_last_name_child_' . $i),
				    'email' => get_post_meta($order_id, 'st_email_child_' . $i),
				    'phone' => get_post_meta($order_id, 'st_phone_child_' . $i),
				    'age' => $child_age[$i]
			    ));
		    }
	    }

	    if(!empty($infant_number)){
	    	$infant_age = explode(',', $infant_age);
		    for($i = 0; $i < $infant_number; $i++){
			    array_push($traveler, array(
				    'type' => 'Infant',
				    'first_name' => get_post_meta($order_id, 'st_first_name_infant_' . $i),
				    'last_name' => get_post_meta($order_id, 'st_last_name_infant_' . $i),
				    'email' => get_post_meta($order_id, 'st_email_infant_' . $i),
				    'phone' => get_post_meta($order_id, 'st_phone_infant_' . $i),
				    'age' => $infant_age[$i]
			    ));
		    }
	    }
	    return $traveler;
    }

    public function __saveBookingSTTPTable($order_id){
	    $st_cart = STCart::get_cart_item();
	    if($st_cart['key'] == 'travelport_api') {
		    $cart        = $_SESSION['sttp_cart'];
		    $cart_reverb = $cart;
		    unset( $cart_reverb['segments'] );
		    unset( $cart_reverb['fares'] );
		    unset( $cart_reverb['flights'] );

		    if ( isset( $cart['cartOutBound'] ) ) {
			    if ( isset( $cart['cartOutBound']['BookingInfo'] ) && ! empty( $cart['cartOutBound']['BookingInfo'] ) ) {
				    foreach ( $cart['cartOutBound']['BookingInfo'] as $k => $v ) {
					    $cart_reverb['cartOutBound']['BookingInfo'][ $k ]['SegmentRef']  = $cart['segments'][ $v['SegmentRef'] ];
					    $cart_reverb['cartOutBound']['BookingInfo'][ $k ]['FareInfoRef'] = $cart['fares'][ $v['FareInfoRef'] ];
				    }
			    }
		    }

		    if ( isset( $cart['cartInBound'] ) ) {
			    if ( isset( $cart['cartInBound']['BookingInfo'] ) && ! empty( $cart['cartInBound']['BookingInfo'] ) ) {
				    foreach ( $cart['cartInBound']['BookingInfo'] as $k => $v ) {
					    $cart_reverb['cartInBound']['BookingInfo'][ $k ]['SegmentRef']  = $cart['segments'][ $v['SegmentRef'] ];
					    $cart_reverb['cartInBound']['BookingInfo'][ $k ]['FareInfoRef'] = $cart['fares'][ $v['FareInfoRef'] ];
				    }
			    }
		    }

		    if ( isset( $cart_reverb['cartOutBound'] ) ) {
			    if ( isset( $cart_reverb['cartOutBound']['BookingInfo'] ) && ! empty( $cart_reverb['cartOutBound']['BookingInfo'] ) ) {
				    foreach ( $cart_reverb['cartOutBound']['BookingInfo'] as $k => $v ) {
					    $flightRef                                                                          = $cart['flights'][ $v['SegmentRef']['FlightDetailsRef'] ];
					    $cart_reverb['cartOutBound']['BookingInfo'][ $k ]['SegmentRef']['FlightDetailsRef'] = $flightRef;
				    }
			    }
		    }

		    if ( isset( $cart_reverb['cartInBound'] ) ) {
			    if ( isset( $cart_reverb['cartInBound']['BookingInfo'] ) && ! empty( $cart_reverb['cartInBound']['BookingInfo'] ) ) {
				    foreach ( $cart_reverb['cartInBound']['BookingInfo'] as $k => $v ) {
					    $flightRef                                                                         = $cart['flights'][ $v['SegmentRef']['FlightDetailsRef'] ];
					    $cart_reverb['cartInBound']['BookingInfo'][ $k ]['SegmentRef']['FlightDetailsRef'] = $flightRef;
				    }
			    }
		    }

		    $seat_passenger = array();
		    if(isset($_SESSION['sttp_seat_passenger']))
		        $seat_passenger = $_SESSION['sttp_seat_passenger'];

		    $totalPrice = $st_cart['value']['price'];
		    if(!empty($seat_passenger)){
			    $seat_price = $_SESSION['sttp_total_price'];
			    $totalPrice += $seat_price;
		    }
		    $id_inserted = STTP_OrderModel::inst()->insert( array(
			    'order_id'      => $order_id,
			    'origin'        => $cart['fromCode'],
			    'destination'   => $cart['toCode'],
			    'flight_type'   => $cart['flightWay'],
			    'fly_out'       => $cart['flyOut'],
			    'fly_back'      => $cart['flyBack'],
			    'number_adult'  => $cart['adultNumber'],
			    'number_child'  => $cart['childNumber'],
			    'number_infant' => $cart['infantNumber'],
			    'age_child'     => $cart['childAge'],
			    'age_infant'    => $cart['infantAge'],
			    'booking_info'  => json_encode( $cart_reverb ),
			    'checkout_info' => json_encode($seat_passenger),
			    'user_id'       => get_current_user_id(),
			    'price'         => $totalPrice
		    ) );

		    unset($_SESSION['sttp_seat_passenger']);
		    unset($_SESSION['sttp_total_price']);
	    }
    }

    public function __addBookingFieldForm($checkout_form_fields){
	    //Passenger
	    $cart = STCart::get_cart_item();
	    if(!empty($cart['value']['data']['adultNumber'])){
	    	for ($i = 0; $i < $cart['value']['data']['adultNumber']; $i++){
			    $checkout_form_fields['st_label_adult_' . $i] = array(
					'label' => 'Adult ' . ($i + 1),
					'type' => 'label'
				);
			    $checkout_form_fields['st_first_name_adult_' . $i] =[
				    'label' => 'First Name',
				    'validate' => 'required|trim|strip_tags',
				    'icon' => 'fa-user',
				    'value' => '',
				    'class' => [
					    'form-control',
					    'sttp-passenger-adult-input'
				    ],
			    ];
			    $checkout_form_fields['st_last_name_adult_' . $i] =[
				    'label' => 'Last Name',
				    'validate' => 'required|trim|strip_tags',
				    'icon' => 'fa-user',
				    'value' => '',
				    'class' => [
					    'form-control',
					    'sttp-passenger-adult-input'
				    ],
			    ];
			    $checkout_form_fields['st_email_adult_' . $i] =[
				    'label' => st_get_language('Email'),
				    'placeholder' => st_get_language('email_domain'),
				    'type' => 'text',
				    'validate' => 'required|trim|strip_tags|valid_email',
				    'value' => STInput::post('st_email_adult_' . $i),
				    'icon' => 'fa-envelope'

			    ];
                 $checkout_form_fields['st_phone_adult_' . $i] = [
				    'label' => st_get_language('Phone'),
				    'placeholder' => st_get_language('Your_Phone'),
				    'validate' => 'required|trim|strip_tags',
				    'icon' => 'fa-phone',
				    'value' => STInput::post('st_phone_adult_' . $i),
			    ];
		    }
	    }
	    if(!empty($cart['value']['data']['childNumber'])){
		    for ($i = 0; $i < $cart['value']['data']['childNumber']; $i++){
			    $checkout_form_fields['st_label_child_' . $i] = array(
				    'label' => 'Children ' . ($i + 1),
				    'type' => 'label'
			    );
			    $checkout_form_fields['st_first_name_child_' . $i] =[
				    'label' => 'First Name',
				    'validate' => 'required|trim|strip_tags',
				    'icon' => 'fa-user',
				    'value' => '',
				    'class' => [
					    'form-control',
					    'sttp-passenger-child-input'
				    ],
			    ];
			    $checkout_form_fields['st_last_name_child_' . $i] =[
				    'label' => 'Last Name',
				    'validate' => 'required|trim|strip_tags',
				    'icon' => 'fa-user',
				    'value' => '',
				    'class' => [
					    'form-control',
					    'sttp-passenger-child-input'
				    ],
			    ];
			    $checkout_form_fields['st_email_child_' . $i] =[
				    'label' => st_get_language('Email'),
				    'placeholder' => st_get_language('email_domain'),
				    'type' => 'text',
				    'validate' => 'required|trim|strip_tags|valid_email',
				    'value' => STInput::post('st_email_child_' . $i),
				    'icon' => 'fa-envelope'

			    ];
			    $checkout_form_fields['st_phone_child_' . $i] = [
				    'label' => st_get_language('Phone'),
				    'placeholder' => st_get_language('Your_Phone'),
				    'validate' => 'required|trim|strip_tags',
				    'icon' => 'fa-phone',
				    'value' => STInput::post('st_phone_child_' . $i),
			    ];
		    }
	    }

	    if(!empty($cart['value']['data']['infantNumber'])){
		    for ($i = 0; $i < $cart['value']['data']['infantNumber']; $i++){
			    $checkout_form_fields['st_label_infant_' . $i] = array(
				    'label' => 'Infant ' . ($i + 1),
				    'type' => 'label'
			    );
			    $checkout_form_fields['st_first_name_infant_' . $i] =[
				    'label' => 'First Name',
				    'validate' => 'required|trim|strip_tags',
				    'icon' => 'fa-user',
				    'value' => ''
			    ];
			    $checkout_form_fields['st_last_name_infant_' . $i] =[
				    'label' => 'Last Name',
				    'validate' => 'required|trim|strip_tags',
				    'icon' => 'fa-user',
				    'value' => ''
			    ];
			    $checkout_form_fields['st_email_infant_' . $i] =[
				    'label' => st_get_language('Email'),
				    'placeholder' => st_get_language('email_domain'),
				    'type' => 'text',
				    'validate' => 'required|trim|strip_tags|valid_email',
				    'value' => STInput::post('st_email_infant_' . $i),
				    'icon' => 'fa-envelope'

			    ];
			    $checkout_form_fields['st_phone_infant_' . $i] = [
				    'label' => st_get_language('Phone'),
				    'placeholder' => st_get_language('Your_Phone'),
				    'validate' => 'required|trim|strip_tags',
				    'icon' => 'fa-phone',
				    'value' => STInput::post('st_phone_infant_' . $i),
			    ];
		    }
	    }
	    return $checkout_form_fields;
    }

    public function __getMoreFielsCheckoutForm(){
	    echo st_travelport_load_view('checkout_more_fields');
    }

    public function __getCartItemHtml(){
    	echo st_travelport_load_view('cart_item_html');
    }

    public function getFilterData($posts){
    	global $segments;
    	$arr_price = array();
    	$stops = array();
    	$airline = array();
    	$airport = array();
    	$duration = array();
    	$main_currency =  get_option('st_travelport_currency');
    	if(empty($main_currency))
    	    $main_currency = 'GBP';
		foreach ($posts as $k => $v){
			//Price
			$price = explode($main_currency, $v['Attributes']['TotalPrice']);
			if(!in_array($price[1], $arr_price))
				array_push($arr_price, $price[1]);

			//Stop
			$countBookingInfo = count($v['FlightList'][0]['Options'][0]['BookingInfo']) - 1;
			if(!in_array($countBookingInfo, $stops))
				array_push($stops, $countBookingInfo);

			//Airline
			$first_item = $v['FlightList'][0]['Options'][0];
			$itnObj = STTP_Search::inst()->getItnObj($first_item['BookingInfo'], $segments);
			if(!empty($itnObj['proccess'])){
				foreach ($itnObj['proccess'] as $kk => $vv){
					if(!in_array($vv, $airport))
						array_push($airport, $vv);
				}
			}

			if(!empty($itnObj['carrier'])){
				foreach ($itnObj['carrier'] as $kk => $vv){
					if(!in_array($vv, $airline))
						array_push($airline, $vv);
				}
			}

			//Duration
			if(!empty($first_item)){
				$duration_item = STTP_Search::inst()->formatTravelTime($first_item['Attributes']['TravelTime']);
				if(!in_array($duration_item, $duration))
					array_push($duration, $duration_item);
			}
		}

	    sort($duration);

		return array(
			'price' => array(
				'min' => min($arr_price),
				'max' => max($arr_price)
			),
			'stops' => $stops,
			'airport' => $airport,
			'airline' => $airline,
			'duration' => $duration
		);
    }

    public function __getBookingHistoryByID(){
        $id = STInput::post('data');
        if(!empty($id)){
            $res = STTP_OrderModel::inst()->where('id', $id)->get()->result();
            if(!empty($res)){
                $content = st_travelport_load_view('booking-history-modal', array('res' => array_shift($res)));
                echo json_encode(array(
                    'status' => true,
                    'content' => $content
                ));die;
            }else{
                echo json_encode(array(
                    'status' => false,
                    'message' => __('Have an error when load this booking.', 'st_travelport')
                ));die;
            }
        }else{
            echo json_encode(array(
                'status' => false,
                'message' => __('Have an error when load this booking.', 'st_travelport')
            ));die;
        }
    }

    public function __setSeatMap(){
        $data = STInput::post('data');
        $data = json_decode(wp_unslash($data));

        $arr_temp = array();

        $totalPrice = 0;

        $main_currency =  get_option('st_travelport_currency');
        if(empty($main_currency))
            $main_currency = 'GBP';
        if(!empty($data)){
            foreach ($data as $k => $v){
            	if($v->price != 'Free'){
            		$price = explode($main_currency, $v->price);
            		$totalPrice += $price[1];
	            }
                if(isset($v->seatCode)) {
                    $arr_temp[$v->coderef][] = $v;
                }
            }
        }

        $_SESSION['sttp_seat_passenger'] = $arr_temp;
        $_SESSION['sttp_total_price'] = $totalPrice;

        $html = '';
        if(!empty($arr_temp)){
        	$html .= '<span class="label">' . __('Seat Price', 'st_travelport') . '</span>';
			$html .= '<span class="value">' . TravelHelper::format_money($totalPrice) . '</span>';
        }

	    $html_sub_total = '';
        $total_amount = 0;
	    if(!empty($arr_temp)){
		    $cart = STCart::get_cart_item();
		    $sub_total = TravelHelper::format_money($cart['value']['price']);
		    $html_sub_total .= '<span class="label">' . __('Sub Total', 'st_travelport') . '</span>';
		    $html_sub_total .= '<span class="value">' . $sub_total . '</span>';

		    $total_amount = $cart['value']['price'] + $totalPrice;
		    $total_amount = TravelHelper::format_money($total_amount);
	    }

        echo json_encode(array(
            'status' => true,
	        'html' => $html,
	        'html_sub_total' => $html_sub_total,
	        'html_total_amount' => $total_amount
        ));die;
    }

    public function __getSeatMap(){
        $res = $this->getRespondSeatMapArray();
	    $getData = STInput::post();
	    $peopleData = array();
	    $cart = STCart::get_cart_item();
	    array_shift($cart);

	    if(!empty($cart['value']['data']['adultNumber'])){
	    	for ($i = 0; $i < $cart['value']['data']['adultNumber']; $i++){
			    $peopleData['ADT,' . $i] = array(
				    'first_name' => $getData['st_first_name_adult_' . $i],
				    'last_name' => $getData['st_last_name_adult_' . $i],
			    );
		    }
	    }

	    if(!empty($cart['value']['data']['childNumber'])){
		    for ($i = 0; $i < $cart['value']['data']['childNumber']; $i++){
			    $peopleData['CNN,' . $i] = array(
				    'first_name' => $getData['st_first_name_child_' . $i],
				    'last_name' => $getData['st_last_name_child_' . $i],
			    );
		    }
	    }

        if(!empty($res)) {
	        $content = st_travelport_load_view( 'seatmap', array( 'seat_data' => $res, 'passenger' => $peopleData) );

	        echo json_encode( array(
		        'status'  => true,
		        'content' => $content
	        ) );
	        die;
        }else{
	        echo json_encode( array(
		        'status'  => false,
	        ) );
	        die;
        }
    }

    public function __createAirBooking(){
        $getData = STInput::post();
        $peopleData = array();
        $addressData = array();
        $cardData = array();
        if(!empty($getData['title'])){
            foreach ($getData['title'] as $k => $v){
                if(!empty($v)){
                    foreach ($v as $kk => $vv){
                        $title = $vv[0];
                        $first_name = $getData['first_name'][$k][$kk][0];
                        $last_name = $getData['last_name'][$k][$kk][0];
                        $phone = $getData['phone'][$k][$kk][0];
                        $email = $getData['email'][$k][$kk][0];
                        $nationality = $getData['nationality'][$k][$kk][0];
                        $peopleData[$k][$kk] = array(
                            'title' => $title,
                            'first_name' => $first_name,
                            'last_name' => $last_name,
                            'phone' => $phone,
                            'email' => $email,
                            'nationality' => $nationality
                        );
                    }
                }
            }
        }

        $addressData = array(
            'address' => $getData['address'],
            'city' => $getData['city'],
            'zip_code' => $getData['zip_code'],
            'country' => $getData['country'],
            'state' => $getData['state'],
        );

        $cardData = array(
            'card_type' => $getData['card_type'],
            'card_number' => $getData['card_number'],
            'card_expires' => $getData['card_expires'],
            'card_cvv' => $getData['card_cvv'],
        );

        $payment_type = $getData['sttp_payment'];

        $checkoutData = array(
            'passenger' => $peopleData,
            'address' => $addressData,
            'card' => $cardData,
	        'paymentType' => $payment_type
        );

        $a = new Travelport();
        //$res = file_get_contents(ST_TravelPort()->_dir_path . '/inc/files/respbook.xml');

        $airPriceData2 = $_SESSION['sttp_AirPrice2'];
        $res = $a->airBookingRequset($airPriceData2, $checkoutData);

        $airBookingData = $a->XML2ARR($res);

	    if(!empty($a->errorHandling($airBookingData))){
            echo json_encode(array(
                'status' => false,
                'message' => $a->errorHandling($airBookingData)
            ));die;
        }else{
            $id_inserted = $this->addOrderData($airBookingData, $checkoutData);
            if($id_inserted > 0){
                /*$res_ticket = $a->airTicketRequset($airBookingData);
                dd($res_ticket);die;*/
                echo json_encode(array(
                    'status' => true,
                    'message' => __('Create Air Booking Success.', 'st_travelport')
                ));die;
            }else{
                echo json_encode(array(
                    'status' => false,
                    'message' => __('Have an error when create air booking.', 'st_travelport')
                ));die;
            }
        }
    }

    public function addOrderData($order, $form){
        $cart = $_SESSION['sttp_cart'];
        $id_inserted = STTP_OrderModel::inst()->insert(array(
            'origin' => $cart['fromCode'],
            'destination' => $cart['toCode'],
            'flight_type' => $cart['flightWay'],
            'fly_out' => $cart['flyOut'],
            'fly_back' => $cart['flyBack'],
            'number_adult' => $cart['adultNumber'],
            'number_child' => $cart['childNumber'],
            'number_infant' => $cart['infantNumber'],
            'age_child' => $cart['childAge'],
            'age_infant' => $cart['infantAge'],
            'booking_info' => json_encode($order),
            'checkout_info' => json_encode($form),
        ));

        return $id_inserted;
    }

    public function __addCartData(){
	    $postPos = STInput::post('postPos');
	    $outboundPos = STInput::post('outboundPos');
	    $inboundPos = STInput::post('inboundPos');
	    $fromCode = STInput::post('from_code');
	    $toCode = STInput::post('to_code');
	    $flyOut = STInput::post('fly_out');
	    $flyBack = STInput::post('fly_back');
	    $flightWay = STInput::post('flight_way');
	    $adultNumber = STInput::post('adult_number');
	    $childNumber = STInput::post('child_number');
	    $infantNumber = STInput::post('infant_number');
	    $childAge = STInput::post('sttp_child_age');
	    $infantAge = STInput::post('sttp_infant_age');
	    $cabinClass = STInput::post('preferred_cabin', '');

	    unset($_SESSION['sttp_seat_passenger']);
	    unset($_SESSION['sttp_total_price']);

	    $roundtrip = true;
	    if(empty($flyBack)  || $flightWay == 'oneway')
		    $roundtrip = false;

	    $childAgeTemp = array();
	    $infantAgeTemp = array();
	    if(!empty($childAge))
		    $childAgeTemp = explode(',', $childAge);
	    if(!empty($infantAge))
		    $infantAgeTemp = explode(',', $infantAge);

	    $keySearch = base64_encode($fromCode . $toCode . $flyOut . $flyBack . $flightWay . $adultNumber . $childNumber . $infantNumber . implode(',', $childAgeTemp) . implode(',', $infantAgeTemp) . $cabinClass .  $roundtrip);

	    if(isset($_SESSION['sttpSearchData_' . $keySearch])){
		    $pointrs = $_SESSION['sttpSearchData_' . $keySearch];
	    }else{
		    $pointrs = STTP_Search::inst()->getSearchResults($fromCode, $toCode, $flyOut, $flyBack, $flightWay, $adultNumber, $childNumber, $infantNumber, $childAgeTemp, $infantAgeTemp, $cabinClass, $roundtrip);
	    }

	    $posts = $pointrs['posts'];
	    $segments = $pointrs['segments'];
	    $flights = $pointrs['flights'];
	    $fares = $pointrs['fare'];

	    $cartPost = $posts[$postPos];
	    $cartOutBound=  $cartPost['FlightList'][0]['Options'][$outboundPos];
	    if($flightWay == 'roundtrip') {
		    $cartInBound = $cartPost['FlightList'][1]['Options'][ $inboundPos ];
	    }else{
		    $cartInBound = '';
	    }

	    $_SESSION['sttp_cart'] = array(
		    'cartOutBound' => $cartOutBound,
		    'cartInBound'  => $cartInBound,
		    'segments'     => $segments,
		    'fares' => $fares,
		    'flights'      => $flights,
		    'fromCode' => $fromCode,
		    'toCode' => $toCode,
		    'flyOut' => $flyOut,
		    'flyBack' => $flyBack,
		    'flightWay' => $flightWay,
		    'adultNumber' => $adultNumber,
		    'childNumber' => $childNumber,
		    'infantNumber' => $infantNumber,
		    'childAge' => $childAge,
		    'infantAge' => $infantAge
	    );

	    $item_id = 'travelport_api';
	    $number = 1;

        $main_currency =  get_option('st_travelport_currency');
        if(empty($main_currency))
            $main_currency = 'GBP';

	    $price = explode($main_currency, $cartPost['Attributes']['TotalPrice'])[1];
	    $data = [
		    'item_price'      => explode($main_currency, $cartPost['Attributes']['TotalPrice'])[1],
		    'ori_price'       => explode($main_currency, $cartPost['Attributes']['TotalPrice'])[1],
		    'fromCode' => $fromCode,
		    'toCode' => $toCode,
		    'flyOut' => $flyOut,
		    'flyBack' => $flyBack,
		    'flightWay' => $flightWay,
		    'adultNumber' => $adultNumber,
		    'childNumber' => $childNumber,
		    'infantNumber' => $infantNumber,
		    'childAge' => $childAge,
		    'infantAge' => $infantAge
	    ];

	    STCart::add_cart( $item_id, $number, $price, $data );

	    $cart_link = get_the_permalink(st()->get_option('page_checkout'));

	    echo json_encode(array(
	    	'status' => true,
            'cartLink' => $cart_link
	    ));
	    die;
    }

    public function __getAirportData(){
        $codes = STInput::post('arr_code');
        $codes = '("' . implode('","', $codes) . '")';
        $res = STTP_AirportModel::inst()
            ->where("airport_code IN {$codes}", '', true)
            ->get()->result();
        $arr = array();
        if(!empty($res)){
            foreach ($res as $k => $v){
                $arr[$v['airport_code']] = $v['airport_name'];
            }
            echo json_encode(array(
                'status' => true,
                'data' => $arr
            ));
            die;
        }else{
            echo json_encode(array(
                'status' => false
            ));
            die;
        }
    }

    public function __getCarrierData(){
        $codes = STInput::post('arr_code');
        $codes = '("' . implode('","', $codes) . '")';
        $res = STTP_CarrierModel::inst()
            ->where("carrier_code IN {$codes}", '', true)
            ->get()->result();
        $arr = array();
        if(!empty($res)){
            foreach ($res as $k => $v){
                $arr[$v['carrier_code']] = $v['carrier_name1'];
            }
            echo json_encode(array(
                'status' => true,
                'data' => $arr
            ));
            die;
        }else{
            echo json_encode(array(
                'status' => false
            ));
            die;
        }
    }

    public function __getAirPlaneData(){
        $codes = STInput::post('arr_code');
        $codes = '("' . implode('","', $codes) . '")';
        $res = STTP_AirplaneModel::inst()
            ->where("airplane_code IN {$codes}", '', true)
            ->get()->result();
        $arr = array();
        if(!empty($res)){
            foreach ($res as $k => $v){
                $arr[$v['airplane_code']] = $v['airplane_name1'];
            }
            echo json_encode(array(
                'status' => true,
                'data' => $arr
            ));
            die;
        }else{
            echo json_encode(array(
                'status' => false
            ));
            die;
        }
    }

    public function minuteToHourMinute($minute){
        if($minute > 60){
            $hours = intval($minute/60);
            if(strlen($hours) < 2){
                $hours = '0' . $hours;
            }
            $min = $minute%60;
            if(strlen($min) < 2){
                $min = '0' . $min;
            }
            return $hours . ':' . $min;
        }else{
            if(strlen($minute) < 2){
                $minute = '0' . $minute;
            }
            return '00:' . $minute;
        }
    }

    public function formatTravelTimeInt($str){
        $arr = explode('DT', $str);
        $arr_temp = explode('M', $arr[1]);
        $arr_temp_1 = explode('H', $arr_temp[0]);
        $hour = $arr_temp_1[0];
        $min = $arr_temp_1[1];
        return $hour * 60 + $min;
    }

    public function formatTravelTime($str){
        $arr = explode('DT', $str);
        $arr_temp = explode('M', $arr[1]);
        $arr_temp_1 = explode('H', $arr_temp[0]);
        if (strlen($arr_temp_1[0]) == 1)
	        $arr_temp_1[0] = '0' . $arr_temp_1[0];

        if(strlen($arr_temp_1[1]) == 1)
	        $arr_temp_1[1] = '0' . $arr_temp_1[1];
    	return $arr_temp_1[0] . 'h ' . $arr_temp_1[1] . 'm';
    }

    public function getDateTime($fullDate, $date_format = 'd M'){
    	$arr = explode('T', $fullDate);
    	$time = substr($arr[1], 0, 5);
    	$timeZone = substr($fullDate, strlen($fullDate) - 6, 6);
    	return array(
    		'date' => date($date_format, strtotime($arr[0])),
    		'time' => $time,
		    'time_zone' => $timeZone
	    );
    }

    public function getItnObj($bookingInfo, $segments){
	    $arr_itn = array();
	    $arr_time = array();
	    $arr_segment = array();
	    $arr_carrier = array();
        if(!empty($bookingInfo)){
        	foreach ($bookingInfo as $k => $v){
				$SegmentRef = $segments[$v['SegmentRef']];
				//dd($SegmentRef);
				array_push($arr_segment, $SegmentRef);
				array_push($arr_carrier, $SegmentRef['Carrier']);
				array_push($arr_time, $SegmentRef['DepartureTime']);
				array_push($arr_time, $SegmentRef['ArrivalTime']);
				if($k == 0){
					array_push($arr_itn, $SegmentRef['Origin']);
					array_push($arr_itn, $SegmentRef['Destination']);
				}else{
					array_push($arr_itn, $SegmentRef['Destination']);
				}
	        }
        }
        return array(
        	'proccess' => $arr_itn,
	        'time' => $arr_time,
            'segment' => $arr_segment,
            'carrier' => $arr_carrier
        );
	}

    public function __getAirportByCityCode(){
        $airport_value = STInput::post('country_code');
        if(!empty($airport_value)){
            $airport_value = '"' . str_replace(' ', '","', trim($airport_value)) . '"';
            $airport_value = '(' . $airport_value . ')';
        }
        $airport_list = STTP_AirportModel::inst()
            ->select(array('airport_code', 'airport_name'))
            ->where("airport_code IN {$airport_value}", false, true)
            ->get()->result();

        echo json_encode(array(
            'status' => true,
            'data' => $airport_list
        ));die;
    }

    public function __getAirportByCountryCode(){
        $airport_list = STTP_AirportModel::inst()
            ->select(array('airport_code', 'airport_name'))
            ->where('country_code', $_POST['country_code'])
            ->get()->result();

        echo json_encode(array(
            'status' => true,
            'data' => $airport_list
        ));die;
    }

    public function getAirPrice(){
        //unset($_SESSION['sttp_AirPrice']);
        if(isset($_SESSION['sttp_AirPrice']) && !empty($_SESSION['sttp_AirPrice'])) {
            $res = $_SESSION['sttp_AirPrice'];
        }else{
            $this->setXML($this->setAirPriceXML());
            $this->sendRequest('AirService');
            $res = $this->getRespondAirPriceArray();
            $_SESSION['sttp_AirPrice'] = $res;
        }
	    return $res;
    }

    public function getSearchResults($origin, $destination, $start, $end, $flight_way, $adult_number, $child_number = 0, $infant_number = 0, $child_age = array(), $infant_age = array(), $cabin_class = '', $roundtrip = true){
	    if(isset(self::$data_static) && !empty(self::$data_static)){
			return self::$data_static;
	    }
        $keySearch = base64_encode($origin . $destination . $start . $end . $flight_way . $adult_number . $child_number . $infant_number . implode(',', $child_age) . implode(',', $infant_age) . $cabin_class . $roundtrip);

	    $now = time();
        //unset($_SESSION['sttpSearchData_' . $keySearch] );
        if(isset($_SESSION['sttpSearchData_' . $keySearch]) && $now < $_SESSION['sttp_search_expire_' . $keySearch]){
            $res = $_SESSION['sttpSearchData_' . $keySearch];
        }else{
            $sanbox = $this->sttpGetOption('sanbox');
            if($sanbox != 'yes'){
                $this->setXML($this->setLowFareAirXML($origin, $destination, $start, $end,  $adult_number, $child_number, $infant_number, $child_age, $infant_age, $cabin_class, $roundtrip));
                $this->sendRequest('AirService');
            }
            $res = $this->getRespondArray();
            $_SESSION['sttpSearchData_' . $keySearch] = $res;
	        $_SESSION['sttp_search_start_' . $keySearch] = time();
	        $_SESSION['sttp_search_expire_' . $keySearch] = $_SESSION['sttp_search_start_' . $keySearch] + (30 * 60);
        }
	    self::$data_static = $res;

        return $res;
    }

    public static function inst(){
        if(!self::$_inst)
            self::$_inst = new self();

        return self::$_inst;
    }
}
STTP_Search::inst();
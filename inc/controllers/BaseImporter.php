<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
if ( ! class_exists( 'STTP_BaseImporter' ) ) {
	class STTP_BaseImporter {
		private $_file_url;
		private $_table_name;
		private $_limit_per_run;

		function __construct() {
		}

		/**
		 * @return mixed
		 */
		protected function getLimitPerRun() {
			return $this->_limit_per_run;
		}

		/**
		 * @param mixed $limit_per_run
		 */
		protected function setLimitPerRun( $limit_per_run ) {
			$this->_limit_per_run = $limit_per_run;
		}

		/**
		 * @return mixed
		 */
		protected function getFileUrl() {
			return $this->_file_url;
		}

		/**
		 * @param mixed $file_url
		 */
		protected function setFileUrl( $file_url ) {
			$this->_file_url = $file_url;
		}

		/**
		 * @return mixed
		 */
		protected function getTableName() {
			return $this->_table_name;
		}

		/**
		 * @param mixed $table_name
		 */
		protected function setTableName( $table_name ) {
			global $wpdb;
			$this->_table_name = $wpdb->prefix . $table_name;
		}

		private function getNumberOfColumn() {
			global $wpdb;
			$result  = $wpdb->get_results( 'DESCRIBE ' . $this->getTableName(), ARRAY_A );
			$columns = array();
			foreach ( $result as $row ) {
				$columns[] = $row['Field'];
			}
			$numColumns = count( $columns );

			return $numColumns;
		}

		private function _fillChunck( $array, $parts ) {
			$t      = 0;
			$result = array_fill( 0, $parts - 1, array() );
			$max    = ceil( count( $array ) / $parts );
			foreach ( $array as $v ) {
				count( $result[ $t ] ) >= $max and $t ++;
				$result[ $t ][] = $v;
			}

			return $result;
		}

		protected function runImport() {
			global $wpdb;
			$file_url   = $this->getFileUrl();
			$table      = $this->getTableName();
			$numColumns = $this->getNumberOfColumn();
			$limit_run  = $this->getLimitPerRun();

			$error_message      = '';
			$success_message    = '';
			$message_info_style = '';

			if ( empty( $file_url ) ) {
				$error_message .= '* ' . __( 'No Database Table was selected to export. Please select a Database Table for exportation.', 'st_pkfare' ) . '<br />';
			}
			$ext = pathinfo( $file_url, PATHINFO_EXTENSION );
			if ( $ext !== 'csv' ) {
				$error_message .= '* ' . __( 'The Input File does not contain the .csv file extension. Please choose a valid .csv file.', 'st_pkfare' );
			}

			if ( ! empty( $file_url ) && ( $ext == 'csv' ) ) {
				$db_cols = $wpdb->get_col( "DESC " . $table, 0 );
				if ( ( $fh = @fopen( $file_url, 'r' ) ) !== false ) {
					$values   = array();
					$too_many = '';
					$ij       = 0;
					while ( ( $row = fgetcsv( $fh ) ) !== false ) {
						if ( count( $row ) == $numColumns ) {
							if ( $ij > 0 ) {
								$values[] = '("' . implode( '", "', $row ) . '")';
							}
							$ij ++;
						}
					}

					if ( empty( $values ) && ( $too_many !== 'true' ) ) {
						$error_message .= '* ' . __( 'Columns do not match.', 'st_pkfare' ) . '<br />';
						$error_message .= '* ' . __( 'The number of columns in the database for this table does not match the number of columns attempting to be imported from the .csv file.', 'st_pkfare' ) . '<br />';
						$error_message .= '* ' . __( 'Please verify the number of columns attempting to be imported in the "Select Input File" exactly matches the number of columns displayed in the "Table Preview".', 'st_pkfare' ) . '<br />';
					} else {
						/*$number_of_part = count( $values ) / $limit_run;
						$data_split     = $this->_fillChunck( $values, $number_of_part );

						$db_cols_implode = implode( ',', $db_cols );
						$updateOnDuplicate = ' ON DUPLICATE KEY UPDATE id=id ';
						foreach ( $data_split as $kk => $vv ) {
							$values_implode  = implode( ',', $vv );
							$sql             = 'INSERT INTO ' . $table . ' (' . $db_cols_implode . ') ' . 'VALUES ' . $values_implode . $updateOnDuplicate;
							$db_query_insert = $wpdb->query( $sql );
							if ( $db_query_insert ) {
								$success_message = __( 'Congratulations!  The database has been updated successfully.', 'st_pkfare' );
								$success_message .= '<br /><strong>' . count( $values ) . '</strong> ' . __( 'record(s) were inserted into the', 'st_pkfare' ) . ' <strong>' . $table . '</strong> ' . __( 'database table.', 'st_pkfare' );
							} else {
								$error_message .= '* ' . __( 'There was a problem with the database query.', 'st_pkfare' ) . '<br />';
								$error_message .= '* ' . __( 'A duplicate entry was found in the database for a .csv file entry.', 'st_pkfare' ) . '<br />';
								$error_message .= '* ' . __( 'If necessary; please use the option below to "Update Database Rows".', 'st_pkfare' ) . '<br />';
							}
						}*/
						$db_cols_implode   = implode( ',', $db_cols );
						$values_implode    = implode( ',', $values );
						$updateOnDuplicate = ' ON DUPLICATE KEY UPDATE ';
						foreach ( $db_cols as $db_col ) {
							$updateOnDuplicate .= "$db_col=VALUES($db_col),";
						}
						$updateOnDuplicate = rtrim( $updateOnDuplicate, ',' );
						$sql               = 'INSERT INTO ' . $table . ' (' . $db_cols_implode . ') ' . 'VALUES ' . $values_implode . $updateOnDuplicate;
						$db_query_update   = $wpdb->query( $sql );
						if ( $db_query_update ) {
							$success_message = __( 'Congratulations!  The database has been updated successfully.', 'st_pkfare' );
							$success_message .= '<br /><strong>' . count( $values ) . '</strong> ' . __( 'record(s) were inserted into the', 'st_pkfare' ) . ' <strong>' . $table . '</strong> ' . __( 'database table.', 'st_pkfare' );
						} else {
							$error_message .= '* ' . __( 'There was a problem with the database query.', 'st_pkfare' ) . '<br />';
							$error_message .= '* ' . __( 'A duplicate entry was found in the database for a .csv file entry.', 'st_pkfare' ) . '<br />';
							$error_message .= '* ' . __( 'If necessary; please use the option below to "Update Database Rows".', 'st_pkfare' ) . '<br />';
						}
					}
				} else {
					$error_message .= '* ' . __( 'No valid .csv file was found at the specified url. Please check the "Select Input File" field and ensure it points to a valid .csv file.', 'st_pkfare' ) . '<br />';
				}
			}

			if ( ! empty( $error_message ) ) {
				return array(
					'status'          => false,
					'message_content' => $error_message
				);
			} else {
				return array(
					'status'          => true,
					'message_content' => $success_message
				);
			}
		}
	}
}
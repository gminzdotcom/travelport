<?php
/**
 * Created by PhpStorm.
 * User: HanhDo
 * Date: 4/1/2019
 * Time: 10:37 AM
 */
if ( ! class_exists( 'STTP_CountryModel' ) ) {
    class STTP_CountryModel extends STTP_BaseModel {
        static $_inst = false;

        function __construct() {
            $this->table_version = '1.0.0';
            $this->table_name       = 'sttp_country';
            $this->columns                 = [
                'country_code'         => [ 'type' => "varchar", 'length' => 2 ],
                'country_name'       => [ 'type' => "varchar", 'length' => 255 ],
                'country_currency' => [ 'type' => "varchar", 'length' => 3 ],
                'country_code2'       => [ 'type' => "varchar", 'length' => 11 ],
                'country_code3'      => [ 'type' => "varchar", 'length' => 11 ],
                'country_code4'      => [ 'type' => "varchar", 'length' => 11 ],
            ];

            parent::__construct();
        }

        public function getAllDataCountry(){
            $data_destination = $this
                ->select(array('country_code', 'country_name'))
                ->orderby('country_name', 'ASC')
                ->get()->result();

            return $data_destination;
        }

        static function inst() {

            if ( ! self::$_inst ) {
                self::$_inst = new self();
            }

            return self::$_inst;
        }
    }
    STTP_CountryModel::inst();
}
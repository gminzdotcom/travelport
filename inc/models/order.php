<?php
/**
 * Created by PhpStorm.
 * User: Jream
 * Date: 4/17/2019
 * Time: 10:51 PM
 */
if ( ! class_exists( 'STTP_OrderModel' ) ) {
	class STTP_OrderModel extends STTP_BaseModel {
		static $_inst = false;

		function __construct() {
			$this->table_version = '1.0.4';
			$this->table_name       = 'sttp_order';
			$this->columns                 = [
				'id'         => [ 'type' => "int", 'length' => 9, 'AUTO_INCREMENT' => TRUE],
				'order_id' => [ 'type' => "int", 'length' => 9 ],
				'origin'       => [ 'type' => "varchar", 'length' => 3 ],
				'destination' => [ 'type' => "varchar", 'length' => 3 ],
				'flight_type' => [ 'type' => "varchar", 'length' => 10 ],
				'fly_out' => [ 'type' => "varchar", 'length' => 50 ],
				'fly_back' => [ 'type' => "varchar", 'length' => 50 ],
                'number_adult' => [ 'type' => "varchar", 'length' => 50 ],
                'number_child' => [ 'type' => "varchar", 'length' => 50 ],
                'number_infant' => [ 'type' => "varchar", 'length' => 50 ],
                'age_child' => [ 'type' => "varchar", 'length' => 50 ],
                'age_infant' => [ 'type' => "varchar", 'length' => 50 ],
				'booking_info' => [ 'type' => "text" ],
				'checkout_info' => [ 'type' => "text" ],
                'user_id' => ['type' => 'int', 'length' => 9],
				'price' => ['type' => 'varchar', 'length' => 50]
			];

			parent::__construct();
		}

		public function getAllDataAirport(){
			$data_destination = $this
				->select(array('airport_code', 'country_code'))
				->orderby('airport_name', 'ASC')
				->get()->result();

			return $data_destination;
		}

		static function inst() {

			if ( ! self::$_inst ) {
				self::$_inst = new self();
			}

			return self::$_inst;
		}
	}
	STTP_OrderModel::inst();
}
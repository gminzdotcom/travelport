<?php
/**
 * Created by PhpStorm.
 * User: HanhDo
 * Date: 4/1/2019
 * Time: 10:37 AM
 */
if ( ! class_exists( 'STTP_CityModel' ) ) {
    class STTP_CityModel extends STTP_BaseModel {
        static $_inst = false;

        function __construct() {
            $this->table_version = '1.0.0';
            $this->table_name       = 'sttp_city';
            $this->columns                 = [
                'city_code'         => [ 'type' => "varchar", 'length' => 3 ],
                'city_unname1'       => [ 'type' => "varchar", 'length' => 255 ],
                'city_name' => [ 'type' => "varchar", 'length' => 255 ],
                'city_country'       => [ 'type' => "varchar", 'length' => 2 ],
                'city_unname2'      => [ 'type' => "varchar", 'length' => 255 ],
                'city_unname3'      => [ 'type' => "varchar", 'length' => 255 ],
                'city_airport'      => [ 'type' => "varchar", 'length' => 255 ],
                'city_unname4'      => [ 'type' => "varchar", 'length' => 255 ],
                'city_unname5'      => [ 'type' => "varchar", 'length' => 255 ],
            ];

            parent::__construct();

            //dd($this->getAllDataCity());
        }

        public function getCityByName($name, $type = 'like'){
            global $wpdb;
            $prefix = $wpdb->prefix;
            $sql = "SELECT cit.city_code, cit.city_name, cit.city_airport, cou.country_name, CONCAT(cit.city_name , ', ' , cou.country_name) AS city_join_name  FROM {$prefix}sttp_city as cit INNER JOIN {$prefix}sttp_country as cou ON cou.country_code = cit.city_country WHERE 1 = 1 AND city_airport != '' AND city_unname1 = '' HAVING (city_name LIKE '%{$name}%' OR city_join_name LIKE '%{$name}%') LIMIT 0, 50";
            $res = $wpdb->get_results($sql);
            return $res;
        }

        public function getAllDataCity(){
            $data_destination = $this
                ->select(array('city_code', 'city_name'))
                ->orderby('city_name', 'ASC')
                ->get()->result();

            return $data_destination;
        }

        static function inst() {

            if ( ! self::$_inst ) {
                self::$_inst = new self();
            }

            return self::$_inst;
        }
    }
	STTP_CityModel::inst();
}
<?php
/**
 * Created by PhpStorm.
 * User: HanhDo
 * Date: 4/1/2019
 * Time: 10:47 AM
 */
if ( ! class_exists( 'STTP_AirplaneModel' ) ) {
    class STTP_AirplaneModel extends STTP_BaseModel {
        static $_inst = false;

        function __construct() {
            $this->table_version = '1.0.0';
            $this->table_name       = 'sttp_airplane';
            $this->columns                 = [
                'airplane_code'         => [ 'type' => "varchar", 'length' => 5 ],
                'airplane_name1'       => [ 'type' => "varchar", 'length' => 255 ],
                'airplane_name2' => [ 'type' => "varchar", 'length' => 255 ],
            ];

            parent::__construct();
        }

        public function getAllDataAirport(){
            $data_destination = $this
                ->select(array('airport_code', 'country_code'))
                ->orderby('airport_name', 'ASC')
                ->get()->result();

            return $data_destination;
        }

        static function inst() {

            if ( ! self::$_inst ) {
                self::$_inst = new self();
            }

            return self::$_inst;
        }
    }
    STTP_AirplaneModel::inst();
}
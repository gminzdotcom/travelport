<?php
/**
 * Created by PhpStorm.
 * User: HanhDo
 * Date: 4/1/2019
 * Time: 10:47 AM
 */
if ( ! class_exists( 'STTP_AirportModel' ) ) {
    class STTP_AirportModel extends STTP_BaseModel {
        static $_inst = false;

        function __construct() {
            $this->table_version = '1.0.0';
            $this->table_name       = 'sttp_airport';
            $this->columns                 = [
                'airport_code'         => [ 'type' => "varchar", 'length' => 3 ],
                'airport_code2'       => [ 'type' => "varchar", 'length' => 255 ],
                'airport_name' => [ 'type' => "varchar", 'length' => 255 ],
                'country_code'       => [ 'type' => "varchar", 'length' => 2 ],
                'airport_code3'      => [ 'type' => "varchar", 'length' => 11 ],
                'airport_code4'      => [ 'type' => "varchar", 'length' => 11 ],
                'airport_code5'      => [ 'type' => "varchar", 'length' => 11 ],
                'airport_code6'      => [ 'type' => "varchar", 'length' => 11 ],
                'airport_code7'      => [ 'type' => "varchar", 'length' => 11 ],
                'airport_code8'      => [ 'type' => "varchar", 'length' => 11 ],
            ];

            parent::__construct();
        }

        public function getAllDataAirport(){
            $data_destination = $this
                ->select(array('airport_code', 'country_code'))
                ->orderby('airport_name', 'ASC')
                ->get()->result();

            return $data_destination;
        }

        static function inst() {

            if ( ! self::$_inst ) {
                self::$_inst = new self();
            }

            return self::$_inst;
        }
    }
    STTP_AirportModel::inst();
}
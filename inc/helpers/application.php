<?php
/**
 * Created by PhpStorm.
 * User: Jream
 * Date: 3/26/2019
 * Time: 3:15 PM
 */
if ( ! function_exists( 'st_travelport_load_view' ) ) {
	function st_travelport_load_view( $view, $data = [], $admin = false) {
		$admin_path = '';
		if($admin)
			$admin_path = 'admin/';
		$file = ST_TravelPort()->get_dir( 'inc/views/' . $admin_path . $view . '.php' );

		if ( file_exists( $file ) ) {
			if ( is_array( $data ) ) {
				extract( $data );
			}
			ob_start();
			include( $file );

			return @ob_get_clean();
		}
	}
}

if(!function_exists('st_travelport_get_option'))
{
	function st_travelport_get_option($key,$default=FALSE){
		$options = get_option( 'st_travelport_'.$key );
		if ( isset( $options ) && '' != $options ) {
			return $options;
		}
		return $default;
	}
}

if(!function_exists('st_travelport_format_currency')){
	function st_travelport_format_currency($currency){
		$currentCurrency = TravelHelper::get_current_currency();
		$symbol = $currentCurrency['name'];
		$currencies = explode($symbol, $currency);
		if(isset($currencies[1])) {
			return TravelHelper::format_money( $currencies[1] );
		}else{
			return $currencies[0];
		}
	}
}
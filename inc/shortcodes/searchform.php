<?php
/**
 * Created by PhpStorm.
 * User: HanhDo
 * Date: 4/1/2019
 * Time: 11:11 AM
 */
if(!function_exists('sttp_search_form_func')){
    function sttp_search_form_func($atts){
        $a = shortcode_atts( array(
            'foo' => 'something',
            'bar' => 'something else',
        ), $atts );

        echo st_travelport_load_view('searchform');

    }
    add_shortcode( 'sttp_search_form', 'sttp_search_form_func' );
}

<?php
/**
 * Created by PhpStorm.
 * User: Jream
 * Date: 4/7/2019
 * Time: 8:12 AM
 */
if(!function_exists('sttp_cart_func')){
	function sttp_cart_func($atts){
		$a = shortcode_atts( array(
			'foo' => 'something',
			'bar' => 'something else',
		), $atts );

		echo st_travelport_load_view('cart');

	}
	add_shortcode( 'sttp_cart', 'sttp_cart_func' );
}
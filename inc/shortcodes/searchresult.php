<?php
/**
 * Created by PhpStorm.
 * User: HanhDo
 * Date: 4/1/2019
 * Time: 11:11 AM
 */
if(!function_exists('sttp_search_result_func')){
    function sttp_search_result_func($atts){
        $a = shortcode_atts( array(
            'foo' => 'something',
            'bar' => 'something else',
        ), $atts );

        echo st_travelport_load_view('searchresult');

    }
    add_shortcode( 'sttp_search_result', 'sttp_search_result_func' );
}

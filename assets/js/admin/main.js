jQuery(document).ready(function ($) {
    $('.btn-sttp-booking-details').click(function (e) {
        var t = $(this);
        e.preventDefault();
        $('#sttp-booking-detail').html('');
        $('#sttpModal').find('.sttp-overlay').show();
        $.ajax({
            'type': 'post',
            'dataType': 'json',
            'url': ajaxurl,
            'data': {
                action: 'sttp_get_booking_history',
                data: t.data('id')
            },
            success: function (data) {
                if (data.status) {
                    $('#sttp-booking-detail').html(data.content);
                    getAirportData();
                } else {
                    alert(data.message);
                }
            },
            complete: function () {
                $('#sttpModal').find('.sttp-overlay').hide();
            }
        })
    })

    if($('.sttp-airport-name').length) {
        getAirportData();
    }



    //sttp-airport-name
    function getAirportData(){
        if( $('.sttp-airport-name').length) {
            var arr_airport_code = [];
            var i_airport_code = 0;
            $('.sttp-airport-name').each(function () {
                i_airport_code++;
                var code = $(this).text();
                if (!arr_airport_code.includes(code)) {
                    arr_airport_code.push(code);
                }

                if(i_airport_code === $('.sttp-airport-name').length){
                    airportCodeCallBack(arr_airport_code);
                }
            });
            function airportCodeCallBack(arr_airport_code){
                $.ajax({
                    'type': 'post',
                    'dataType': 'json',
                    'url': ajaxurl,
                    'data': {
                        arr_code: arr_airport_code,
                        action: 'sttp_get_airport_data'
                    },
                    success: function (data) {
                        if(data.status){
                            $('.sttp-airport-name').each(function () {
                                var code = $(this).text();
                                $(this).text(data.data[code]).show();
                            });
                            $('.sttp-airport-name').each(function () {
                                $(this).parent().attr('title', $(this).text());
                            });
                            $('.sttp-airport-tooltip').tooltip();
                        }
                    }
                })
            }
        }
    }

    sttpDataImport('city');
    sttpDataImport('airplane');
    sttpDataImport('airport');
    sttpDataImport('carrier');
    sttpDataImport('country');

    function sttpDataImport(obj_name){
        $('#sttp_import_' + obj_name).click(function (e) {
            var sparent = $(this).closest('.st_pkfare_sttp_import_' + obj_name);
            $('.spinner', sparent).css('visibility', 'visible');
            $('.message', sparent).html('');
            e.preventDefault();
            $.ajax({
                url:ajaxurl,
                data: {
                    action: 'sttp_import_'+ obj_name +'_data',
                    security: sttp_ajax_object.security
                },
                type: 'POST',
                dataType: 'JSON',
                success: function (response) {
                    $('.message', sparent).html(response.message_content);
                    setTimeout(function () {
                        $('.message', sparent).html("");
                    }, 5000);
                    $('.spinner', sparent).css('visibility', 'hidden');
                    $('.status-import', sparent).html('<span class="dashicons dashicons-yes yes"></span> ' + $('.status-import', sparent).data('text-yes'));
                },
                error: function (response) {
                    $('.spinner', sparent).css('visibility', 'hidden');
                }
            });
        });
    }


});
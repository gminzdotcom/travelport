jQuery(document).ready(function ($) {
    var mainCurrency = 'GBP';
    if($('#sttp-currency-symbol').length){
        mainCurrency = $('#sttp-currency-symbol').data('value');
        console.log(mainCurrency);
    }

    $('.sttp-search-result').closest('.search-result-page>.container').addClass('sttp-fullwidth');

    if($('#sttp-ranger-duration').length){
        var durationRanges = $('#sttp-ranger-duration').data('duration').split(',');
        $("#sttp-ranger-duration").ionRangeSlider({
            type: "double",
            values: durationRanges,
        });
    }
    if($('#sttp-ranger-departure').length){
        var hourRanges = ['00:00', '00:01'];
        for(var iHour = 0; iHour <= 23; iHour++){
            for(var iMinute = 0; iMinute <= 59; iMinute++){
                var hourItem = iHour;
                if(iHour < 10)
                    hourItem = '0' + iHour;

                var minuteItem = iMinute;
                if(minuteItem < 10)
                    minuteItem = '0' + iMinute;

                hourRanges.push(hourItem + ':' + minuteItem);
            }
        }
        $("#sttp-ranger-departure").ionRangeSlider({
            type: "double",
            values: hourRanges,
        });
    }
    if($('.sttp-data-carries').length) {
        var dataCarries = $('.sttp-data-carries').val();

        if (dataCarries !== '') {
            var dataCarriesArr = dataCarries.split(',');
            var teCarries = '';
            for (var i = 0; i < dataCarriesArr.length; i++) {
                var dataCarriesSubArr = dataCarriesArr[i].split('|');
                teCarries += '<div class="item" data-value="' + dataCarriesSubArr[0] + '"><img src="' + dataCarriesSubArr[1] + '" /></div>';
            }
            $('.data-carries').html('<div class="owl-carousel owl-theme">' + teCarries + '</div>');
            $('.data-carries .owl-carousel').owlCarousel({
                loop: true,
                margin: 10,
                nav: true,
                responsive: {
                    0: {
                        items: 1
                    },
                    600: {
                        items: 3
                    },
                    1000: {
                        items: 5
                    }
                }
            });
        }
    }

    $(document).on('click', '.data-carries .item', function () {
       var t = $(this);
        $('.sttp-search-result .item.item-search').each(function () {
            var me = $(this);
            var dataCarriesClick = me.find('.program .outbound-list .sttp-show-by-carries').val();
            console.log(dataCarriesClick);
            var dataCarriesClickArr = dataCarriesClick.split(',');
            if(dataCarriesClickArr.includes(t.data('value'))){
                me.show();
            }else{
                me.hide();
            }
        });
        $('#sttp-clear-filter').addClass('showfilter');
    });

    $('#sttp-clear-filter').click(function () {
        $('.sttp-search-result .item.item-search').show();
        $(this).removeClass('showfilter');
    });


    var currentLocationField = '';
    $('.sttp-search-form .search-form .field-detination').each(function () {
        var t = $(this);
        t.click(function (e) {
            e.preventDefault();
            var me = $(this);
            var currentStatus = me.data('value');
            if (currentStatus === 'from') {
                currentLocationField = 'from';
                if ($('.sttp-dropdown-menu').is(':visible')) {
                    $('.sttp-dropdown-menu').removeClass('next');
                } else {
                    $('.sttp-dropdown-menu').slideDown();
                }

            } else {
                currentLocationField = 'to';
                if ($('.sttp-dropdown-menu').is(':visible')) {
                    $('.sttp-dropdown-menu').addClass('next');
                } else {
                    $('.sttp-dropdown-menu').addClass('next');
                    $('.sttp-dropdown-menu').slideDown();
                }
            }
            $('.sttp-dropdown-menu').find('.country .sttp-search-js').focus();
        })
    });

    function checkAirportSelect() {
        var c = 0;
        $('.sttp-field-airport-code').each(function () {
            if ($(this).val() !== '') {
                c++;
            }
        })
        if (c === $('.sttp-field-airport-code').length) {
            return true;
        } else {
            return false;
        }
    }

    $(document).mouseup(function (e) {
        var container = $(".sttp-dropdown-menu, .sttp-search-form .search-form .field-detination");

        // if the target of the click isn't the container nor a descendant of the container
        if (!container.is(e.target) && container.has(e.target).length === 0) {
            $('.sttp-dropdown-menu').hide();
        }
    });

    var sttpSearchBy = 'country';
    if($('.sttp-search-form').length){
        sttpSearchBy = $('.sttp-search-form').data('search');
    }

    $(document).on('click', '.sttp-dropdown-menu .sttp-dropdown-menu-country .item', function () {
        var t = $(this);
        var parent = t.closest('.sttp-dropdown-menu');
        parent.addClass('loading');
        t.closest('.sttp-dropdown-menu').find('.item').removeClass('active');
        t.addClass('active');

        if(sttpSearchBy != 'city'){
            var sttp_get_airport_action = 'sttp_get_airport_by_country_code';
        }else{
            var sttp_get_airport_action = 'sttp_get_airport_by_city_code';
        }
        $.ajax({
            'type': 'post',
            'dataType': 'json',
            'url': sttp_ajax_object.ajax_url,
            'data': {
                country_code: t.data('value'),
                action: sttp_get_airport_action
            },
            success: function (data) {
                console.log(data.data);
                var te = '';
                if (data.status) {
                    for (var i = 0; i < data.data.length; i++) {
                        te += '<div class="item-wrapper" data-name="' + data.data[i]['airport_name'] + '"><span class="item" data-value="' + data.data[i]['airport_code'] + '">' + data.data[i]['airport_name'] + '</span></div>';
                    }
                    $('#sttp-airport-list').html("").append(te);
                    $('.sttp-dropdown-menu').find('.airport .sttp-search-js').focus();
                }
            },
            complete: function () {
                parent.removeClass('loading');
            },
            error: function (data) {

            }
        })
    });

    if ($('.sttp-dropdown-menu').length) {

        //Search by city name like
        $('.sttp-dropdown-menu .country').each(function (index, el) {
            var parent = $(this);
            var parentDropdown = parent.closest('.sttp-dropdown-menu');
            var input = $('input.sttp-search-js', parent);
            var list = $('.sttp-dropdown-menu-country', parent);
            var timeout;

            if(sttpSearchBy != 'city') {
                input.keyup(function (event) {
                    clearTimeout(timeout);
                    var t = $(this);
                    timeout = setTimeout(function () {
                        var text = t.val().toLowerCase();
                        if (text == '') {
                            $('.item-wrapper', list).show()
                        } else {
                            $('.item-wrapper', list).hide();
                            $(".item-wrapper", list).each(function () {
                                var name = $(this).data("name").toLowerCase();
                                var reg = new RegExp(text, "g");
                                if (reg.test(name)) {
                                    $(this).show()
                                }
                            })
                        }
                    }, 100)
                })
            }else{
                var requestRunningSearchCity = false;
                var xhrSearchCity;
                input.keyup(function (event) {
                    clearTimeout(timeout);
                    var t = $(this);
                    timeout = setTimeout(function () {
                        var text = t.val().toLowerCase();
                        if(text.length >= 2) {
                            parentDropdown.addClass('fa-loading');
                            if (requestRunningSearchCity) {
                                xhrSearchCity.abort();
                            }

                            requestRunningSearchCity = true;

                            xhrSearchCity = $.ajax({
                                'type': 'post',
                                'dataType': 'json',
                                'url': sttp_ajax_object.ajax_url,
                                'data': {
                                    search_text: text,
                                    action: 'sttp_get_city_data'
                                },
                                success: function (data) {
                                    if(data.status){
                                        var te = '<div class="row">';
                                        if(data.data.length){
                                            for (var u = 0; u < data.data.length; u++){
                                                te += '<div class="col-sm-4 col-xs-6 item-wrapper" data-name="'+ data.data[u]['city_name'] +'"><span class="item" data-value="' + data.data[u]['city_airport'] + '">'+ data.data[u]['city_name'] + ', ' + data.data[u]['country_name'] +'</span></div>';
                                            }
                                        }
                                        te += '</div>';
                                        list.html(te);
                                    }else{
                                        list.html('<div class="alert alert-warning">'+ sttp_ajax_object.noCityData +'</div>');
                                    }
                                },
                                complete: function () {
                                    parentDropdown.removeClass('fa-loading');
                                    requestRunningSearchCity = false;
                                }
                            });
                        }
                    }, 250)
                })
            }
        });


        $('.sttp-dropdown-menu .airport').each(function (index, el) {
            var parent = $(this);
            var input = $('input.sttp-search-js', parent);
            var list = $('#sttp-airport-list', parent);
            var timeout;
            input.keyup(function (event) {
                clearTimeout(timeout);
                var t = $(this);
                timeout = setTimeout(function () {
                    var text = t.val().toLowerCase();
                    if (text == '') {
                        $('.item-wrapper', list).show()
                    } else {
                        $('.item-wrapper', list).hide();
                        $(".item-wrapper", list).each(function () {
                            var name = $(this).data("name").toLowerCase();
                            var reg = new RegExp(text, "g");
                            if (reg.test(name)) {
                                $(this).show()
                            }
                        })
                    }
                }, 100)
            })
        });
        $(document).on('click', '#sttp-airport-list .item', function () {
            var t = $(this),
                parent = t.closest('#sttp-airport-list'),
                objActive;

            parent.find('.item').removeClass('active');
            t.addClass('active');

            console.log(currentLocationField);

            if (currentLocationField === 'from') {
                objActive = $('.field-detination[data-value="from"]');
            } else {
                objActive = $('.field-detination[data-value="to"]');
            }
            objActive.find('.render .destination').text(t.data('value') + ' (' + t.text() + ')');
            objActive.find('.sttp-field-airport-code').val(t.data('value'));
            objActive.find('.sttp-field-airport-name').val(t.text());
            objActive.find('.sttp-field-country-code').val($('.sttp-dropdown-menu-country .item.active').data('value'));

            if(currentLocationField === 'from'){
                if($('.field-detination[data-value="to"] ').find('.sttp-field-airport-code').val() === ''){
                    currentLocationField = 'to';
                    $('.sttp-dropdown-menu').addClass('next');
                }
            }else{
                if($('.field-detination[data-value="from"] ').find('.sttp-field-airport-code').val() === ''){
                    currentLocationField = 'from';
                    $('.sttp-dropdown-menu').removeClass('next');
                }
            }

            if (checkAirportSelect()) {
                $('.advance-search').slideDown();
                $('.sttp-dropdown-menu').slideUp();
            }
        });
    }

    if (typeof daterangepicker !== 'undefined' && $.isFunction(daterangepicker)) {
        $('.sttp-input-out-date').daterangepicker({
                autoApply: true,
                disabledPast: true,
                dateFormat: $('.sttp-date').data('format'),
                customClass: '',
                widthSingle: 500,
                singleDatePicker: true,
                sameDate: true,
            },
            function (start, end, label) {
                var parent = $('.sttp-date');
                $('.sttp-out', parent).find('.render span').html(start.format(parent.data('format')));
                $('.sttp-fly-out', parent).val(start.format(parent.data('format')));

                var back_date = $('.sttp-back', parent).find('.sttp-fly-back').val();
                if (back_date < start.format(parent.data('format'))) {
                    $('.sttp-back', parent).find('.sttp-fly-back').val(start.format(parent.data('format')));
                    $('.sttp-back', parent).find('.render span').html(start.format(parent.data('format')));
                    $('.sttp-back', parent).find('.sttp-input-back-date').val(start.format(parent.data('format')) + '-' + start.format(parent.data('format')));

                    var minDateBackDate = start.format('MM/DD/YYYY');
                    $('.sttp-input-back-date').daterangepicker({
                            autoApply: true,
                            disabledPast: true,
                            dateFormat: $('.sttp-date').data('format'),
                            customClass: '',
                            widthSingle: 500,
                            singleDatePicker: true,
                            sameDate: true,
                            minDate: minDateBackDate
                        },
                        function (start, end, label) {
                            var parent = $('.sttp-date');
                            $('.sttp-back', parent).find('.render span').html(start.format(parent.data('format')));
                            $('.sttp-fly-back', parent).val(start.format(parent.data('format')));
                        }
                    );
                }
            }
        );

        $('.sttp-input-back-date').daterangepicker({
                autoApply: true,
                disabledPast: true,
                dateFormat: $('.sttp-date').data('format'),
                customClass: '',
                widthSingle: 500,
                singleDatePicker: true,
                sameDate: true,
            },
            function (start, end, label) {
                var parent = $('.sttp-date');
                $('.sttp-back', parent).find('.render span').html(start.format(parent.data('format')));
                $('.sttp-fly-back', parent).val(start.format(parent.data('format')));
            }
        );
    }

    $('.people-item select.child, .people-item select.infant').change(function(){
        var ageType = 'infant';
        if($(this).hasClass('child')){
            ageType = 'child';
        }

        var ageValue = $(this).val();

        var te = '';
        var peopleText = sttp_ajax_object.childText;
        var jfrom = 2;
        var jto = 11;
        if(ageType === 'infant'){
            peopleText = sttp_ajax_object.infantText;
            jfrom = 0;
            jto = 1;
        }
        var lengthChild = $('.sttp-age-item.'+ageType+' .item').length;
        if(ageValue > 0) {
            if (lengthChild > ageValue) {
                for (var i = lengthChild; i > ageValue; i--) {
                    $('.sttp-age-item.' + ageType + ' .item').eq(i - 1).remove();
                }
            } else (lengthChild < ageValue)
            {
                var remain = ageValue - lengthChild;
                var istart = lengthChild;
                for (var i = 0; i < remain; i++) {
                    te += '<div class="item col-sm-2">\n' +
                        '                                <small>' + peopleText + ' ' + (i + istart + 1) + '</small>\n' +
                        '                                <select class="form-control" name="sttp_' + ageType + '_age[]">\n';
                    for (var j = jfrom; j <= jto; j++) {
                        te += '<option value="' + j + '">' + j + '</option>';
                    }
                    te += '                              </select>\n' +
                        '                            </div>';
                }
                $('.sttp-age-item.' + ageType).show().find('.row').append(te);
            }
        }else{
            $('.sttp-age-item.' + ageType).hide().find('.row .item').remove();
        }
    });

    $('.sttp-more-option span').click(function () {
        var t = $(this);
        var parent = t.closest('.program');
        if(t.find('.subset').text() == '+'){
            //$('.sub-item-wrapper').slideUp();
            $('.sub-item-wrapper', parent).slideDown();
            t.find('.subset').text('-');
            $('.sttp-search-result .item').removeClass('active');
            t.closest('.item').addClass('active');
        }else{
            $('.sub-item-wrapper', parent).slideUp();
            t.find('.subset').text('+');
            t.closest('.item').removeClass('active');
        }
    });

    if( $('.sttp-cabin-class').length) {
        var arr_plane_code = [];
        var i_plane_code = 0;
        $('.sttp-cabin-class').each(function () {
            i_plane_code++;
            var code = $(this).text();
            if (!arr_plane_code.includes(code)) {
                arr_plane_code.push(code);
            }

            if(i_plane_code === $('.sttp-cabin-class').length){
                airplaneCodeCallBack(arr_plane_code);
            }
        });
        function airplaneCodeCallBack(arr_plane_code){
            $.ajax({
                'type': 'post',
                'dataType': 'json',
                'url': sttp_ajax_object.ajax_url,
                'data': {
                    arr_code: arr_plane_code,
                    action: 'sttp_get_equipment_data'
                },
                success: function (data) {
                    if(data.status){
                        $('.sttp-cabin-class').each(function () {
                            var code = $(this).text();
                            $(this).text(data.data[code]);
                        });
                    }
                },
                complete: function () {
                    getCarrierData();
                }
            })
        }
    }

    if($('.sttp-booking').length || $('#cart-info').length || $('.sttp-airport-name').length){
        getAirportData();
    }

    //zzz
    //sttp-airport-name
    function getAirportData(){
        if( $('.sttp-airport-name').length) {
            var arr_airport_code = [];
            var i_airport_code = 0;
            $('.sttp-airport-name').each(function () {
                i_airport_code++;
                var code = $(this).text();
                if (!arr_airport_code.includes(code)) {
                    arr_airport_code.push(code);
                }

                if(i_airport_code === $('.sttp-airport-name').length){
                    airportCodeCallBack(arr_airport_code);
                }
            });
            function airportCodeCallBack(arr_airport_code){
                $.ajax({
                    'type': 'post',
                    'dataType': 'json',
                    'url': sttp_ajax_object.ajax_url,
                    'data': {
                        arr_code: arr_airport_code,
                        action: 'sttp_get_airport_data'
                    },
                    success: function (data) {
                        if(data.status){
                            $('.sttp-airport-name').each(function () {
                                var code = $(this).text();
                                $(this).text(data.data[code]).show();
                            });
                            $('.sttp-airport-name').each(function () {
                                $(this).parent().attr('title', $(this).text());
                            });
                            $('.sttp-airport-tooltip').tooltip();
                        }
                    }
                })
            }
        }
    }

    function getCarrierData(){
        if( $('.sttp-carrier').length) {
            var arr_carrier_code = [];
            var i_carrier_code = 0;
            $('.sttp-carrier').each(function () {
                i_carrier_code++;
                var code = $(this).text();
                if (!arr_carrier_code.includes(code)) {
                    arr_carrier_code.push(code);
                }

                if(i_carrier_code === $('.sttp-carrier').length){
                    carrierCodeCallBack(arr_carrier_code);
                }
            });
            function carrierCodeCallBack(arr_carrier_code){
                $.ajax({
                    'type': 'post',
                    'dataType': 'json',
                    'url': sttp_ajax_object.ajax_url,
                    'data': {
                        arr_code: arr_carrier_code,
                        action: 'sttp_get_carrier_data'
                    },
                    success: function (data) {
                        if(data.status){
                            $('.sttp-carrier').each(function () {
                                var code = $(this).text();
                                $(this).text(data.data[code]);
                            });
                        }

                    },
                    complete: function () {
                        getAirportData();
                    }
                })
            }
        }
    }

    $('.outbound-list .sttp-selection input[type="checkbox"]').change(function () {
        //$('.program .inbound-list').slideUp();
        var parent = $(this).closest('.program');
        if($(this).is(':checked')) {
            var currentItn = $(this).closest('.itn');
            $('.outbound-list .itn', parent).not(currentItn).slideUp();
            $('.sttp-more-option', parent).fadeOut();
            $('.inbound-list', parent).slideDown();
        }else{
            $('.outbound-list .itn', parent).slideDown();
            $('.sttp-more-option', parent).fadeIn();
            $('.inbound-list', parent).slideUp();
        }

    });

    //Display multi add to cart button
    $('.sttp-search-result .item').each(function(){
       var parent = $(this);
       $('.outbound-list input[name="sttp_selection"]', parent).change(function () {
          if($(this).is(':checked')){
              var checkI = 0;
              if($(this).data('way') === 'roundtrip') {
                  $('.inbound-list .sttp-selection input', parent).each(function () {
                      if ($(this).is(':checked')) {
                          checkI++;
                      }
                  });
              }else{
                  checkI = 1;
              }
              if(checkI > 0){
                  $('.sttp-add-to-cart', parent).fadeIn();
              }
          }else{
              $('.sttp-add-to-cart', parent).fadeOut();
          }
       });

        $('.inbound-list .sttp-selection input', parent).change(function () {
            if($(this).is(':checked')){
                var checkI = 0;
                if($(this).data('way') === 'roundtrip') {
                    $('.outbound-list .sttp-selection input', parent).each(function () {
                        if ($(this).is(':checked')) {
                            checkI++;
                        }
                    });
                }else{
                    checkI = 1;
                }
                if(checkI > 0){
                    $('.sttp-add-to-cart', parent).fadeIn();
                }
            }else{
                $('.sttp-add-to-cart', parent).fadeOut();
            }
        });
    });

    //Add to cart multi data
    $('.sttp-add-to-cart').click(function (e) {
        e.preventDefault();
        var t = $(this);
        var parent = t.closest('.item');
        var postPos = parent.data('index');
        var outboundPos = $('.outbound-list .sttp-selection input:checked', parent).closest('.itn').data('index');
        var inboundPos = $('.inbound-list .sttp-selection input:checked', parent).closest('.itn').data('index');

        var getData = $('#sttp-form-get form').serializeArray();
        getData.push({name: 'postPos',value: postPos});
        getData.push({name: 'inboundPos',value: inboundPos});
        getData.push({name: 'outboundPos',value: outboundPos});
        getData.push({name: 'action',value: 'sttp_add_cart_data'});

        t.addClass('loading');

        $.ajax({
            'type': 'post',
            'dataType': 'json',
            'url': sttp_ajax_object.ajax_url,
            'data': getData,
            success: function (data) {
                if(data.status){
                    if(data.cartLink !== '') {
                        window.location.href = data.cartLink;
                    }
                }
            },
            complete: function(){
                t.removeClass('loading');
            }
        })
    })

    $('#show-advance').click(function (e) {
        $('.advance-search').slideToggle();
    });

    //$('.advance-search-box').find('.sttp-date .sttp-back').fadeOut();

    $('.flight-way ul li').click(function(){
        $('.flight-way ul li').removeClass('active');
        $(this).addClass('active');
        if($(this).data('value') === 'oneway'){
            $('input[name="flight_way"]').val('oneway');
            $(this).closest('.advance-search-box').find('.sttp-date .sttp-back').fadeOut();
        }else{
            $('input[name="flight_way"]').val('roundtrip');
            $(this).closest('.advance-search-box').find('.sttp-date .sttp-back').fadeIn();
        }
    })

    $('#sttp-checkout-form').submit(function(e){
        e.preventDefault();
        var form = $(this);

        var check = true;
        $('input[type="text"].passenger', form).each(function () {
            if ($(this).val() === '') {
                check = false;
                $(this).addClass('error');
            }
        });

        var paymentType = $('input[name="sttp_payment"]:checked', form).val();
        if(paymentType === 'credit_card'){
            $('input[type="text"].cardinfo', form).each(function () {
                if ($(this).val() === '') {
                    check = false;
                    $(this).addClass('error');
                }
            });
        }

        if(!check){
            window.scroll({
                top:  $('input[type="text"].error', form).first().offset().top - 70,
                left: 0,
                behavior: 'smooth'
            });
        }else {
            $('.sttp-purchase', form).addClass('loading');
            var getData = $(this).serializeArray();

            getData.push({name: 'action', value: 'sttp_create_air_booking'});
            $.ajax({
                'type': 'post',
                'dataType': 'json',
                'url': sttp_ajax_object.ajax_url,
                'data': getData,
                success: function (data) {
                    if(data.status){
                        $('.purchase-message').html('<div class="alert alert-success">'+ data.message +'</div>');
                    }else{
                        $('.purchase-message').html('<div class="alert alert-danger">'+ data.message +'</div>');
                    }
                },
                complete: function () {
                    $('.sttp-purchase', form).removeClass('loading');
                }
            })
        }
    })

    $('.btn-get-seat').click(function(){
        var t = $(this);

        $('.sttp-passenger-adult-input').removeClass('error');
        $('.sttp-passenger-child-input').removeClass('error');
        $('.ct-seat-message').hide();

        var checkAdult = true;
        $('.sttp-passenger-adult-input').each(function () {
            if($(this).val() === '') {
                checkAdult = false;
                $(this).addClass('error');
            }
        });

        var checkChild = true;
        $('.sttp-passenger-child-input').each(function () {
            if($(this).val() === '') {
                checkChild = false;
                $(this).addClass('error');
            }
        });

        if(!checkAdult || !checkChild) {
            $('.ct-seat-message').html(sttp_ajax_object.enterNamePassenger).show();
            window.scroll({
                top:  $('input[type="text"].error', '.check-out-form').first().offset().top - 70,
                left: 0,
                behavior: 'smooth'
            });

            return false;
        }
        //Get passenger data
        //if($('.sttp-popup-seat-box .content').is(':empty')) {
            t.addClass('active');

            var getData = $('.check-out-form #cc-form').serializeArray();

            getData.push({name: 'action', value: 'sttp_get_seatmap'});

            $.ajax({
                'type': 'post',
                'dataType': 'json',
                'url': sttp_ajax_object.ajax_url,
                'data': getData,
                success: function (data) {
                    if (data.status) {
                        $('.sttp-popup-seat-box .content').html(data.content);
                        $('.sttp-popup-seat-box').fadeIn();
                        /*$('.sttp-seat-tooltip').each(function(){
                            var t = $(this);
                            var tWidth = t.width();
                            console.log(tWidth);
                            t.css({'margin-left': '-' + (tWidth/2) + 'px'});
                        })*/

                        if($('.ct-tooltip').length) {
                            $('.ct-tooltip').tooltipster({
                                //theme: 'tooltipster-light',
                                contentCloning: true
                            });
                        }
                    }
                },
                complete: function () {
                    t.removeClass('active');
                },
                error: function(e) {
                    $('.ct-seat-message').html(sttp_ajax_object.errorWhenSearchSeatData).show();
                }
            })
        //}else{
        //    $('.sttp-popup-seat-box').fadeIn();
        //}
    });

    $('.sttp-popup-seat-box .close').click(function () {
       $(this).parent().fadeOut();
    });

    //Choose seatmap
    $(document).on('click', '.tab-pane.active .seat-table td .item-available.normal', function(){
        var t = $(this);
        if(t.hasClass('normal')) {
            var seatCode = t.data('seat');
            var seatPrice = t.data('price');

            var currentPos = $('.tab-pane.active .sttp-seat-traveler .item.active').index();

            $('.tab-pane.active .sttp-seat-traveler .item.active .seat-value').text(seatCode);
            $('.tab-pane.active .sttp-seat-traveler .item.active .seat-value').attr('data-price', seatPrice);
            $('.tab-pane.active .sttp-seat-traveler .item.active .seat-value').attr('data-seat', seatCode);
            if (currentPos < $('.tab-pane.active .sttp-seat-traveler .item').length - 1) {
                if($('.tab-pane.active .sttp-seat-traveler .item').eq(currentPos + 1).find('.seat-value').text() === ''){
                    $('.tab-pane.active .sttp-seat-traveler .item').removeClass('active');
                    $(this).removeClass('normal').removeClass('selected-active').addClass('selected');
                    $(this).attr('data-index', currentPos);
                    $('.tab-pane.active .sttp-seat-traveler .item').eq(currentPos + 1).addClass('active');
                }else{
                    var tI=  0;
                    var iTemp = '';
                    for(var i = 0; i < $('.tab-pane.active .sttp-seat-traveler .item').length; i++){
                        if($('.tab-pane.active .sttp-seat-traveler .item').eq(i).find('.seat-value').text() === ''){
                            tI++;
                            iTemp = i;
                            break;
                        }
                    }

                    if(tI > 0){
                        $('.tab-pane.active .sttp-seat-traveler .item').removeClass('active');
                        $(this).removeClass('normal').removeClass('selected-active').addClass('selected');
                        $(this).attr('data-index', currentPos);
                        $('.tab-pane.active .sttp-seat-traveler .item').eq(iTemp).addClass('active');
                    }else{
                        $(this).attr('data-index', currentPos);
                        $('.tab-pane.active .seat-table td .item-available.selected-active[data-index="'+ currentPos +'"]').removeClass('selected-active').removeClass('selected').addClass('normal').attr('data-index', '');
                        $(this).removeClass('normal').removeClass('selected').addClass('selected-active');
                    }
                }
            }else{
                var tI=  0;
                var iTemp = '';
                for(var i = 0; i < $('.tab-pane.active .sttp-seat-traveler .item').length; i++){
                    if($('.tab-pane.active .sttp-seat-traveler .item').eq(i).find('.seat-value').text() === ''){
                        tI++;
                        iTemp = i;
                        break;
                    }
                }

                if(tI > 0){
                    $('.tab-pane.active .sttp-seat-traveler .item').removeClass('active');
                    $(this).removeClass('normal').removeClass('selected-active').addClass('selected');
                    $(this).attr('data-index', currentPos);
                    $('.tab-pane.active .sttp-seat-traveler .item').eq(iTemp).addClass('active');
                }else {
                    $(this).attr('data-index', currentPos);
                    $('.tab-pane.active .seat-table td .item-available.selected-active[data-index="' + currentPos + '"]').removeClass('selected-active').removeClass('selected').addClass('normal').attr('data-index', '');
                    $(this).removeClass('normal').removeClass('selected').addClass('selected-active');
                }
            }
        }
    });

    $(document).on('click', '.tab-pane.active .sttp-seat-traveler .item', function () {
        if(!$(this).hasClass('active')) {
            $('.tab-pane.active .sttp-seat-traveler .item').removeClass('active');
            $(this).addClass('active');
            var pos = $(this).index();

            $('.tab-pane.active .item-available.selected-active').removeClass('selected-active').addClass('selected');
            $('.tab-pane.active .item-available.selected[data-index="' + pos + '"]').removeClass('selected').addClass('selected-active');
        }
    })

    $(document).on('click', '.sttp-apply-seat', function () {
        var data = [];
        var me = $(this);
        me.addClass('loading');
        $('#cart-info .total-section ul li.seat-price').html('');
        $('#cart-info .total-section ul li.sub-total').html('');
        $('.sttp-seat-data').html('');

        var totalPrice = 0;

        $('.sttp-seat-traveler .item').each(function () {
            var t = $(this);
            var arrTemp = [];
            var seat = t.find('.seat-value');
            //arrTemp = ;

            data.push({
                'coderef': seat.data('coderef'),
                'key': seat.data('key'),
                'price': seat.data('price'),
                'seatCode': seat.data('seat'),
                'fullName':seat.data('name')
            });

            var dataCodeRef = seat.data('coderef');
            $('.sttp-seat-data').each(function(){
                if($(this).data('value') === dataCodeRef){
                    if(typeof seat.data('seat') !== 'undefined') {
                        $(this).append(seat.data('name') + " " + seat.data('seat') + " (" + seat.data('price') + ")" + "<br />");
                    }
                }
            });

            $('.sttp-seat-data').show();
        });

        $.ajax({
            'type': 'post',
            'dataType': 'json',
            'url': sttp_ajax_object.ajax_url,
            'data': {
                action: 'sttp_set_seatmap',
                data: JSON.stringify(data)
            },
            success: function (data) {
                if(data.html !== ''){
                    $('#cart-info .total-section ul li.seat-price').html(data.html);
                    $('#cart-info .total-section ul li.sub-total').html(data.html_sub_total);
                    $('#cart-info .total-section ul li.payment-amount .value').html(data.html_total_amount);
                }
            },
            complete: function () {
                me.removeClass('loading');
                $('.sttp-popup-seat-box').fadeOut();
            }
        })
    });


    $('.btn-sttp-booking-details').click(function (e) {
        var t = $(this);
        e.preventDefault();
        $('#sttp-booking-detail').html('');
        $('#sttpModal').find('.sttp-overlay').show();
        $.ajax({
            'type': 'post',
            'dataType': 'json',
            'url': sttp_ajax_object.ajax_url,
            'data': {
                action: 'sttp_get_booking_history',
                data: t.data('id')
            },
            success: function (data) {
                if(data.status){
                    $('#sttp-booking-detail').html(data.content);
                    getAirportData();
                }else{
                    alert(data.message);
                }
            },
            complete: function () {
                $('#sttpModal').find('.sttp-overlay').hide();
            }
        })
    })

    if($('.sttp_price_range').length){
        var maxPrice = $(this).data('max');
        var minPrice = $(this).data('min');
        var dataFrom = $(".sttp_price_range").data('sttpfrom');
        if(dataFrom === '')
            dataFrom = minPrice;
        var dataTo = $(".sttp_price_range").data('sttpto');
        if(dataTo === '')
            dataTo = maxPrice;
        $(".sttp_price_range").ionRangeSlider({
            max: maxPrice,
            min: minPrice,
            from: dataFrom,
            to: dataTo,
            type: 'double',
            prettify: false,
        });
    }
});
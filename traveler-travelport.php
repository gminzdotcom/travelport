<?php
/**
 * Plugin Name: Traveler - Travelport
 * Plugin URI: #
 * Description: Plugin only for Theme: Shinetheme Traveler. TravelPort API
 * Version: 1.0
 * Author: Shinetheme
 * Author URI: http://shinetheme.com
 * Requires at least: 4.0
 * Tested up to: 4.7
 * Text Domain: st_travelport
 */
// don't load directly

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

if ( ! class_exists( 'ST_TravelPort_API' ) and ! function_exists( 'ST_TravelPort' ) ) {
	class ST_TravelPort_API {
		static $_inst = false;
		public $_dir_path = false;
		public $_dir_url = false;

		function __construct() {
            $this->_dir_path = trailingslashit(plugin_dir_path(__FILE__));
            $this->_dir_url = trailingslashit(plugin_dir_url(__FILE__));
            add_action('plugins_loaded', [$this, '_init']);
            add_action('after_setup_theme', [$this, '_init_cores'], 10);
            add_action('admin_enqueue_scripts', [$this, '_add_admin_scripts']);
            add_action('wp_enqueue_scripts', [$this, '_add_scripts']);
		}

		function _init() {
			load_plugin_textdomain( 'st_travelport', false, plugin_basename( dirname( __FILE__ ) ) . '/languages' );
		}

		function _init_cores() {
			$this->_load_files();
		}

		function _load_files() {
            if(class_exists('STTravelCode') && class_exists('STTraveler')) {
                $arr = [
                    'inc/controllers/BaseImporter.php',
                    'inc/helpers/application.php',
                    'inc/models/base.php',
                    'inc/models/country.php',
	                'inc/models/city.php',
                    'inc/models/airport.php',
                    'inc/models/airplane.php',
                    'inc/models/carrier.php',
                    'inc/models/order.php',
                    'inc/controllers/travelport.php',
                    'inc/controllers/base.php',
                    'inc/controllers/readapi.php',
                    'inc/controllers/settings.php',
                    'inc/shortcodes/searchform.php',
                    'inc/shortcodes/searchresult.php',
                    'inc/shortcodes/cart.php',
                    'inc/controllers/ImporterController.php',
                ];
                foreach ($arr as $v) {
                    include_once trailingslashit($this->_dir_path) . $v;
                }
            }
		}

		function _add_admin_scripts() {
            if(class_exists('STTravelCode') && class_exists('STTraveler')) {
                $page = STInput::get('page');
                if ($page == 'sttp-flight-booking') {
                    wp_enqueue_style('sttp-bootstrap-css', get_template_directory_uri() . '/v2/css/bootstrap.min.css');
                    wp_enqueue_script('sttp-bootstrap-js', get_template_directory_uri() . '/v2/js/bootstrap.min.js', array('jquery'), false, true);
                }

                wp_enqueue_style('sttp-main-css', $this->_dir_url . "assets/css/admin/main.css");
                wp_enqueue_script('sttp-main-js', $this->_dir_url . "assets/js/admin/main.js", array('jquery'), false, true);

                wp_localize_script(
                    'jquery',
                    'sttp_ajax_object',
                    [
                        'ajax_url' => admin_url('admin-ajax.php'),
                        'security' => wp_create_nonce('sttp-security-nonce'),
                        'childText' => __('Child', 'st_travelport'),
                        'infantText' => __('Infant', 'st_travelport'),
                        'noCityData' => __('No City Data', 'st_travelport'),
                        'enterNamePassenger' => __('Please enter name of passengers', 'st_travelport'),
                        'errorWhenSearchSeatData' => __('Have an error when get seat map data', 'st_travelport')
                    ]
                );
            }
		}

		function _add_scripts() {
            if(class_exists('STTravelCode') && class_exists('STTraveler')) {
                wp_enqueue_style('sttp-main-css', $this->_dir_url . "assets/css/main.css");
                wp_enqueue_style('sttp-tooltipster-css', $this->_dir_url . "assets/js/tooltip/css/tooltipster.bundle.min.css");
                wp_enqueue_style('sttp-tooltipster-light-css', $this->_dir_url . "assets/js/tooltip/css/plugins/tooltipster/sideTip/themes/tooltipster-sideTip-light.min.css");
                wp_enqueue_style('sttp-responsive-css', $this->_dir_url . "assets/css/responsive.css");
                wp_enqueue_script('sttp-tooltipster-js', $this->_dir_url . "assets/js/main.js", array('jquery'), false, true);
                wp_enqueue_script('sttp-main-js', $this->_dir_url . "assets/js/tooltip/js/tooltipster.bundle.min.js", array('jquery'), false, true);

                wp_localize_script(
                    'jquery',
                    'sttp_ajax_object',
                    [
                        'ajax_url' => admin_url('admin-ajax.php'),
                        'security' => wp_create_nonce('sttp-security-nonce'),
                        'childText' => __('Child', 'st_travelport'),
                        'infantText' => __('Infant', 'st_travelport'),
                        'noCityData' => __('No City Data', 'st_travelport'),
                        'enterNamePassenger' => __('Please enter name of passengers', 'st_travelport'),
                        'errorWhenSearchSeatData' => __('Have an error when get seat map data', 'st_travelport')
                    ]
                );
            }
		}

		function get_dir( $file = false ) {
			return $this->_dir_path . $file;
		}

		function get_url( $file = false ) {
			return $this->_dir_url . $file;
		}

		function d( $arr ) {
			/*echo '<pre>';
			print_r( $arr );
			echo '</pre>';*/
		}



		static function inst() {
			if ( ! self::$_inst ) {
				self::$_inst = new self();
			}
			return self::$_inst;
		}
	}

	function ST_TravelPort() {
		return ST_TravelPort_API::inst();
	}

	ST_TravelPort();
}
